/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gCUSTOMER_URL = "http://localhost:8080/api/customers";
const gCOLUMN_ID = {
    stt: 0,
    contactName: 1,
    contactTitle: 2,
    address: 3,
    mobile: 4,
    email: 5,
    createdBy:6,
    updatedBy:7,
    createDate: 8,
    updateDate:9,
    action:10
}
const gCOL_NAME = [
    "stt",
    "contactName",
    "contactTitle",
    "address",
    "mobile",
    "email",
    "createdBy",
    "updatedBy",
    "createDate",
    "updateDate"

]
//Hàm chính để load html hiển thị ra bảng
class Main {
    constructor() {
        $(document).ready(() => {
            this.vOrderList = new RenderPage()
            this.vOrderList.renderPage()
            this.vModal = new Modal()
            this.vModal.openModal("create")
            $('.select2').select2()
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })
        })
    }



}
new Main()
/*** REGION 2 - vùng để render ra bảng*/
class RenderPage {
    constructor() {
        this.vApi = new CallApi()
        this.vModal = new Modal()

    }

    //Hàm gọi api lấy danh sách đơn hàng
    _getCustomerList() {
        this.vApi.onGetCustomersClick((paramcustomer) => {
            this._createCustomerTable(paramcustomer)
        })
    }

    //Hàm tạo các thành phần của bảng
    _createCustomerTable(paramcustomer) {
        let stt = 1;
        if ($.fn.DataTable.isDataTable('#table-customer')) {
            $('#table-customer').DataTable().destroy();
        }
        const vOrderTable = $("#table-customer").DataTable({
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false,
            "scrollX": true,
            "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],
            // Khai báo các cột của datatable
            "columns": [
                { "data": null },
                { "data": gCOL_NAME[gCOLUMN_ID.contactName] },
                { "data": gCOL_NAME[gCOLUMN_ID.contactTitle] },
                { "data": gCOL_NAME[gCOLUMN_ID.address] },
                { "data": gCOL_NAME[gCOLUMN_ID.mobile] },
                { "data": gCOL_NAME[gCOLUMN_ID.email] },
                { "data": gCOL_NAME[gCOLUMN_ID.createdBy] },
                { "data": gCOL_NAME[gCOLUMN_ID.updatedBy] },
                { "data": gCOL_NAME[gCOLUMN_ID.createDate] },
                { "data": gCOL_NAME[gCOLUMN_ID.updateDate] },
                { "data": gCOL_NAME[gCOLUMN_ID.action] },
            ],

            "columnDefs": [
    
                {
                    targets: gCOLUMN_ID.stt,
                    render: function () {
                        return stt++;
                    }
                },
                {
                    targets: gCOLUMN_ID.action,
                    defaultContent: `
                        <img class="edit-customer" src="https://cdn0.iconfinder.com/data/icons/glyphpack/45/edit-alt-512.png" style="width: 20px;cursor:pointer;">
                        <img class="delete-customer" src="https://cdn4.iconfinder.com/data/icons/complete-common-version-6-4/1024/trash-512.png" style="width: 20px;cursor:pointer;">
                    `,
                    createdCell: (cell, cellData, rowData, rowIndex, colIndex) => {
                        if (colIndex === gCOLUMN_ID.action) {
                            $(cell).find('.edit-customer').on('click', () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                this.vModal.openModal('edit', vData);
                            });
    
                            $(cell).find('.delete-customer').on('click', () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                console.log(vData);
                                this.vModal.openModal('delete', vData);
                            });
                        }
                    }
                }
            ]
        });
        vOrderTable.clear(); // xóa toàn bộ dữ liệu trong bảng
        vOrderTable.rows.add(paramcustomer); // cập nhật dữ liệu cho bảng
        vOrderTable.draw(); // hàm vẽ lại bảng
        vOrderTable.buttons().container().appendTo('.example1_wrapper .col-md-6:eq(0)'); 
        $('.table-container').css('overflow-x', 'auto');
    }
    
    _saveExcelFile(data, filename) {
        const blob = new Blob([data], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
        const url = window.URL.createObjectURL(blob);
        const link = document.createElement("a");
        link.href = url;
        link.download = filename;
        link.click();
      }
      
    _exportToExcel() {
        $("#btn-export-excel").on("click",()=> {
            this.vApi.onExportExcelClick((data)=> {
                console.log(data)
              this._saveExcelFile(data, "users.xlsx");
            });
        });
    }
    // Hàm sẽ được gọi ở class Main 
    renderPage() {
        this._exportToExcel()
        this._getCustomerList()
    }
}

/*** REGION 3 - vùng hiện modal*/

class Modal {
    constructor() {
        this.vApi = new CallApi()
    }

    _onEventListner() {
        $("#btn-add-customer").on("click", () => {
            $("#create-customer-modal").modal("show")
        })
    }

    _createCustomer() {
        this._clearInput();
        $("#btn-create-customer").on("click", () => {
          this._clearInValid()
      
          let isValid = true;
          const vFields = [
            "input-create-customer-contactName",
            "input-create-customer-address",
            "input-create-customer-mobile",
          ];
      
          vFields.forEach((field) => {
            const value = $(`#${field}`).val().trim();
            if (!value) {
              isValid = false;
              $(`#${field}`).addClass("is-invalid");
              $(`#${field}`).after(`<div class="invalid-feedback error-message">Please enter a valid value!</div>`);
            }
          });
      
          if (isValid) {
            const customerData = {
              contactName: $("#input-create-customer-contactName").val().trim(),
              contactTitle: $("#input-create-customer-contactTitle").val().trim(),
              address: $("#input-create-customer-address").val().trim(),
              mobile: $("#input-create-customer-mobile").val().trim(),
              email: $("#input-create-customer-email").val().trim(),
              note: $("#input-create-customer-note").val().trim()
            };
      
            this.vApi.onCreateCustomerClick(customerData, (data) => {
              console.log(data);
            });
          }
        }).bind(this);
      }
      
    
      _updateCustomer(customer) {
        // Show values in the input fields
        $('#input-update-customer-contactName').val(customer.contactName);
        $('#input-update-customer-contactTitle').val(customer.contactTitle);
        $('#input-update-customer-address').val(customer.address);
        $('#input-update-customer-mobile').val(customer.mobile);
        $('#input-update-customer-email').val(customer.email);
        $('#input-update-customer-note').val(customer.note);
      
        $("#btn-update-customer").on("click", () => {
          this._clearInput();
          this._clearInValid();
          let isValid = true;
          const vFields = [
            "input-update-customer-contactName",
            "input-update-customer-address",
            "input-update-customer-mobile",
          ];
      
          vFields.forEach((field) => {
            const value = $(`#${field}`).val().trim();
            if (!value) {
              isValid = false;
              $(`#${field}`).addClass("is-invalid");
              $(`#${field}`).after(`<div class="invalid-feedback error-message">Please enter a valid value!</div>`);
            }
          });
      
          if (isValid) {
            const customerData = {
              contactName: $("#input-update-customer-contactName").val().trim(),
              contactTitle: $("#input-update-customer-contactTitle").val().trim(),
              address: $("#input-update-customer-address").val().trim(),
              mobile: $("#input-update-customer-mobile").val().trim(),
              email: $("#input-update-customer-email").val().trim(),
              note: $("#input-update-customer-note").val().trim()
            };
      
            this.vApi.onUpdateCustomerClick(customer.id, customerData, (data) => {
              console.log(data);
            });
          }
        }).bind(this);
      }
      
    

    _deleteCustomer(data) {
        $("#btn-confirm-delete-customer").on("click", () => {
          this.vApi.onDeleteCustomerClick(data.id)
        })

    }
    _clearInValid(input) {
        if (input) {
            input.removeClass("is-invalid");
            $(".invalid-feedback").remove();
        }
        $(".form-control").removeClass("is-invalid");
        $(".invalid-feedback").remove();
    }

    _clearInput() {
        $("#create-customer-form").find("input").val("");
        $("#create-customer-form").find("select").val("")
    }
    openModal(type, data) {
        if (type === "create") {
            this._clearInput()
            this._onEventListner()
            this._createCustomer()
        } else {
            if (type === "edit") {
                this._updateCustomer(data)
                $("#update-customer-modal").modal("show")
            } else {
                this._deleteCustomer(data)
                $("#delete-confirm-modal").modal("show")
            }
        }

    }
}

/*** REGION 4 - vùng để gọi lên cơ sở dữ liệu lấy đa ta về*/
class CallApi {
    constructor() {

    }
    onShowToast(paramTitle, paramMessage) {
        $('#myToast .mr-auto').text(paramTitle)
        $('#myToast .toast-body').text(paramMessage);
        $('#myToast').toast('show');
    }

    onGetCustomersClick(paramCallbackFn) {
        $.ajax({
            url: gCUSTOMER_URL,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onGetCustomersPerCountry(paramCallbackFn) {
        $.ajax({
            url: `${gCUSTOMER_URL}/country`,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data)
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    onExportExcelClick(paramCallbackFn) {
        $.ajax({
            url: `http://localhost:8080/api/export/customers/excel`,
            method: 'GET',
            xhrFields: {
                responseType: "blob"
              },
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    onGetCustomerByIdClick(customerId, paramCallbackFn) {
        $.ajax({
            url: gcustomer_URL + "/" + customerId,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onCreateCustomerClick(customerData, paramCallbackFn) {
        $.ajax({
            url: gCUSTOMER_URL,
            method: 'POST',
            data: JSON.stringify(customerData),
            contentType: 'application/json',
            success: (data) => {
                paramCallbackFn(data);
                const render = new RenderPage()
                render.renderPage()
                this.onShowToast("Tạo thành công", "Tạo khách hàng thành công!!")
                $("#create-customer-modal").modal("hide");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown, textStatus);
            }
        });
    }
    onUpdateCustomerClick(customerId, customerData, paramCallbackFn) {
        $.ajax({
            url: gCUSTOMER_URL + "/" + customerId,
            method: 'PUT',
            data: JSON.stringify(customerData),
            contentType: 'application/json',
            success: (data) => {
                const render = new RenderPage()
                render.renderPage()
                paramCallbackFn(data);
                $('#update-customer-modal').modal('hide');
                this.onShowToast("Sửa thành công", "Sửa khách hàng thành công!!")
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    onDeleteCustomerClick(customerId) {
        $.ajax({
            url: gCUSTOMER_URL + "/" + customerId,
            method: 'DELETE',
            success:  () => {
                this.onShowToast("Xóa thành công", "bạn đã xóa khách hàng thành công!!")
                const render = new RenderPage()
                render.renderPage()
                $("#delete-confirm-modal").modal("hide");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    
}