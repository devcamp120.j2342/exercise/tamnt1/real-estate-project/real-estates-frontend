/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gDESIGN_UNIT_URL = "http://localhost:8080/api/design-units";
const gCOLUMN_ID = {
    stt: 0,
    name: 1,
    projects: 2,
    address: 3,
    phone: 4,
    fax:5,
    email:6,
    website: 7,
    note:8,
    action:9
}
const gCOL_NAME = [
    "stt",
    "name",
    "projects",
    "address",
    "phone",
    "fax",
    "email",
    "website",
    "note"

]
//Hàm chính để load html hiển thị ra bảng
class Main {
    constructor() {
        $(document).ready(() => {
            this.vOrderList = new RenderPage()
            this.vOrderList.renderPage()
            this.vModal = new Modal()
            this.vModal.openModal("create")
            $('.select2').select2()
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })
        })
    }



}
new Main()
/*** REGION 2 - vùng để render ra bảng*/
class RenderPage {
    constructor() {
        this.vApi = new CallApi()
        this.vModal = new Modal()

    }

    //Hàm gọi api lấy danh sách đơn hàng
    _getDesignUnitList() {
        this.vApi.onGetDesignUnitsClick((paramDesignUnit) => {
            this._createDesignUnitTable(paramDesignUnit)
        })
    }

    //Hàm tạo các thành phần của bảng
    _createDesignUnitTable(paramDesignUnit) {
        let stt = 1;
        if ($.fn.DataTable.isDataTable('#table-design-unit')) {
            $('#table-design-unit').DataTable().destroy();
        }
        const vOrderTable = $("#table-design-unit").DataTable({
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false,
            "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],
            // Khai báo các cột của datatable
            "columns": [
                { "data": null },
                { "data": gCOL_NAME[gCOLUMN_ID.name] },
                { "data": gCOL_NAME[gCOLUMN_ID.projects] },
                { "data": gCOL_NAME[gCOLUMN_ID.address] },
                { "data": gCOL_NAME[gCOLUMN_ID.phone] },
                { "data": gCOL_NAME[gCOLUMN_ID.fax] },
                { "data": gCOL_NAME[gCOLUMN_ID.email] },
                { "data": gCOL_NAME[gCOLUMN_ID.website] },
                { "data": gCOL_NAME[gCOLUMN_ID.note] },
                { "data": gCOL_NAME[gCOLUMN_ID.action] },
            ],
            // Ghi đè nội dung của cột action
            "columnDefs": [
                {
                    targets: gCOLUMN_ID.stt,
                    render: function () {
                        return stt++;
                    }
                },
                {
                    targets: gCOLUMN_ID.action,
                    defaultContent: `
                        <img class="edit-designUnit" src="https://cdn0.iconfinder.com/data/icons/glyphpack/45/edit-alt-512.png" style="width: 20px;cursor:pointer;">
                        <img class="delete-designUnit" src="https://cdn4.iconfinder.com/data/icons/complete-common-version-6-4/1024/trash-512.png" style="width: 20px;cursor:pointer;">
                    `,
                    createdCell: (cell, cellData, rowData, rowIndex, colIndex) => {
                        if (colIndex === gCOLUMN_ID.action) {
                            $(cell).find('.edit-designUnit').on('click', () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                this.vModal.openModal('edit', vData);
                            });
    
                            $(cell).find('.delete-designUnit').on('click', () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                console.log(vData);
                                this.vModal.openModal('delete', vData);
                            });
                        }
                    }
                }
            ]
        });
        vOrderTable.clear(); // xóa toàn bộ dữ liệu trong bảng
        vOrderTable.rows.add(paramDesignUnit); // cập nhật dữ liệu cho bảng
        vOrderTable.draw(); // hàm vẽ lại bảng
        vOrderTable.buttons().container().appendTo('.example1_wrapper .col-md-6:eq(0)'); // Add buttons to the desired container
    }
    
    _saveExcelFile(data, filename) {
        const blob = new Blob([data], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
        const url = window.URL.createObjectURL(blob);
        const link = document.createElement("a");
        link.href = url;
        link.download = filename;
        link.click();
      }
      
    _exportToExcel() {
        $("#btn-export-excel").on("click",()=> {
            this.vApi.onExportExcelClick((data)=> {
                console.log(data)
              this._saveExcelFile(data, "users.xlsx");
            });
        });
    }

      
    // Hàm sẽ được gọi ở class Main 
    renderPage() {
        this._exportToExcel()
        this._getDesignUnitList()
    }
}

/*** REGION 3 - vùng hiện modal*/

class Modal {
    constructor() {
        this.vApi = new CallApi()
    }

    _onEventListner() {
        $("#btn-add-design-unit").on("click", () => {
            $("#create-design-unit-modal").modal("show")
        })
    }

    _createDesignUnit() {
        $("#btn-create-design-unit").on("click", () => {
          this._clearInValid();
      
          let isValid = true;
          const vFields = [
            "input-create-name",
            "input-create-address",
            "input-create-phone",
          ];
      
          vFields.forEach((field) => {
            const value = $(`#${field}`).val().trim();
            if (!value) {
              isValid = false;
              $(`#${field}`).addClass("is-invalid");
              $(`#${field}`).after(`<div class="invalid-feedback error-message">Vui lòng nhập và giá trị hợp lệ!.</div>`);
            }
          });
      
          if (isValid) {
            const designUnitData = {
              name: $("#input-create-name").val().trim(),
              description: $("#input-create-desc").val().trim(),
              project: $("#input-create-project").val().trim(),
              address: $("#input-create-address").val().trim(),
              phone: $("#input-create-phone").val().trim(),
              secondPhone: $("#input-create-phone2").val().trim(),
              fax: $("#input-create-fax").val().trim(),
              email: $("#input-create-email").val().trim(),
              website: $("#input-create-website").val().trim(),
              note: $("#input-create-note").val().trim(),
            };
      
            this.vApi.onCreateDesignUnitClick(designUnitData, (data) => {
              console.log(data);
            });
          }
        }).bind(this);
      }
      
    
      _updateDesignUnit(designUnit) {
        $("#input-update-name").val(designUnit.name);
        $("#input-update-desc").val(designUnit.description);
        $("#input-update-project").val(designUnit.project);
        $("#input-update-address").val(designUnit.address);
        $("#input-update-phone").val(designUnit.phone);
        $("#input-update-phone2").val(designUnit.phone2);
        $("#input-update-fax").val(designUnit.fax);
        $("#input-update-email").val(designUnit.email);
        $("#input-update-website").val(designUnit.website);
        $("#input-update-note").val(designUnit.note);
        $("#btn-update-design-unit").on("click", () => {
          this._clearInValid()
      
          let isValid = true;
          const vFields = [
            "input-update-name",
            "input-update-address",
            "input-update-phone",
          ];
      
          vFields.forEach((field) => {
            const value = $(`#${field}`).val().trim();
            if (!value) {
              isValid = false;
              $(`#${field}`).addClass("is-invalid");
              $(`#${field}`).after(`<div class="invalid-feedback error-message">Vui lòng nhập và giá trị hợp lệ!.</div>`);
            }
          });
      
          if (isValid) {
            const designUnitData = {
              name: $("#input-update-name").val().trim(),
              description: $("#input-update-desc").val().trim(),
              project: $("#input-update-project").val().trim(),
              address: $("#input-update-address").val().trim(),
              phone: $("#input-update-phone").val().trim(),
              phone2: $("#input-update-phone2").val().trim(),
              fax: $("#input-update-fax").val().trim(),
              email: $("#input-update-email").val().trim(),
              website: $("#input-update-website").val().trim(),
              note: $("#input-update-note").val().trim(),
            };
      
            this.vApi.onUpdateDesignUnitClick(designUnit.id,designUnitData, (data) => {
              console.log(data);
            });
          }
        }).bind(this);
      }
      
    

    _deleteDesignUnit(data) {
        $("#btn-confirm-delete-design-unit").on("click", () => {
          this.vApi.onDeleteDesignUnitClick(data.id)
        })

    }
    _clearInValid(input) {
        if (input) {
            input.removeClass("is-invalid");
            $(".invalid-feedback").remove();
        }
        $(".form-control").removeClass("is-invalid");
        $(".invalid-feedback").remove();
    }

    _clearInput() {
        $("#create-design-unit-form").find("input").val("");
        $("#create-design-unit-form").find("select").val("")
    }
    openModal(type, data) {
        if (type === "create") {
            this._clearInput()
            this._onEventListner()
            this._createDesignUnit()
        } else {
            if (type === "edit") {
                this._updateDesignUnit(data)
                $("#update-design-unit-modal").modal("show")
            } else {
                this._deleteDesignUnit(data)
                $("#delete-confirm-modal").modal("show")
            }
        }

    }
}

/*** REGION 4 - vùng để gọi lên cơ sở dữ liệu lấy đa ta về*/
class CallApi {
    constructor() {

    }
    onShowToast(paramTitle, paramMessage) {
        $('#myToast .mr-auto').text(paramTitle)
        $('#myToast .toast-body').text(paramMessage);
        $('#myToast').toast('show');
    }

    onGetDesignUnitsClick(paramCallbackFn) {
        $.ajax({
            url: gDESIGN_UNIT_URL,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onExportExcelClick(paramCallbackFn) {
        $.ajax({
            url: `http://localhost:8080/api/export/designUnits/excel`,
            method: 'GET',
            xhrFields: {
                responseType: "blob"
              },
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    onGetDesignUnitByIdClick(designUnitId, paramCallbackFn) {
        $.ajax({
            url: gDESIGN_UNIT_URL + "/" + designUnitId,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onCreateDesignUnitClick(designUnitData, paramCallbackFn) {
        $.ajax({
            url: gDESIGN_UNIT_URL,
            method: 'POST',
            data: JSON.stringify(designUnitData),
            contentType: 'application/json',
            success: (data) => {
                paramCallbackFn(data);
                const render = new RenderPage()
                render.renderPage()
                this.onShowToast("Tạo thành công", "Tạo nhà thầu thiết kế thành công!!")
                $("#create-design-unit-modal").modal("hide");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown, textStatus);
            }
        });
    }
    onUpdateDesignUnitClick(designUnitId, designUnitData, paramCallbackFn) {
        $.ajax({
            url: gDESIGN_UNIT_URL + "/" + designUnitId,
            method: 'PUT',
            data: JSON.stringify(designUnitData),
            contentType: 'application/json',
            success: (data) => {
                const render = new RenderPage()
                render.renderPage()
                paramCallbackFn(data);
                $('#update-design-unit-modal').modal('hide');
                this.onShowToast("Sửa thành công", "Sửa  nhà thầu thiết kế thành công!!")
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    onDeleteDesignUnitClick(designUnitId) {
        $.ajax({
            url: gDESIGN_UNIT_URL + "/" + designUnitId,
            method: 'DELETE',
            success:  () => {
                this.onShowToast("Xóa thành công", "bạn đã xóa  nhà thầu thiết kế thành công!!")
                const render = new RenderPage()
                render.renderPage()
                $("#delete-confirm-modal").modal("hide");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    
}