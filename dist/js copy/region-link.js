/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gregionLink_URL = "http://localhost:8080/api/region-links";

const gCOLUMN_ID = {
    stt: 0,
    name: 1,
    latitude: 2,
    longitude:3,
    action: 4

}
const gCOL_NAME = [
    "stt",
    "name",
    "latitude",
    "longitude"
    

]
//Hàm chính để load html hiển thị ra bảng
class Main {
    constructor() {
        $(document).ready(() => {
            this.vOrderList = new RenderPage()
            this.vOrderList.renderPage()
            this.vModal = new Modal()
            this.vModal.openModal("create")
            $('.select2').select2()
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })
        })
    }

}
new Main()
/*** REGION 2 - vùng để render ra bảng*/
class RenderPage {
    constructor() {
        this.vApi = new CallApi()
        this.vModal = new Modal()

    }

    //Hàm gọi api lấy danh sách đơn hàng
    _getRegionLinkList() {
        this.vApi.onGetRegionLinksClick((paramRegionLink) => {
            this._createRegionLinkTable(paramRegionLink)
        })
    }

    //Hàm tạo các thành phần của bảng
    _createRegionLinkTable(paramRegionLink) {
        let stt = 1
        if ($.fn.DataTable.isDataTable('#table-regionLink')) {
            $('#table-regionLink').DataTable().destroy();
        }
        const vOrderTable = $("#table-regionLink").DataTable({
            // Khai báo các cột của datatable
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false,
            "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],
            "columns": [
                { "data": null },
                { "data": gCOL_NAME[gCOLUMN_ID.name] },
                { "data": gCOL_NAME[gCOLUMN_ID.latitude] },
                { "data": gCOL_NAME[gCOLUMN_ID.longitude] },
                { "data": gCOL_NAME[gCOLUMN_ID.action] },


            ],
            // Ghi đè nội dung của cột action
            "columnDefs": [
                {
                    targets: gCOLUMN_ID.stt,
                    render: function () {
                        return stt++
                    }
                }, {

                    targets: gCOLUMN_ID.action,
                    defaultContent: `
                              <img class="edit-regionLink" src="https://cdn0.iconfinder.com/data/icons/glyphpack/45/edit-alt-512.png" style="width: 20px;cursor:pointer;">
                              <img class="delete-regionLink" src="https://cdn4.iconfinder.com/data/icons/complete-common-version-6-4/1024/trash-512.png" style="width: 20px;cursor:pointer;">
                            `,
                    createdCell: (cell, cellData, rowData, rowIndex, colIndex) => {
                        if (colIndex === gCOLUMN_ID.action) {
                            $(cell).find('.edit-regionLink').on('click', () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                this.vModal.openModal('edit', vData);
                            });

                            $(cell).find('.delete-regionLink').on('click', () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                console.log(vData)
                                this.vModal.openModal('delete', vData);
                            });
                        }
                    }
                }],

        });
        vOrderTable.clear() // xóa toàn bộ dữ liệu trong bảng
        vOrderTable.rows.add(paramRegionLink) // cập nhật dữ liệu cho bảng
        vOrderTable.draw()// hàm vẻ lại bảng
        vOrderTable.buttons().container().appendTo('.example1_wrapper .col-md-6:eq(0)')
    }

    _saveExcelFile(data, filename) {
        const blob = new Blob([data], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
        const url = window.URL.createObjectURL(blob);
        const link = document.createElement("a");
        link.href = url;
        link.download = filename;
        link.click();
      }
      
    _exportToExcel() {
        $("#btn-export-excel").on("click",()=> {
            this.vApi.onExportExcelClick((data)=> {
                console.log(data)
              this._saveExcelFile(data, "regionLink.xlsx");
            });
        });
    }


    // Hàm sẽ được gọi ở class Main 
    renderPage() {
        this._exportToExcel()
        this._getRegionLinkList()
    }
}

/*** REGION 3 - vùng hiện modal*/

class Modal {
    constructor() {
        this.vApi = new CallApi()
    }

    _onEventListner() {
        $("#btn-add-regionLink").on("click", () => {
            $("#create-regionLink-modal").modal("show")

        })
    }


    _createRegionLink() {
        this._clearInValid();
        $("#btn-create-regionLink").on("click", () => {

          let isValid = true;
          const vFields = [
            "input-create-name",
          ];

      
          vFields.forEach((field) => {
            const value = $(`#${field}`).val().trim();
            if (!value) {
              isValid = false;
              $(`#${field}`).addClass("is-invalid");
              $(`#${field}`).after(`<div class="invalid-feedback error-message">Vui lòng nhập và giá trị hợp lệ!</div>`);
            }
          });
      
          if (isValid) {
            const regionLinkData = {
              name: $("#input-create-name").val().trim(),
              description: $("#input-create-desc").val().trim(),
              photo: $("#input-create-photo").val().trim(),
              address: $("#input-create-address").val().trim(),
              latitude: $("#input-create-latedute").val().trim(),
              longitude: $("#input-create-longtadue").val().trim(),
            };
            this.vApi.onCreateRegionLinkClick(regionLinkData,(data)=> {
                console.log(data)
            })
          }
        }).bind(this);
      

      }
       
   _updateRegionLink(regionLink) {
    $("#input-update-name").val(regionLink.name);
    $("#input-update-desc").val(regionLink.requiredDate);
    $("#input-update-address").val(regionLink.address);
    $("#input-update-latedute").val(regionLink.latitude);
    $("#input-update-longtadue").val(regionLink.longitude);

  const fileInput = $("#input-update-photo");
  const imagePreviewContainer = $(".image-preview-container");
  const deleteButton = $("#btn-delete-photo");

  $("#btn-update-regionLink").on("click", () => {
    this._clearInValid();
    let isValid = true;
    const vFields = [
      "input-update-name",
    ];

    vFields.forEach((field) => {
      const value = $(`#${field}`).val().trim();
      if (!value) {
        isValid = false;
        $(`#${field}`).addClass("is-invalid");
        $(`#${field}`).after(`<div class="invalid-feedback error-message">Vui lòng nhập và giá trị hợp lệ!</div>`);
      }
    });

    if (isValid) {
      const regionLinkData = {
        name: $("#input-update-name").val().trim(),
        requiredDate: $("#input-update-desc").val().trim(),
        photo: fileInput[0].files && fileInput[0].files[0] ? fileInput[0].files[0].name : "",
        address: $("#input-update-address").val().trim(),
        latedute: $("#input-update-latedute").val().trim(),
        longtadue: $("#input-update-longtadue").val().trim(),
      };

         this.vApi.onUpdateRegionLinkClick(regionLink.id, regionLinkData, (data) => {
          console.log(data);
            });
          }
        }).bind(this);
     }
      

    _deleteRegionLink(data) {
        $("#btn-confirm-delete-regionLink").on("click", () => {
            this.vApi.onDeleteRegionLinkClick(data.id)
        })

    }
    _clearInValid(input) {
        if (input) {
            input.removeClass("is-invalid");
            $(".invalid-feedback").remove();
        }
        $(".form-control").removeClass("is-invalid");
        $(".invalid-feedback").remove();
    }

    _clearInput() {
        $("#create-regionLink-form").find("input").val("");
        $("#create-regionLink-form").find("select").val("")
    }
    openModal(type, data) {
        if (type === "create") {
            this._clearInput()
            this._onEventListner()
            this._createRegionLink()
        } else {
            if (type === "edit") {
                this._updateRegionLink(data)
                $("#update-regionLink-modal").modal("show")
            } else {
                this._deleteRegionLink(data)
                $("#delete-confirm-modal").modal("show")
            }
        }

    }
}

/*** REGION 4 - vùng để gọi lên cơ sở dữ liệu lấy đa ta về*/
class CallApi {
    constructor() {

    }
    onShowToast(paramTitle, paramMessage) {
        $('#myToast .mr-auto').text(paramTitle)
        $('#myToast .toast-body').text(paramMessage);
        $('#myToast').toast('show');
    }


    onGetRegionLinksClick(paramCallbackFn) {
        $.ajax({
            url: gregionLink_URL,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    onGetRegionLinkByIdClick(regionLinkId, paramCallbackFn) {
        $.ajax({
            url: gregionLink_URL + "/" + regionLinkId,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onCreateRegionLinkClick(regionLinkData, paramCallbackFn) {
        $.ajax({
            url: gregionLink_URL,
            method: 'POST',
            data: JSON.stringify(regionLinkData),
            contentType: 'application/json',
            success: (data) => {
                paramCallbackFn(data);
                const render = new RenderPage()
                render.renderPage()
                this.onShowToast("Tạo thành công", "Tạo regionLink thành công!!")
                $("#create-regionLink-modal").modal("hide");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown, textStatus);
            }
        });
    }
    onUpdateRegionLinkClick(regionLinkId, regionLinkData, paramCallbackFn) {
        $.ajax({
            url: gregionLink_URL + "/" + regionLinkId,
            method: 'PUT',
            data: JSON.stringify(regionLinkData),
            contentType: 'application/json',
            success: (data) => {
                const render = new RenderPage()
                render.renderPage()
                paramCallbackFn(data);
                $('#update-regionLink-modal').modal('hide');
                this.onShowToast("Sửa thành công", "Sửa regionLink thành công!!")
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown); 
            }
        });
    }
    onDeleteRegionLinkClick(regionLinkId) {
        $.ajax({
            url: gregionLink_URL + "/" + regionLinkId,
            method: 'DELETE',
            success:  () =>{
                this.onShowToast("Xóa thành công", "bạn đã xóa regionLink thành công!!")
                const render = new RenderPage()
                render.renderPage()
                $("#delete-confirm-modal").modal("hide");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    onExportExcelClick(paramCallbackFn) {
        $.ajax({
            url: `http://localhost:8080/api/export/regionLinks/excel`,
            method: 'GET',
            xhrFields: {
                responseType: "blob"
              },
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
}