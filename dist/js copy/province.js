/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gPROVINCE_URL = "http://localhost:8080/api/provinces";

const gCOLUMN_ID = {
    stt: 0,
    name: 1,
    code: 2,
    action: 3

}
const gCOL_NAME = [
    "stt",
    "name",
    "code",
    

]
//Hàm chính để load html hiển thị ra bảng
class Main {
    constructor() {
        $(document).ready(() => {
            this.vOrderList = new RenderPage()
            this.vOrderList.renderPage()
            this.vModal = new Modal()
            this.vModal.openModal("create")
            $('.select2').select2()
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })
        })
    }

}
new Main()
/*** REGION 2 - vùng để render ra bảng*/
class RenderPage {
    constructor() {
        this.vApi = new CallApi()
        this.vModal = new Modal()

    }

    //Hàm gọi api lấy danh sách đơn hàng
    _getProvinceList() {
        this.vApi.onGetProvincesClick((paramprovince) => {
            this._createProvinceTable(paramprovince)
        })
    }

    //Hàm tạo các thành phần của bảng
    _createProvinceTable(paramprovince) {
        let stt = 1
        if ($.fn.DataTable.isDataTable('#table-province')) {
            $('#table-province').DataTable().destroy();
        }
        const vOrderTable = $("#table-province").DataTable({
            // Khai báo các cột của datatable
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false,
            "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],
            "columns": [
                { "data": null },
                { "data": gCOL_NAME[gCOLUMN_ID.name] },
                { "data": gCOL_NAME[gCOLUMN_ID.code] },
                { "data": gCOL_NAME[gCOLUMN_ID.action] },


            ],
            // Ghi đè nội dung của cột action
            "columnDefs": [
                {
                    targets: gCOLUMN_ID.stt,
                    render: function () {
                        return stt++
                    }
                }, {

                    targets: gCOLUMN_ID.action,
                    defaultContent: `
                              <img class="edit-province" src="https://cdn0.iconfinder.com/data/icons/glyphpack/45/edit-alt-512.png" style="width: 20px;cursor:pointer;">
                              <img class="delete-province" src="https://cdn4.iconfinder.com/data/icons/complete-common-version-6-4/1024/trash-512.png" style="width: 20px;cursor:pointer;">
                            `,
                    createdCell: (cell, cellData, rowData, rowIndex, colIndex) => {
                        if (colIndex === gCOLUMN_ID.action) {
                            $(cell).find('.edit-province').on('click', () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                this.vModal.openModal('edit', vData);
                            });

                            $(cell).find('.delete-province').on('click', () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                console.log(vData)
                                this.vModal.openModal('delete', vData);
                            });
                        }
                    }
                }],

        });
        vOrderTable.clear() // xóa toàn bộ dữ liệu trong bảng
        vOrderTable.rows.add(paramprovince) // cập nhật dữ liệu cho bảng
        vOrderTable.draw()// hàm vẻ lại bảng
        vOrderTable.buttons().container().appendTo('.example1_wrapper .col-md-6:eq(0)')
    }

    _saveExcelFile(data, filename) {
        const blob = new Blob([data], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
        const url = window.URL.createObjectURL(blob);
        const link = document.createElement("a");
        link.href = url;
        link.download = filename;
        link.click();
      }
      
    _exportToExcel() {
        $("#btn-export-excel").on("click",()=> {
            this.vApi.onExportExcelClick((data)=> {
                console.log(data)
              this._saveExcelFile(data, "province.xlsx");
            });
        });
    }


    // Hàm sẽ được gọi ở class Main 
    renderPage() {
        this._exportToExcel()
        this._getProvinceList()
    }
}

/*** REGION 3 - vùng hiện modal*/

class Modal {
    constructor() {
        this.vApi = new CallApi()
    }

    _onEventListner() {
        $("#btn-add-province").on("click", () => {
            $("#create-province-modal").modal("show")
            this._renderPhotoList()
        })
    }

    _createprovince() {
        this._clearInput();
        $("#btn-create-province").on("click", () => {
          this._clearInValid();
      
          let isValid = true;
          const vFields = [
            "input-create-checkNumber",
            "input-create-provinceDate",
            "input-create-amount"
          ];
      
          vFields.forEach((field) => {
            const value = $(`#${field}`).val().trim();
            if (!value) {
              isValid = false;
              $(`#${field}`).addClass("is-invalid");
              $(`#${field}`).after(`<div class="invalid-feedback error-message">Vui lòng nhập và giá trị hợp lệ!.</div>`);
            }
          });
      
          if (isValid) {
            const provinceData = {
              checkNumber: $("#input-create-checkNumber").val().trim(),
              provinceDate: $("#input-create-provinceDate").val().trim(),
              ammount: parseFloat($("#input-create-amount").val().trim())
            };
            this.vApi.onCreateProvinceClick(provinceData, (data) => {
              console.log(data);
            });
          }
        }).bind(this);
      }
      
      _updateProvince(province) {
        $("#input-update-checkNumber").val(province.checkNumber);
        $("#input-update-provinceDate").val(province.provinceDate);
        $("#input-update-amount").val(province.ammount);
      
        $("#btn-update-province").off("click").on("click", () => {
          this._clearInValid();
          let isValid = true;
          const requiredFields = ["input-update-checkNumber", "input-update-provinceDate", "input-update-amount"];
      
          requiredFields.forEach((field) => {
            if (!$(`#${field}`).val().trim()) {
              isValid = false;
              $(`#${field}`).addClass("is-invalid");
              $(`#${field}`).after('<div class="invalid-feedback">Vui lòng nhập giá trị hợp lệ!</div>');
            }
          });
      
          if (isValid) {
            const updatedprovince = {
              checkNumber: $("#input-update-checkNumber").val().trim(),
              provinceDate: $("#input-update-provinceDate").val().trim(),
              ammount: parseFloat($("#input-update-amount").val().trim())
            };
      
            this.vApi.onUpdateProvinceClick(province.id, updatedprovince, (data) => {
              console.log(data);
            });
          }
        }).bind(this);
      }
      

    _deleteProvince(data) {
        $("#btn-confirm-delete-province").on("click", () => {
            this.vApi.onDeleteProvinceClick(data.id)
        })

    }
    _clearInValid(input) {
        if (input) {
            input.removeClass("is-invalid");
            $(".invalid-feedback").remove();
        }
        $(".form-control").removeClass("is-invalid");
        $(".invalid-feedback").remove();
    }

    _clearInput() {
        $("#create-province-form").find("input").val("");
        $("#create-province-form").find("select").val("")
    }
    openModal(type, data) {
        if (type === "create") {
            this._clearInput()
            this._onEventListner()
            this._createprovince()
        } else {
            if (type === "edit") {
                this._updateProvince(data)
                $("#update-province-modal").modal("show")
            } else {
                this._deleteProvince(data)
                $("#delete-confirm-modal").modal("show")
            }
        }

    }
}

/*** REGION 4 - vùng để gọi lên cơ sở dữ liệu lấy đa ta về*/
class CallApi {
    constructor() {

    }
    onShowToast(paramTitle, paramMessage) {
        $('#myToast .mr-auto').text(paramTitle)
        $('#myToast .toast-body').text(paramMessage);
        $('#myToast').toast('show');
    }


    onGetProvincesClick(paramCallbackFn) {
        $.ajax({
            url: gPROVINCE_URL,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    onGetProvinceByIdClick(provinceId, paramCallbackFn) {
        $.ajax({
            url: gPROVINCE_URL + "/" + provinceId,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onCreateProvinceClick(provinceData, paramCallbackFn) {
        $.ajax({
            url: gPROVINCE_URL,
            method: 'POST',
            data: JSON.stringify(provinceData),
            contentType: 'application/json',
            success: (data) => {
                paramCallbackFn(data);
                const render = new RenderPage()
                render.renderPage()
                this.onShowToast("Tạo thành công", "Tạo province thành công!!")
                $("#create-province-modal").modal("hide");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown, textStatus);
            }
        });
    }
    onUpdateProvinceClick(provinceId, provinceData, paramCallbackFn) {
        $.ajax({
            url: gPROVINCE_URL + "/" + provinceId,
            method: 'PUT',
            data: JSON.stringify(provinceData),
            contentType: 'application/json',
            success: (data) => {
                const render = new RenderPage()
                render.renderPage()
                paramCallbackFn(data);
                $('#update-province-modal').modal('hide');
                this.onShowToast("Sửa thành công", "Sửa province thành công!!")
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown); 
            }
        });
    }
    onDeleteProvinceClick(provinceId) {
        $.ajax({
            url: gPROVINCE_URL + "/" + provinceId,
            method: 'DELETE',
            success:  () =>{
                this.onShowToast("Xóa thành công", "bạn đã xóa province thành công!!")
                const render = new RenderPage()
                render.renderPage()
                $("#delete-confirm-modal").modal("hide");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    onExportExcelClick(paramCallbackFn) {
        $.ajax({
            url: `http://localhost:8080/api/export/provinces/excel`,
            method: 'GET',
            xhrFields: {
                responseType: "blob"
              },
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
}