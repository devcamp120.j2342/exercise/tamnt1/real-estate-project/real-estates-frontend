/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gWARD_URL = "http://localhost:8080/api/wards";

const gCOLUMN_ID = {
    stt: 0,
    name: 1,
    prefix: 2,
    district:3,
    province:4,
    action: 5

}
const gCOL_NAME = [
    "stt",
    "name",
    "prefix",
    "district",
    "province"
    

]
//Hàm chính để load html hiển thị ra bảng
class Main {
    constructor() {
        $(document).ready(() => {
            this.vOrderList = new RenderPage()
            this.vOrderList.renderPage()
            this.vModal = new Modal()
            this.vModal.openModal("create")
            $('.select2').select2()
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })
        })
    }

}
new Main()
/*** REGION 2 - vùng để render ra bảng*/
class RenderPage {
    constructor() {
        this.vApi = new CallApi()
        this.vModal = new Modal()

    }

    //Hàm gọi api lấy danh sách đơn hàng
    _getWardList() {
        this.vApi.onGetWardsClick((paramWard) => {
            this._createWardTable(paramWard)
        })
    }

    //Hàm tạo các thành phần của bảng
    _createWardTable(paramWard) {
        let stt = 1
        if ($.fn.DataTable.isDataTable('#table-ward')) {
            $('#table-ward').DataTable().destroy();
        }

        console.log(paramWard)
        const vOrderTable = $("#table-ward").DataTable({
            // Khai báo các cột của datatable
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false,
            "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],
            "columns": [
                { "data": null },
                { "data": gCOL_NAME[gCOLUMN_ID.name] },
                { "data": gCOL_NAME[gCOLUMN_ID.prefix] },
                { "data": gCOL_NAME[gCOLUMN_ID.district] },
                { "data": gCOL_NAME[gCOLUMN_ID.province] },
                { "data": gCOL_NAME[gCOLUMN_ID.action] },
            ],
            // Ghi đè nội dung của cột action
            "columnDefs": [
                {
                    targets: gCOLUMN_ID.stt,
                    render: function () {
                        return stt++
                    }
                }, {

                    targets: gCOLUMN_ID.action,
                    defaultContent: `
                              <img class="edit-ward" src="https://cdn0.iconfinder.com/data/icons/glyphpack/45/edit-alt-512.png" style="width: 20px;cursor:pointer;">
                              <img class="delete-ward" src="https://cdn4.iconfinder.com/data/icons/complete-common-version-6-4/1024/trash-512.png" style="width: 20px;cursor:pointer;">
                            `,
                    createdCell: (cell, cellData, rowData, rowIndex, colIndex) => {
                        if (colIndex === gCOLUMN_ID.action) {
                            $(cell).find('.edit-ward').on('click', () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                this.vModal.openModal('edit', vData);
                            });

                            $(cell).find('.delete-ward').on('click', () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                console.log(vData)
                                this.vModal.openModal('delete', vData);
                            });
                        }
                    }
                }],

        });
        vOrderTable.clear() // xóa toàn bộ dữ liệu trong bảng
        vOrderTable.rows.add(paramWard) // cập nhật dữ liệu cho bảng
        vOrderTable.draw()// hàm vẻ lại bảng
        vOrderTable.buttons().container().appendTo('.example1_wrapper .col-md-6:eq(0)')
    }

    _saveExcelFile(data, filename) {
        const blob = new Blob([data], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
        const url = window.URL.createObjectURL(blob);
        const link = document.createElement("a");
        link.href = url;
        link.download = filename;
        link.click();
      }
      
    _exportToExcel() {
        $("#btn-export-excel").on("click",()=> {
            this.vApi.onExportExcelClick((data)=> {
                console.log(data)
              this._saveExcelFile(data, "ward.xlsx");
            });
        });
    }


    // Hàm sẽ được gọi ở class Main 
    renderPage() {
        this._exportToExcel()
        this._getWardList()
    }
}

/*** REGION 3 - vùng hiện modal*/

class Modal {
    constructor() {
        this.vApi = new CallApi()
    }

    _onEventListner() {
        $("#btn-add-ward").on("click", () => {
            $("#create-ward-modal").modal("show")
            this._renderPhotoList()
        })
    }

    _createWard() {
        this._clearInput();
        $("#btn-create-ward").on("click", () => {
          this._clearInValid();
      
          let isValid = true;
          const vFields = [
            "input-create-checkNumber",
            "input-create-wardDate",
            "input-create-amount"
          ];
      
          vFields.forEach((field) => {
            const value = $(`#${field}`).val().trim();
            if (!value) {
              isValid = false;
              $(`#${field}`).addClass("is-invalid");
              $(`#${field}`).after(`<div class="invalid-feedback error-message">Vui lòng nhập và giá trị hợp lệ!.</div>`);
            }
          });
      
          if (isValid) {
            const wardData = {
              checkNumber: $("#input-create-checkNumber").val().trim(),
              wardDate: $("#input-create-wardDate").val().trim(),
              ammount: parseFloat($("#input-create-amount").val().trim())
            };
            this.vApi.onCreatewardClick(wardData, (data) => {
              console.log(data);
            });
          }
        }).bind(this);
      }
      
      _updateWard(ward) {
        $("#input-update-checkNumber").val(ward.checkNumber);
        $("#input-update-wardDate").val(ward.wardDate);
        $("#input-update-amount").val(ward.ammount);
      
        $("#btn-update-ward").off("click").on("click", () => {
          this._clearInValid();
          let isValid = true;
          const requiredFields = ["input-update-checkNumber", "input-update-wardDate", "input-update-amount"];
      
          requiredFields.forEach((field) => {
            if (!$(`#${field}`).val().trim()) {
              isValid = false;
              $(`#${field}`).addClass("is-invalid");
              $(`#${field}`).after('<div class="invalid-feedback">Vui lòng nhập giá trị hợp lệ!</div>');
            }
          });
      
          if (isValid) {
            const updatedward = {
              checkNumber: $("#input-update-checkNumber").val().trim(),
              wardDate: $("#input-update-wardDate").val().trim(),
              ammount: parseFloat($("#input-update-amount").val().trim())
            };
      
            this.vApi.onUpdateWardClick(ward.id, updatedward, (data) => {
              console.log(data);
            });
          }
        }).bind(this);
      }
      

    _deleteWard(data) {
        $("#btn-confirm-delete-ward").on("click", () => {
            this.vApi.onDeleteWardClick(data.id)
        })

    }
    _clearInValid(input) {
        if (input) {
            input.removeClass("is-invalid");
            $(".invalid-feedback").remove();
        }
        $(".form-control").removeClass("is-invalid");
        $(".invalid-feedback").remove();
    }

    _clearInput() {
        $("#create-ward-form").find("input").val("");
        $("#create-ward-form").find("select").val("")
    }
    openModal(type, data) {
        if (type === "create") {
            this._clearInput()
            this._onEventListner()
            this._createWard()
        } else {
            if (type === "edit") {
                this._updateWard(data)
                $("#update-ward-modal").modal("show")
            } else {
                this._deleteWard(data)
                $("#delete-confirm-modal").modal("show")
            }
        }

    }
}

/*** REGION 4 - vùng để gọi lên cơ sở dữ liệu lấy đa ta về*/
class CallApi {
    constructor() {

    }
    onShowToast(paramTitle, paramMessage) {
        $('#myToast .mr-auto').text(paramTitle)
        $('#myToast .toast-body').text(paramMessage);
        $('#myToast').toast('show');
    }


    onGetWardsClick(paramCallbackFn) {
        $.ajax({
            url: gWARD_URL + "/district",
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    onGetWardByIdClick(wardId, paramCallbackFn) {
        $.ajax({
            url: gWARD_URL + "/" + wardId,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onCreateWardClick(wardData, paramCallbackFn) {
        $.ajax({
            url: gWARD_URL,
            method: 'POST',
            data: JSON.stringify(wardData),
            contentType: 'application/json',
            success: (data) => {
                paramCallbackFn(data);
                const render = new RenderPage()
                render.renderPage()
                this.onShowToast("Tạo thành công", "Tạo ward thành công!!")
                $("#create-ward-modal").modal("hide");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown, textStatus);
            }
        });
    }
    onUpdateWardClick(wardId, wardData, paramCallbackFn) {
        $.ajax({
            url: gWARD_URL + "/" + wardId,
            method: 'PUT',
            data: JSON.stringify(wardData),
            contentType: 'application/json',
            success: (data) => {
                const render = new RenderPage()
                render.renderPage()
                paramCallbackFn(data);
                $('#update-ward-modal').modal('hide');
                this.onShowToast("Sửa thành công", "Sửa ward thành công!!")
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown); 
            }
        });
    }
    onDeleteWardClick(wardId) {
        $.ajax({
            url: gWARD_URL + "/" + wardId,
            method: 'DELETE',
            success:  () =>{
                this.onShowToast("Xóa thành công", "bạn đã xóa ward thành công!!")
                const render = new RenderPage()
                render.renderPage()
                $("#delete-confirm-modal").modal("hide");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    onExportExcelClick(paramCallbackFn) {
        $.ajax({
            url: `http://localhost:8080/api/export/wards/excel`,
            method: 'GET',
            xhrFields: {
                responseType: "blob"
              },
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
}