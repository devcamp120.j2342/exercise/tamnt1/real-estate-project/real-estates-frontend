/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gLOCATION_URL = "http://localhost:8080/api/address-map";

const gCOLUMN_ID = {
    stt: 0,
    address:1,
    lat: 2,
    lng: 3,
    action: 4

}
const gCOL_NAME = [
    "stt",
    "address",
    "lat",
    "lng",
    

]
//Hàm chính để load html hiển thị ra bảng
class Main {
    constructor() {
        $(document).ready(() => {
            this.vOrderList = new RenderPage()
            this.vOrderList.renderPage()
            this.vModal = new Modal()
            this.vModal.openModal("create")
            $('.select2').select2()
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })
        })
    }

}
new Main()
/*** REGION 2 - vùng để render ra bảng*/
class RenderPage {
    constructor() {
        this.vApi = new CallApi()
        this.vModal = new Modal()

    }

    //Hàm gọi api lấy danh sách đơn hàng
    _getLocationList() {
        this.vApi.onGetLocationsClick((paramlocation) => {
            this._createLocationTable(paramlocation)
        })
    }

    //Hàm tạo các thành phần của bảng
    _createLocationTable(paramlocation) {
        let stt = 1
        if ($.fn.DataTable.isDataTable('#table-location')) {
            $('#table-location').DataTable().destroy();
        }
        const vOrderTable = $("#table-location").DataTable({
            // Khai báo các cột của datatable
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false,
            "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],
            "columns": [
                { "data": null },
                { "data": gCOL_NAME[gCOLUMN_ID.address] },
                { "data": gCOL_NAME[gCOLUMN_ID.lat] },
                { "data": gCOL_NAME[gCOLUMN_ID.lng] },
                { "data": gCOL_NAME[gCOLUMN_ID.action] },


            ],
            // Ghi đè nội dung của cột action
            "columnDefs": [
                {
                    targets: gCOLUMN_ID.stt,
                    render: function () {
                        return stt++
                    }
                }, {

                    targets: gCOLUMN_ID.action,
                    defaultContent: `
                              <img class="edit-location" src="https://cdn0.iconfinder.com/data/icons/glyphpack/45/edit-alt-512.png" style="width: 20px;cursor:pointer;">
                              <img class="delete-location" src="https://cdn4.iconfinder.com/data/icons/complete-common-version-6-4/1024/trash-512.png" style="width: 20px;cursor:pointer;">
                            `,
                    createdCell: (cell, cellData, rowData, rowIndex, colIndex) => {
                        if (colIndex === gCOLUMN_ID.action) {
                            $(cell).find('.edit-location').on('click', () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                this.vModal.openModal('edit', vData);
                            });

                            $(cell).find('.delete-location').on('click', () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                console.log(vData)
                                this.vModal.openModal('delete', vData);
                            });
                        }
                    }
                }],

        });
        vOrderTable.clear() // xóa toàn bộ dữ liệu trong bảng
        vOrderTable.rows.add(paramlocation) // cập nhật dữ liệu cho bảng
        vOrderTable.draw()// hàm vẻ lại bảng
        vOrderTable.buttons().container().appendTo('.example1_wrapper .col-md-6:eq(0)')
    }

    _saveExcelFile(data, filename) {
        const blob = new Blob([data], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
        const url = window.URL.createObjectURL(blob);
        const link = document.createElement("a");
        link.href = url;
        link.download = filename;
        link.click();
      }
      
    _exportToExcel() {
        $("#btn-export-excel").on("click",()=> {
            this.vApi.onExportExcelClick((data)=> {
                console.log(data)
              this._saveExcelFile(data, "location.xlsx");
            });
        });
    }


    // Hàm sẽ được gọi ở class Main 
    renderPage() {
        this._exportToExcel()
        this._getLocationList()
    }
}

/*** REGION 3 - vùng hiện modal*/

class Modal {
    constructor() {
        this.vApi = new CallApi()
    }

    _onEventListner() {
        $("#btn-add-location").on("click", () => {
            $("#create-location-modal").modal("show")   
        })
    }
    _createLocation() {
        $("#btn-create-location").on("click", () => {
          this._clearInValid();
          let isValid = true;
          const vFields = [
            "input-create-address",
          ];
      
          vFields.forEach((field) => {
            const value = $(`#${field}`).val().trim();
            if (!value) {
              isValid = false;
              $(`#${field}`).addClass("is-invalid");
              $(`#${field}`).after(`<div class="invalid-feedback error-message">Vui lòng nhập và giá trị hợp lệ!</div>`);
            }
          });
      
          if (isValid) {
            const locationData = {
              address: $("#input-create-address").val().trim(),
              latitude: $("#input-create-latitude").val().trim(),
              longitude: $("#input-create-longitude").val().trim(),
            };
      
            this.vApi.onCreateLocationClick(locationData, (data) => {
              console.log(data);
            });
          }
        }).bind(this);
      }
      
      _updateLocation(location) {
        // Show values in the input fields
        $('#input-update-address').val(location.address);
        $('#input-update-latitude').val(location.latitude);
        $('#input-update-longitude').val(location.longitude);
      
  const locationId = parseInt(location.id)
        $("#btn-update-location").on("click", () => {
          this._clearInValid();
          let isValid = true;
          const vFields = [
            "input-update-address",

          ];
      
          vFields.forEach((field) => {
            const value = $(`#${field}`).val().trim();
            if (!value) {
              isValid = false;
              $(`#${field}`).addClass("is-invalid");
              $(`#${field}`).after(`<div class="invalid-feedback error-message">Vui lòng nhập và giá trị hợp lệ!</div>`);
            }
          });
      
          if (isValid) {
            const locationData = {
              address: $("#input-update-address").val().trim(),
              latitude: $("#input-update-latitude").val().trim(),
              longitude: $("#input-update-longitude").val().trim(),
            };
        
            this.vApi.onUpdateLocationClick(locationId, locationData, (data) => {
              console.log(data);

            });
          }
        }).bind(this);
      }
      
      

    _deleteLocation(data) {
        $("#btn-confirm-delete-location").on("click", () => {
            this.vApi.onDeleteLocationClick(data.id)
        })

    }
    _clearInValid(input) {
        if (input) {
            input.removeClass("is-invalid");
            $(".invalid-feedback").remove();
        }
        $(".form-control").removeClass("is-invalid");
        $(".invalid-feedback").remove();
    }

    _clearInput() {
        $("#create-location-form").find("input").val("");
        $("#create-location-form").find("select").val("")
    }
    openModal(type, data) {
        if (type === "create") {
            this._clearInput()
            this._onEventListner()
            this._createLocation()
        } else {
            if (type === "edit") {
                this._updateLocation(data)
                $("#update-location-modal").modal("show")
            } else {
                this._deleteLocation(data)
                $("#delete-confirm-modal").modal("show")
            }
        }

    }
}

/*** REGION 4 - vùng để gọi lên cơ sở dữ liệu lấy đa ta về*/
class CallApi {
    constructor() {

    }
    onShowToast(paramTitle, paramMessage) {
        $('#myToast .mr-auto').text(paramTitle)
        $('#myToast .toast-body').text(paramMessage);
        $('#myToast').toast('show');
    }


    onGetLocationsClick(paramCallbackFn) {
        $.ajax({
            url: gLOCATION_URL,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    onGetLocationByIdClick(locationId, paramCallbackFn) {
        $.ajax({
            url: gLOCATION_URL + "/" + locationId,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onCreateLocationClick(locationData, paramCallbackFn) {
        $.ajax({
            url: gLOCATION_URL,
            method: 'POST',
            data: JSON.stringify(locationData),
            contentType: 'application/json',
            success: (data) => {
                paramCallbackFn(data);
                const render = new RenderPage()
                render.renderPage()
                this.onShowToast("Tạo thành công", "Tạo location thành công!!")
                $("#create-location-modal").modal("hide");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown, textStatus);
            }
        });
    }
    onUpdateLocationClick(locationId, locationData, paramCallbackFn) {
        $.ajax({
            url: `${gLOCATION_URL}/${locationId}`,
            method: 'PUT',
            data: JSON.stringify(locationData),
            contentType: 'application/json',
            success: (data) => {
                const render = new RenderPage()
                render.renderPage()
                paramCallbackFn(data);
                $('#update-location-modal').modal('hide');
                this.onShowToast("Sửa thành công", "Sửa location thành công!!")
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    onDeleteLocationClick(locationId) {
        $.ajax({
            url: gLOCATION_URL + "/" + locationId,
            method: 'DELETE',
            success:  () =>{
                this.onShowToast("Xóa thành công", "bạn đã xóa location thành công!!")
                const render = new RenderPage()
                render.renderPage()
                $("#delete-confirm-modal").modal("hide");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    onExportExcelClick(paramCallbackFn) {
        $.ajax({
            url: `http://localhost:8080/api/export/locations/excel`,
            method: 'GET',
            xhrFields: {
                responseType: "blob"
              },
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
}