/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const ginvestor_URL = "http://localhost:8080/api/investors";
const gCOLUMN_ID = {
    stt: 0,
    name: 1,
    projects: 2,
    address: 3,
    phone: 4,
    fax:5,
    email:6,
    website: 7,
    note:8,
    action:9
}
const gCOL_NAME = [
    "stt",
    "name",
    "projects",
    "address",
    "phone",
    "fax",
    "email",
    "website",
    "note"

]
//Hàm chính để load html hiển thị ra bảng
class Main {
    constructor() {
        $(document).ready(() => {
            this.vOrderList = new RenderPage()
            this.vOrderList.renderPage()
            this.vModal = new Modal()
            this.vModal.openModal("create")
            $('.select2').select2()
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })
        })
    }



}
new Main()
/*** REGION 2 - vùng để render ra bảng*/
class RenderPage {
    constructor() {
        this.vApi = new CallApi()
        this.vModal = new Modal()

    }

    //Hàm gọi api lấy danh sách đơn hàng
    _getInvestorList() {
        this.vApi.onGetInvestorsClick((paraminvestor) => {
            this._createInvestorTable(paraminvestor)
        })
    }

    //Hàm tạo các thành phần của bảng
    _createInvestorTable(paraminvestor) {
        let stt = 1;
        if ($.fn.DataTable.isDataTable('#table-investor')) {
            $('#table-investor').DataTable().destroy();
        }
        const vOrderTable = $("#table-investor").DataTable({
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false,
            "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],
            // Khai báo các cột của datatable
            "columns": [
                { "data": null },
                { "data": gCOL_NAME[gCOLUMN_ID.name] },
                { "data": gCOL_NAME[gCOLUMN_ID.projects] },
                { "data": gCOL_NAME[gCOLUMN_ID.address] },
                { "data": gCOL_NAME[gCOLUMN_ID.phone] },
                { "data": gCOL_NAME[gCOLUMN_ID.fax] },
                { "data": gCOL_NAME[gCOLUMN_ID.email] },
                { "data": gCOL_NAME[gCOLUMN_ID.website] },
                { "data": gCOL_NAME[gCOLUMN_ID.note] },
                { "data": gCOL_NAME[gCOLUMN_ID.action] },
            ],
            // Ghi đè nội dung của cột action
            "columnDefs": [
                {
                    targets: gCOLUMN_ID.stt,
                    render: function () {
                        return stt++;
                    }
                },
                {
                    targets: gCOLUMN_ID.action,
                    defaultContent: `
                        <img class="edit-investor" src="https://cdn0.iconfinder.com/data/icons/glyphpack/45/edit-alt-512.png" style="width: 20px;cursor:pointer;">
                        <img class="delete-investor" src="https://cdn4.iconfinder.com/data/icons/complete-common-version-6-4/1024/trash-512.png" style="width: 20px;cursor:pointer;">
                    `,
                    createdCell: (cell, cellData, rowData, rowIndex, colIndex) => {
                        if (colIndex === gCOLUMN_ID.action) {
                            $(cell).find('.edit-investor').on('click', () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                this.vModal.openModal('edit', vData);
                            });
    
                            $(cell).find('.delete-investor').on('click', () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                console.log(vData);
                                this.vModal.openModal('delete', vData);
                            });
                        }
                    }
                }
            ]
        });
        vOrderTable.clear(); // xóa toàn bộ dữ liệu trong bảng
        vOrderTable.rows.add(paraminvestor); // cập nhật dữ liệu cho bảng
        vOrderTable.draw(); // hàm vẽ lại bảng
        vOrderTable.buttons().container().appendTo('.example1_wrapper .col-md-6:eq(0)'); // Add buttons to the desired container
    }
    
    _saveExcelFile(data, filename) {
        const blob = new Blob([data], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
        const url = window.URL.createObjectURL(blob);
        const link = document.createElement("a");
        link.href = url;
        link.download = filename;
        link.click();
      }
      
    _exportToExcel() {
        $("#btn-export-excel").on("click",()=> {
            this.vApi.onExportExcelClick((data)=> {
                console.log(data)
              this._saveExcelFile(data, "users.xlsx");
            });
        });
    }
      
    // Hàm sẽ được gọi ở class Main 
    renderPage() {
        this._exportToExcel()
        this._getInvestorList()

    }
}

/*** REGION 3 - vùng hiện modal*/

class Modal {
    constructor() {
        this.vApi = new CallApi()
    }

    _onEventListner() {
        $("#btn-add-investor").on("click", () => {
            $("#create-investor-modal").modal("show")
        })
    }

    _createInvestor() {
        this._clearInput();
        $("#btn-create-investor").on("click", () => {

            this._clearInValid();
            
            let isValid = true;
            const vFields = [
              "input-create-name",
              "input-create-address",
              "input-create-phone",
            ];
        
            vFields.forEach((field) => {
              const value = $(`#${field}`).val().trim();
              if (!value) {
                isValid = false;
                $(`#${field}`).addClass("is-invalid");
                $(`#${field}`).after(`<div class="invalid-feedback error-message">Vui lòng nhập và giá trị hợp lệ!</div>`);
              }
            });
        
            if (isValid) {
              const investorData = {
                name: $("#input-create-name").val().trim(),
                description: $("#input-create-desc").val().trim(),
                project: $("#input-create-project").val().trim(),
                address: $("#input-create-address").val().trim(),
                phone: $("#input-create-phone").val().trim(),
                phone2: $("#input-create-phone2").val().trim(),
                fax: $("#input-create-fax").val().trim(),
                email: $("#input-create-email").val().trim(),
                website: $("#input-create-website").val().trim(),
                note: $("#input-create-note").val().trim(),
              };
              this.vApi.onCreateInvestorClick(investorData, (data) => {
                console.log(data);
              });
            }
          }).bind(this);
    }
    
   
    _updateInvestor(investor) {
        $("#input-update-name").val(investor.name);
        $("#input-update-desc").val(investor.description);
        $("#input-update-project").val(investor.project);
        $("#input-update-address").val(investor.address);
        $("#input-update-phone").val(investor.phone);
        $("#input-update-phone2").val(investor.phone2);
        $("#input-update-fax").val(investor.fax);
        $("#input-update-email").val(investor.email);
        $("#input-update-website").val(investor.website);
        $("#input-update-note").val(investor.note);
        $("#btn-update-investor").on("click", () => {
          this._clearInValid()
      
          let isValid = true;
          const vFields = [            "input-update-name",
            "input-update-address",
            "input-update-phone",
          ];
      
          vFields.forEach((field) => {
            const value = $(`#${field}`).val().trim();
            if (!value) {
              isValid = false;
              $(`#${field}`).addClass("is-invalid");
              $(`#${field}`).after(`<div class="invalid-feedback error-message">Vui lòng nhập và giá trị hợp lệ!.</div>`);
            }
          });
      
          if (isValid) {
            const investorData = {
              name: $("#input-update-name").val().trim(),
              description: $("#input-update-desc").val().trim(),
              projects: $("#input-update-project").val().trim(),
              address: $("#input-update-address").val().trim(),
              phone: $("#input-update-phone").val().trim(),
              phone2: $("#input-update-phone2").val().trim(),
              fax: $("#input-update-fax").val().trim(),
              email: $("#input-update-email").val().trim(),
              website: $("#input-update-website").val().trim(),
              note: $("#input-update-note").val().trim(),
            };
      
            this.vApi.onUpdateInvestorClick(investor.id,investorData, (data) => {
              console.log(data);
            });
          }
        }).bind(this);
      }

    _deleteInvestor(data) {
        $("#btn-confirm-delete-investor").on("click", () => {
          this.vApi.onDeleteInvestorClick(data.id)
        })

    }
    _clearInValid(input) {
        if (input) {
            input.removeClass("is-invalid");
            $(".invalid-feedback").remove();
        }
        $(".form-control").removeClass("is-invalid");
        $(".invalid-feedback").remove();
    }

    _clearInput() {
        $("#create-investor-form").find("input").val("");
        $("#create-investor-form").find("select").val("")
    }
    openModal(type, data) {
        if (type === "create") {
            this._clearInput()
            this._onEventListner()
            this._createInvestor()
        } else {
            if (type === "edit") {
                this._updateInvestor(data)
                $("#update-investor-modal").modal("show")
            } else {
                this._deleteInvestor(data)
                $("#delete-confirm-modal").modal("show")
            }
        }

    }
}

/*** REGION 4 - vùng để gọi lên cơ sở dữ liệu lấy đa ta về*/
class CallApi {
    constructor() {

    }
    onShowToast(paramTitle, paramMessage) {
        $('#myToast .mr-auto').text(paramTitle)
        $('#myToast .toast-body').text(paramMessage);
        $('#myToast').toast('show');
    }

    onGetInvestorsClick(paramCallbackFn) {
        $.ajax({
            url: ginvestor_URL,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onExportExcelClick(paramCallbackFn) {
        $.ajax({
            url: `http://localhost:8080/api/export/investors/excel`,
            method: 'GET',
            xhrFields: {
                responseType: "blob"
              },
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    onGetInvestorByIdClick(investorId, paramCallbackFn) {
        $.ajax({
            url: ginvestor_URL + "/" + investorId,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onCreateInvestorClick(investorData, paramCallbackFn) {
        $.ajax({
            url: ginvestor_URL,
            method: 'POST',
            data: JSON.stringify(investorData),
            contentType: 'application/json',
            success: (data) => {
                paramCallbackFn(data);
                const render = new RenderPage()
                render.renderPage()
                this.onShowToast("Tạo thành công", "Tạo investor thành công!!")
                $("#create-investor-modal").modal("hide");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown, textStatus);
            }
        });
    }
    onUpdateInvestorClick(investorId, investorData, paramCallbackFn) {
        $.ajax({
            url: ginvestor_URL + "/" + investorId,
            method: 'PUT',
            data: JSON.stringify(investorData),
            contentType: 'application/json',
            success: (data) => {
                const render = new RenderPage()
                render.renderPage()
                paramCallbackFn(data);
                $('#update-investor-modal').modal('hide');
                this.onShowToast("Sửa thành công", "Sửa investor thành công!!")
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    onDeleteInvestorClick(investorId) {
        $.ajax({
            url: ginvestor_URL + "/" + investorId,
            method: 'DELETE',
            success:  () => {
                this.onShowToast("Xóa thành công", "bạn đã xóa investor thành công!!")
                const render = new RenderPage()
                render.renderPage()
                $("#delete-confirm-modal").modal("hide");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    
}