/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gdistrict_URL = "http://localhost:8080/api/districts";

const gCOLUMN_ID = {
    stt: 0,
    name: 1,
    prefix: 2,
    province:3,
    action: 4

}
const gCOL_NAME = [
    "stt",
    "name",
    "prefix",
    "province"

]
//Hàm chính để load html hiển thị ra bảng
class Main {
    constructor() {
        $(document).ready(() => {
            this.vOrderList = new RenderPage()
            this.vOrderList.renderPage()
            this.vModal = new Modal()
            this.vModal.openModal("create")
            $('.select2').select2()
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })
        })
    }

}
new Main()
/*** REGION 2 - vùng để render ra bảng*/
class RenderPage {
    constructor() {
        this.vApi = new CallApi()
        this.vModal = new Modal()

    }

    //Hàm gọi api lấy danh sách đơn hàng
    _getDistrictList() {
        this.vApi.onGetDistrictsAndProvince((paramDistrict) => {
            this._createDistrictTable(paramDistrict)
        })
    }

    //Hàm tạo các thành phần của bảng
    _createDistrictTable(paramDistrict) {
        let stt = 1
        if ($.fn.DataTable.isDataTable('#table-district')) {
            $('#table-district').DataTable().destroy();
        }
        const vOrderTable = $("#table-district").DataTable({
            // Khai báo các cột của datatable
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false,
            "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],
            "columns": [
                { "data": null },
                { "data": gCOL_NAME[gCOLUMN_ID.name] },
                { "data": gCOL_NAME[gCOLUMN_ID.prefix] },
                { "data": gCOL_NAME[gCOLUMN_ID.province] },
                { "data": gCOL_NAME[gCOLUMN_ID.action] },

            ],
            // Ghi đè nội dung của cột action
            "columnDefs": [
                {
                    targets: gCOLUMN_ID.stt,
                    render: function () {
                        return stt++
                    }
                }, {

                    targets: gCOLUMN_ID.action,
                    defaultContent: `
                              <img class="edit-district" src="https://cdn0.iconfinder.com/data/icons/glyphpack/45/edit-alt-512.png" style="width: 20px;cursor:pointer;">
                              <img class="delete-district" src="https://cdn4.iconfinder.com/data/icons/complete-common-version-6-4/1024/trash-512.png" style="width: 20px;cursor:pointer;">
                            `,
                    createdCell: (cell, cellData, rowData, rowIndex, colIndex) => {
                        if (colIndex === gCOLUMN_ID.action) {
                            $(cell).find('.edit-district').on('click', () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                this.vModal.openModal('edit', vData);
                            });

                            $(cell).find('.delete-district').on('click', () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                console.log(vData)
                                this.vModal.openModal('delete', vData);
                            });
                        }
                    }
                }],

        });
        vOrderTable.clear() // xóa toàn bộ dữ liệu trong bảng
        vOrderTable.rows.add(paramDistrict) // cập nhật dữ liệu cho bảng
        vOrderTable.draw()// hàm vẻ lại bảng
        vOrderTable.buttons().container().appendTo('.example1_wrapper .col-md-6:eq(0)')
    }

    _saveExcelFile(data, filename) {
        const blob = new Blob([data], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
        const url = window.URL.createObjectURL(blob);
        const link = document.createElement("a");
        link.href = url;
        link.download = filename;
        link.click();
      }
      
    _exportToExcel() {
        $("#btn-export-excel").on("click",()=> {
            this.vApi.onExportExcelClick((data)=> {
                console.log(data)
              this._saveExcelFile(data, "district.xlsx");
            });
        });
    }


    // Hàm sẽ được gọi ở class Main 
    renderPage() {
        this._exportToExcel()
        this._getDistrictList()
    }
}

/*** REGION 3 - vùng hiện modal*/

class Modal {
    constructor() {
        this.vApi = new CallApi()
    }

    _onEventListner() {
        $("#btn-add-district").on("click", () => {
            $("#create-district-modal").modal("show")
            this._renderPhotoList()
        })
    }

    _createDistrict() {
        this._clearInput();
        $("#btn-create-district").on("click", () => {
          this._clearInValid();
      
          let isValid = true;
          const vFields = [
            "input-create-checkNumber",
            "input-create-districtDate",
            "input-create-amount"
          ];
      
          vFields.forEach((field) => {
            const value = $(`#${field}`).val().trim();
            if (!value) {
              isValid = false;
              $(`#${field}`).addClass("is-invalid");
              $(`#${field}`).after(`<div class="invalid-feedback error-message">Vui lòng nhập và giá trị hợp lệ!.</div>`);
            }
          });
      
          if (isValid) {
            const districtData = {
              checkNumber: $("#input-create-checkNumber").val().trim(),
              districtDate: $("#input-create-districtDate").val().trim(),
              ammount: parseFloat($("#input-create-amount").val().trim())
            };
            this.vApi.onCreatedistrictClick(districtData, (data) => {
              console.log(data);
            });
          }
        }).bind(this);
      }
      
      _updateDistrict(district) {
        $("#input-update-checkNumber").val(district.checkNumber);
        $("#input-update-districtDate").val(district.districtDate);
        $("#input-update-amount").val(district.ammount);
      
        $("#btn-update-district").off("click").on("click", () => {
          this._clearInValid();
          let isValid = true;
          const requiredFields = ["input-update-checkNumber", "input-update-districtDate", "input-update-amount"];
      
          requiredFields.forEach((field) => {
            if (!$(`#${field}`).val().trim()) {
              isValid = false;
              $(`#${field}`).addClass("is-invalid");
              $(`#${field}`).after('<div class="invalid-feedback">Vui lòng nhập giá trị hợp lệ!</div>');
            }
          });
      
          if (isValid) {
            const updateddistrict = {
              checkNumber: $("#input-update-checkNumber").val().trim(),
              districtDate: $("#input-update-districtDate").val().trim(),
              ammount: parseFloat($("#input-update-amount").val().trim())
            };
      
            this.vApi.onUpdateDistrictClick(district.id, updateddistrict, (data) => {
              console.log(data);
            });
          }
        }).bind(this);
      }
      

    _deleteDistrict(data) {
        $("#btn-confirm-delete-district").on("click", () => {
            this.vApi.onDeleteDistrictClick(data.id)
        })

    }
    _clearInValid(input) {
        if (input) {
            input.removeClass("is-invalid");
            $(".invalid-feedback").remove();
        }
        $(".form-control").removeClass("is-invalid");
        $(".invalid-feedback").remove();
    }

    _clearInput() {
        $("#create-district-form").find("input").val("");
        $("#create-district-form").find("select").val("")
    }
    openModal(type, data) {
        if (type === "create") {
            this._clearInput()
            this._onEventListner()
            this._createDistrict()
        } else {
            if (type === "edit") {
                this._updateDistrict(data)
                $("#update-district-modal").modal("show")
            } else {
                this._deleteDistrict(data)
                $("#delete-confirm-modal").modal("show")
            }
        }

    }
}

/*** REGION 4 - vùng để gọi lên cơ sở dữ liệu lấy đa ta về*/
class CallApi {
    constructor() {

    }
    onShowToast(paramTitle, paramMessage) {
        $('#myToast .mr-auto').text(paramTitle)
        $('#myToast .toast-body').text(paramMessage);
        $('#myToast').toast('show');
    }


    onGetDistrictsAndProvince(paramCallbackFn) {
        $.ajax({
            url: gdistrict_URL + "/province" ,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    onGetDistrictByIdClick(districtId, paramCallbackFn) {
        $.ajax({
            url: gdistrict_URL + "/" + districtId,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onCreatedistrictClick(districtData, paramCallbackFn) {
        $.ajax({
            url: gdistrict_URL,
            method: 'POST',
            data: JSON.stringify(districtData),
            contentType: 'application/json',
            success: (data) => {
                paramCallbackFn(data);
                const render = new RenderPage()
                render.renderPage()
                this.onShowToast("Tạo thành công", "Tạo district thành công!!")
                $("#create-district-modal").modal("hide");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown, textStatus);
            }
        });
    }
    onUpdateDistrictClick(districtId, districtData, paramCallbackFn) {
        $.ajax({
            url: gdistrict_URL + "/" + districtId,
            method: 'PUT',
            data: JSON.stringify(districtData),
            contentType: 'application/json',
            success: (data) => {
                const render = new RenderPage()
                render.renderPage()
                paramCallbackFn(data);
                $('#update-district-modal').modal('hide');
                this.onShowToast("Sửa thành công", "Sửa district thành công!!")
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown); 
            }
        });
    }
    onDeleteDistrictClick(districtId) {
        $.ajax({
            url: gdistrict_URL + "/" + districtId,
            method: 'DELETE',
            success:  () =>{
                this.onShowToast("Xóa thành công", "bạn đã xóa district thành công!!")
                const render = new RenderPage()
                render.renderPage()
                $("#delete-confirm-modal").modal("hide");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    onExportExcelClick(paramCallbackFn) {
        $.ajax({
            url: `http://localhost:8080/api/export/districts/excel`,
            method: 'GET',
            xhrFields: {
                responseType: "blob"
              },
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
}