/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gPRODUCT_URL = "http://localhost:8080/api/products";
const gCOLUMN_ID = {
    stt: 0,
   productCode: 1,
   productName: 2,
    productDescription: 3,
    productScale: 4,
    productVendor: 5,
    quantityInStock:6,
    buyPrice:7,
    action:8

}
const gCOL_NAME = [
    "stt",
    "productCode",
    "productName",
    "productDescription",
    "productScale",
    "productVendor",
    "quantityInStock",
    "buyPrice"

]
//Hàm chính để load html hiển thị ra bảng
class Main {
    constructor() {
        $(document).ready(() => {
            this.vOrderList = new RenderPage()
            this.vOrderList.renderPage()
            this.vModal = new Modal()
            this.vModal.openModal("create")
            $('.select2').select2()
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })
        })
    }

}
new Main()
/*** REGION 2 - vùng để render ra bảng*/
class RenderPage {
    constructor() {
        this.vApi = new CallApi()
        this.vModal = new Modal()

    }

    //Hàm gọi api lấy danh sách đơn hàng
    _getproductList() {
        this.vApi.onGetProductsClick((paramproduct) => {
            this._createproductTable(paramproduct)
        })
    }

    //Hàm tạo các thành phần của bảng
    _createproductTable(paramproduct) {
        let stt = 1
        if ($.fn.DataTable.isDataTable('#table-product')) {
            $('#table-product').DataTable().destroy();
        }
        const vOrderTable = $("#table-product").DataTable({
            // Khai báo các cột của datatable
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false,
            "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],
            "columns": [
                { "data": null },
                { "data": gCOL_NAME[gCOLUMN_ID.productCode] },
                { "data": gCOL_NAME[gCOLUMN_ID.productName] },
                {
                    data: gCOL_NAME[gCOLUMN_ID.productDescription],
                    render: function (data, type, row) {
                      if (type === "display" && data.length > 50) {
                        return data.substr(0, 50) + "...";
                      }
                      return data;
                    },
                  },
                { "data": gCOL_NAME[gCOLUMN_ID.productScale] },
                { "data": gCOL_NAME[gCOLUMN_ID.productVendor] },
                { "data": gCOL_NAME[gCOLUMN_ID.quantityInStock] },
                { "data": gCOL_NAME[gCOLUMN_ID.buyPrice] },
                { "data": gCOL_NAME[gCOLUMN_ID.action] },


            ],
            // Ghi đè nội dung của cột action
            "columnDefs": [
                {
                    targets: gCOLUMN_ID.photos,
                    render: function (data) {
                        let photoNames = '';
                        if (Array.isArray(data)) {
                            data.forEach((photo, index) => {
                                photoNames += photo.photoName;
                                if (index < data.length - 1) {
                                    photoNames += ', ';
                                }
                            });
                        }
                        return photoNames;
                    }
                },

                {
                    targets: gCOLUMN_ID.stt,
                    render: function () {
                        return stt++
                    }
                }, {

                    targets: gCOLUMN_ID.action,
                    defaultContent: `
                              <img class="edit-product" src="https://cdn0.iconfinder.com/data/icons/glyphpack/45/edit-alt-512.png" style="width: 20px;cursor:pointer;">
                              <img class="delete-product" src="https://cdn4.iconfinder.com/data/icons/complete-common-version-6-4/1024/trash-512.png" style="width: 20px;cursor:pointer;">
                            `,
                    createdCell: (cell, cellData, rowData, rowIndex, colIndex) => {
                        if (colIndex === gCOLUMN_ID.action) {
                            $(cell).find('.edit-product').on('click', () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                this.vModal.openModal('edit', vData);
                            });

                            $(cell).find('.delete-product').on('click', () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                console.log(vData)
                                this.vModal.openModal('delete', vData);
                            });
                        }
                    }
                }],

        });
        vOrderTable.clear() // xóa toàn bộ dữ liệu trong bảng
        vOrderTable.rows.add(paramproduct) // cập nhật dữ liệu cho bảng
        vOrderTable.draw()// hàm vẻ lại bảng
        vOrderTable.buttons().container().appendTo('.example1_wrapper .col-md-6:eq(0)')
    }

    _saveExcelFile(data, filename) {
        const blob = new Blob([data], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
        const url = window.URL.createObjectURL(blob);
        const link = document.createElement("a");
        link.href = url;
        link.download = filename;
        link.click();
      }
      
    _exportToExcel() {
        $("#btn-export-excel").on("click",()=> {
            this.vApi.onExportExcelClick((data)=> {
                console.log(data)
              this._saveExcelFile(data, "product.xlsx");
            });
        });
    }


    // Hàm sẽ được gọi ở class Main 
    renderPage() {
        this._exportToExcel()
        this._getproductList()
    }
}

/*** REGION 3 - vùng hiện modal*/

class Modal {
    constructor() {
        this.vApi = new CallApi()
    }

    _onEventListner() {
        $("#btn-add-product").on("click", () => {
            $("#create-product-modal").modal("show")
        })
    }


    _createProduct() {
        this._clearInput();
        $("#btn-create-product").on("click", () => {
            this._clearInValid();

          let isValid = true;
    
          const vFields = [
            "input-create-productCode",
            "input-create-name",
            "input-create-desc",
            "input-create-scale",
            "input-create-vendor",
            "input-create-qty",
            "input-create-buyPrice",
          ];
      
          vFields.forEach((field) => {
            const fieldValue = $(`#${field}`).val().trim();
      
            if (!fieldValue || fieldValue === "") {
              isValid = false;
              $(`#${field}`).addClass("is-invalid");
              $(`#${field}`).after(`<div class="invalid-feedback error-message">Vui lòng nhập và giá trị hợp lệ!.</div>`);
            }
          });
      
          if (isValid) {
            const productData = {
              productCode: $("#input-create-productCode").val().trim(),
              productName: $("#input-create-name").val().trim(),
              productDescription: $("#input-create-desc").val().trim(),
              productScale: $("#input-create-scale").val().trim(),
              productVendor: $("#input-create-vendor").val().trim(),
              quantityInStock: parseInt($("#input-create-qty").val().trim()),
              buyPrice: parseFloat($("#input-create-buyPrice").val().trim()),
              orderDetails:[]
            };
      
            this.vApi.onCreateProductClick(productData, (responseData) => {
                console.log(responseData);
              });
 
          }
        }).bind(this);
      }
      
      _updateProduct(data) {
        this._clearInput();
        $("#input-update-productCode").val(data.productCode);
        $("#input-update-name").val(data.productName);
        $("#input-update-desc").val(data.productDescription);
        $("#input-update-scale").val(data.productScale);
        $("#input-update-vendor").val(data.productVendor);
        $("#input-update-qty").val(data.quantityInStock);
        $("#input-update-buyPrice").val(data.buyPrice);
      
        $("#btn-update-product").off("click").on("click", () => {
            this._clearInValid();
      
          let isValid = true;
          const requiredFields = [
            "input-update-productCode",
            "input-update-name",
            "input-update-desc",
            "input-update-scale",
            "input-update-vendor",
            "input-update-qty",
            "input-update-buyPrice",
          ];
      
          requiredFields.forEach((field) => {
            if (!$(`#${field}`).val().trim()) {
              isValid = false;
              $(`#${field}`).addClass("is-invalid");
              $(`#${field}`).after('<div class="invalid-feedback">Vui lòng nhập giá trị hợp lệ!</div>');
            }
          });
      
          if (isValid) {
            const updatedProduct = {
              productCode: $("#input-update-productCode").val().trim(),
              productName: $("#input-update-name").val().trim(),
              productDescription: $("#input-update-desc").val().trim(),
              productScale: $("#input-update-scale").val().trim(),
              productVendor: $("#input-update-vendor").val().trim(),
              quantityInStock: parseInt($("#input-update-qty").val()),
              buyPrice: parseFloat($("#input-update-buyPrice").val()),
              orderDetails:[],
            };
      
            this.vApi.onUpdateProductClick(data.id, updatedProduct, (responseData) => {
                console.log(responseData);
              });
         
          }
        }).bind(this);
      }
      
      

    _deleteProduct(data) {
        $("#btn-confirm-delete-product").on("click", () => {
            this.vApi.onDeleteProductClick(data.id)
        })

    }
    _clearInValid(input) {
        if (input) {
            input.removeClass("is-invalid");
            $(".invalid-feedback").remove();
        }
        $(".form-control").removeClass("is-invalid");
        $(".invalid-feedback").remove();
    }

    _clearInput() {
        $("#create-product-form").find("input").val("");
        $("#create-product-form").find("select").val("")
    }
    openModal(type, data) {
        if (type === "create") {
            this._clearInput()
            this._onEventListner()
            this._createProduct()
        } else {
            if (type === "edit") {
                this._updateProduct(data)
                $("#update-product-modal").modal("show")
            } else {
                this._deleteProduct(data)
                $("#delete-confirm-modal").modal("show")
            }
        }

    }
}

/*** REGION 4 - vùng để gọi lên cơ sở dữ liệu lấy đa ta về*/
class CallApi {
    constructor() {

    }
    onShowToast(paramTitle, paramMessage) {
        $('#myToast .mr-auto').text(paramTitle)
        $('#myToast .toast-body').text(paramMessage);
        $('#myToast').toast('show');
    }
    onGetPhotosClick(paramCallbackFn) {
        $.ajax({
            url: gPHOTO_URL,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onGetPhotoByIdClick(photoId, paramCallbackFn) {
        $.ajax({
            url: gPHOTO_URL + "/" + photoId,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onGetProductsClick(paramCallbackFn) {
        $.ajax({
            url: gPRODUCT_URL,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    onGetProductByIdClick(productId, paramCallbackFn) {
        $.ajax({
            url: gPRODUCT_URL + "/" +productId,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onCreateProductClick(productData, paramCallbackFn) {
        $.ajax({
            url: gPRODUCT_URL,
            method: 'POST',
            data: JSON.stringify(productData),
            contentType: 'application/json',
            success: (data) => {
                paramCallbackFn(data);
                const render = new RenderPage()
                render.renderPage()
                this.onShowToast("Tạo thành công", "Tạoproduct thành công!!")
                $("#create-product-modal").modal("hide");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown, textStatus);
            }
        });
    }
    onUpdateProductClick(productId,productData, paramCallbackFn) {
        $.ajax({
            url: gPRODUCT_URL + "/" +productId,
            method: 'PUT',
            data: JSON.stringify(productData),
            contentType: 'application/json',
            success: (data) => {
                const render = new RenderPage()
                render.renderPage()
                paramCallbackFn(data);
                $('#update-product-modal').modal('hide');
                this.onShowToast("Sửa thành công", "Sửaproduct thành công!!")
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    onDeleteProductClick(productId) {
        $.ajax({
            url: gPRODUCT_URL + "/" +productId,
            method: 'DELETE',
            success:  ()=> {
                this.onShowToast("Xóa thành công", "bạn đã xóa product thành công!!")
                const render = new RenderPage()
                render.renderPage()
                $("#delete-confirm-modal").modal("hide");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onExportExcelClick(paramCallbackFn) {
        $.ajax({
            url: `http://localhost:8080/api/export/products/excel`,
            method: 'GET',
            xhrFields: {
                responseType: "blob"
              },
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
}