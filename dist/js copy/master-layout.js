/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gMASTERLAYOUT_URL = "http://localhost:8080/api/master-layouts";
const gPROJECT_URL = "http://localhost:8080/api/projects";
const gCOLUMN_ID = {
    stt: 0,
    name: 1,
    description: 2,
    projectId: 3,
    acreage: 4,
    dateCreate: 5,
    dateUpdate: 6,
    action: 7
  };
  const gCOL_NAME = [
    "stt",
    "name",
    "description",
    "projectId",
    "acreage",
    "dateCreate",
    "dateUpdate",
  ];
//Hàm chính để load html hiển thị ra bảng
class Main {
    constructor() {
        $(document).ready(() => {
            this.vOrderList = new RenderPage()
            this.vOrderList.renderPage()
            this.vModal = new Modal()
            this.vModal.openModal("create")
            $('.select2').select2()
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })
        })
    }

}
new Main()
/*** REGION 2 - vùng để render ra bảng*/
class RenderPage {
    constructor() {
        this.vApi = new CallApi()
        this.vModal = new Modal()

    }

    //Hàm gọi api lấy danh sách đơn hàng
    _getMasterLayoutList() {
        this.vApi.onGetMasterLayoutsClick((parammasterLayout) => {
            console.log(parammasterLayout)
            this._createmasterLayoutTable(parammasterLayout)
        })
    }

    //Hàm tạo các thành phần của bảng
    _createmasterLayoutTable(parammasterLayout) {
        let stt = 1
        if ($.fn.DataTable.isDataTable('#table-masterLayout')) {
            $('#table-masterLayout').DataTable().destroy();
        }
        const vOrderTable = $("#table-masterLayout").DataTable({
            // Khai báo các cột của datatable
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false,
            "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],
            "columns": [
                { "data": null },
                { "data": gCOL_NAME[gCOLUMN_ID.name] },
                { "data": gCOL_NAME[gCOLUMN_ID.description] },
                { "data": gCOL_NAME[gCOLUMN_ID.projectId] },
                { "data": gCOL_NAME[gCOLUMN_ID.acreage] },
                { "data": gCOL_NAME[gCOLUMN_ID.dateCreate] },
                { "data": gCOL_NAME[gCOLUMN_ID.dateUpdate] },
                { "data": gCOL_NAME[gCOLUMN_ID.action] },

            ],
            // Ghi đè nội dung của cột action
            "columnDefs": [
                {
                    targets: gCOLUMN_ID.stt,
                    render: function () {
                        return stt++
                    }
                }, {

                    targets: gCOLUMN_ID.action,
                    defaultContent: `
                              <img class="edit-masterLayout" src="https://cdn0.iconfinder.com/data/icons/glyphpack/45/edit-alt-512.png" style="width: 20px;cursor:pointer;">
                              <img class="delete-masterLayout" src="https://cdn4.iconfinder.com/data/icons/complete-common-version-6-4/1024/trash-512.png" style="width: 20px;cursor:pointer;">
                            `,
                    createdCell: (cell, cellData, rowData, rowIndex, colIndex) => {
                        if (colIndex === gCOLUMN_ID.action) {
                            $(cell).find('.edit-masterLayout').on('click', () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                this.vModal.openModal('edit', vData);
                            });

                            $(cell).find('.delete-masterLayout').on('click', () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                console.log(vData)
                                this.vModal.openModal('delete', vData);
                            });
                        }
                    }
                }],

        });
        vOrderTable.clear() // xóa toàn bộ dữ liệu trong bảng
        vOrderTable.rows.add(parammasterLayout) // cập nhật dữ liệu cho bảng
        vOrderTable.draw()// hàm vẻ lại bảng
        vOrderTable.buttons().container().appendTo('.example1_wrapper .col-md-6:eq(0)')
        $('.table-container').css('overflow-x', 'auto');
    }

    _saveExcelFile(data, filename) {
        const blob = new Blob([data], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
        const url = window.URL.createObjectURL(blob);
        const link = document.createElement("a");
        link.href = url;
        link.download = filename;
        link.click();
      }
      
    _exportToExcel() {
        $("#btn-export-excel").on("click",()=> {
            this.vApi.onExportExcelClick((data)=> {
                console.log(data)
              this._saveExcelFile(data, "product_line.xlsx");
            });
        });
    }

    // Hàm sẽ được gọi ở class Main 
    renderPage() {
        this._exportToExcel()
        this._getMasterLayoutList()
    }
}

/*** REGION 3 - vùng hiện modal*/

class Modal {
    constructor() {
        this.vApi = new CallApi()
    }

    _onEventListner() {
        $("#btn-add-masterLayout").on("click", () => {
            $("#create-masterLayout-modal").modal("show")
  
        })
    }
    _getProjectList(type, callback) {
        let projectDropdown;
        if (type === "update") {
          projectDropdown = $("#input-update-projectId");
        } else {
          projectDropdown = $("#input-create-projectId");
        }
      
        this.vApi.onGetProjectsClick((projects) => {
          projectDropdown.empty();
          projectDropdown.append('<option value="" selected>Chọn</option>');
      
          projects.forEach((project) => {
            const option = `<option value="${project.id}">${project.name}</option>`;
            projectDropdown.append(option);
          });
      
          if (typeof callback === "function") {
            callback();
          }
        });
      }
      
      _updateMasterLayout(masterLayout) {
        this._getProjectList("update", () => {
          // Show values in the input fields
          $('#input-update-name').val(masterLayout.name);
          $('#input-update-description').val(masterLayout.description);
          $('#input-update-acreage').val(masterLayout.acreage);
          $('#input-update-apartmentList').val(masterLayout.apartmentList);
      
          // Set the selected value for the projectId dropdown
          $('#input-update-projectId').val(masterLayout.projectId);
        });
      
        $("#btn-update-masterLayout").on("click", () => {
          // Rest of the code...
        }).bind(this);
      }
      
    _createMasterLayout() {
        this._clearInput()
        this._getProjectList()
        $("#btn-create-masterLayout").on("click", () => {
 
          this._clearInValid()
          let isValid = true;
          const vFields = [
            "input-create-name",
            "input-create-projectId",
            "input-create-acreage",
          ];
      
          vFields.forEach((field) => {
            const value = $(`#${field}`).val().trim();
            if (!value) {
              isValid = false;
              $(`#${field}`).addClass("is-invalid");
              $(`#${field}`).after(`<div class="invalid-feedback error-message">Please enter a valid value!</div>`);
            }
          });
      
          if (isValid) {
            const masterLayoutData = {
              name: $("#input-create-name").val().trim(),
              description: $("#input-create-description").val().trim(),
              projectId: $("#input-create-projectId").val().trim(),
              acreage: $("#input-create-acreage").val().trim(),
              apartmentList: $("#input-create-apartmentList").val().trim()
            };
      
            // Handle file input for photos
            const fileInput = document.getElementById("input-create-photo");
            if (fileInput.files.length > 0) {
              const photos = [];
              for (let i = 0; i < fileInput.files.length; i++) {
                photos.push(fileInput.files[i]);
              }
              masterLayoutData.photos = photos;
            }
      
            this.vApi.onCreateMasterLayoutClick(masterLayoutData, (data) => {
              console.log(data);
            });
          }
        }).bind(this);
      }
      
      
      _updateMasterLayout(masterLayout) {
        this._getProjectList("update", () => {
            // Show values in the input fields
            $('#input-update-name').val(masterLayout.name);
            $('#input-update-description').val(masterLayout.description);
            $('#input-update-acreage').val(masterLayout.acreage);
            $('#input-update-apartmentList').val(masterLayout.apartmentList);
        
            // Set the selected value for the projectId dropdown
            $('#input-update-projectId').val(masterLayout.projectId);
          });
       
      
        $("#btn-update-masterLayout").off("click").on("click", () => {
          this._clearInValid();
          let isValid = true;
          const vFields = [
            "input-update-name",
          ];
      
          vFields.forEach((field) => {
            const value = $(`#${field}`).val().trim();
            if (!value) {
              isValid = false;
              $(`#${field}`).addClass("is-invalid");
              $(`#${field}`).after(`<div class="invalid-feedback error-message">Vui lòng nhập và giá trị hợp lệ!</div>`);
            }
          });
      
          if (isValid) {
            const masterLayoutData = {
              name: $("#input-update-name").val().trim(),
              description: $("#input-update-description").val().trim(),
              projectId: $("#input-update-projectId").val().trim(),
              acreage: $("#input-update-acreage").val().trim(),
              apartmentList: $("#input-update-apartmentList").val().trim(),
              // Add other data fields here as needed
            };
      
            this.vApi.onUpdateMasterLayoutClick(masterLayout.id, masterLayoutData, (data) => {
              console.log(data);
            });
          }
        }).bind(this);
      }
      
      
    _deleteMasterLayout(data) {
        $("#btn-confirm-delete-masterLayout").off("click").on("click", () => {
            this.vApi.onDeleteMasterLayoutClick(data.id)
        })

    }
    _clearInValid(input) {
        if (input) {
            input.removeClass("is-invalid");
            $(".invalid-feedback").remove();
        }
        $(".form-control").removeClass("is-invalid");
        $(".invalid-feedback").remove();
    }

    _clearInput() {
        $("#create-masterLayout-form").find("input").val("");
        $("#create-masterLayout-form").find("select").val("")
    }
    openModal(type, data) {
        if (type === "create") {
            this._clearInput()
            this._onEventListner()
            this._createMasterLayout()
        } else {
            if (type === "edit") {
                this._updateMasterLayout(data)
                $("#update-masterLayout-modal").modal("show")
            } else {
                this._deleteMasterLayout(data)
                $("#delete-confirm-modal").modal("show")
            }
        }

    }
}

/*** REGION 4 - vùng để gọi lên cơ sở dữ liệu lấy đa ta về*/
class CallApi {
    constructor() {

    }
    onShowToast(paramTitle, paramMessage) {
        $('#myToast .mr-auto').text(paramTitle)
        $('#myToast .toast-body').text(paramMessage);
        $('#myToast').toast('show');
    }

    onGetMasterLayoutsClick(paramCallbackFn) {
        $.ajax({
            url: gMASTERLAYOUT_URL,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    
    onGetProjectsClick(paramCallbackFn) {
        $.ajax({
            url: gPROJECT_URL,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    onGetMasterLayoutByIdClick(masterLayoutId, paramCallbackFn) {
        $.ajax({
            url: gMASTERLAYOUT_URL + "/" + masterLayoutId,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    

    onCreateMasterLayoutClick(masterLayoutData, paramCallbackFn) {
        $.ajax({
            url: gMASTERLAYOUT_URL,
            method: 'POST',
            data: JSON.stringify(masterLayoutData),
            contentType: 'application/json',
            success: (data) => {
                paramCallbackFn(data);
                const render = new RenderPage()
                render.renderPage()
                this.onShowToast("Tạo thành công", "Tạo masterLayout thành công!!")
                $("#create-masterLayout-modal").modal("hide");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown, textStatus);
            }
        });
    }
    onUpdateMasterLayoutClick(masterLayoutId, masterLayoutData, paramCallbackFn) {
        $.ajax({
            url: gMASTERLAYOUT_URL + "/" + masterLayoutId,
            method: 'PUT',
            data: JSON.stringify(masterLayoutData),
            contentType: 'application/json',
            success: (data) => {
                const render = new RenderPage()
                render.renderPage()
                paramCallbackFn(data);
                $('#update-masterLayout-modal').modal('hide');
                this.onShowToast("Sửa thành công", "Sửa masterLayout thành công!!")
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    onDeleteMasterLayoutClick(masterLayoutId) {
        $.ajax({
            url: gMASTERLAYOUT_URL + "/" + masterLayoutId,
            method: 'DELETE',
            success:  ()=> {
                this.onShowToast("Xóa thành công", "bạn đã xóa masterLayout thành công!!")
                const render = new RenderPage()
                render.renderPage()
                $("#delete-confirm-modal").modal("hide");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onExportExcelClick(paramCallbackFn) {
        $.ajax({
            url: `http://localhost:8080/api/export/product-lines/excel`,
            method: 'GET',
            xhrFields: {
                responseType: "blob"
              },
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
}