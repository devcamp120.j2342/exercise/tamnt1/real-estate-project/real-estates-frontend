/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gPROJECT_URL = "http://localhost:8080/api/projects";

const gCOLUMN_ID = {
    stt: 0,
    name: 1,
    provinceId: 2,
    districtId: 3,
    wardId: 4,
    streetId: 5,
    address: 6,
    slogan: 7,
    description: 8,
    acreage: 9,
    constructArea: 10,
    numBlock: 11,
    numFloors: 12,
    numApartment: 13,
    apartmentArea: 14,
    investor: 15,
    constructionContractor: 16,
    designUnit: 17,
    utilities: 18,
    regionLink: 19,
    photo: 20,
    latitude: 21,
    longitude: 22,
    action:23
};
const gCOL_NAME = [
    "stt",
    "name",
    "provinceId",
    "districtId",
    "wardId",
    "streetId",
    "address",
    "slogan",
    "description",
    "acreage",
    "constructArea",
    "numBlock",
    "numFloors",
    "numApartment",
    "apartmentArea",
    "investor",
    "constructionContractor",
    "designUnit",
    "utilities",
    "regionLink",
    "photo",
    "latitude",
    "longitude"
];
//Hàm chính để load html hiển thị ra bảng
class Main {
    constructor() {
        $(document).ready(() => {
            this.vOrderList = new RenderPage()
            this.vOrderList.renderPage()
            this.vModal = new Modal()
            this.vModal.openModal("create")
            $('.select2').select2()
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })
        })
    }

}
new Main()
/*** REGION 2 - vùng để render ra bảng*/
class RenderPage {
    constructor() {
        this.vApi = new CallApi()
        this.vModal = new Modal()

    }

    //Hàm gọi api lấy danh sách đơn hàng
    _getProjectList() {
        this.vApi.onGetProjectsClick((paramProject) => {
            this._createProjectTable(paramProject)
        })
    }

    //Hàm tạo các thành phần của bảng
    _createProjectTable(paramProject) {
        let stt = 1
        if ($.fn.DataTable.isDataTable('#table-project')) {
            $('#table-project').DataTable().destroy();
        }
        const vOrderTable = $("#table-project").DataTable({
            // Khai báo các cột của datatable
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false,
            "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],
            "columns": [
                { "data": null },
                { "data": gCOL_NAME[gCOLUMN_ID.name] },
                { "data": gCOL_NAME[gCOLUMN_ID.provinceId] },
                { "data": gCOL_NAME[gCOLUMN_ID.districtId] },
                { "data": gCOL_NAME[gCOLUMN_ID.wardId] },
                { "data": gCOL_NAME[gCOLUMN_ID.streetId] },
                { "data": gCOL_NAME[gCOLUMN_ID.address] },
                { "data": gCOL_NAME[gCOLUMN_ID.slogan] },
                { "data": gCOL_NAME[gCOLUMN_ID.description] },
                { "data": gCOL_NAME[gCOLUMN_ID.acreage] },
                { "data": gCOL_NAME[gCOLUMN_ID.constructArea] },
                { "data": gCOL_NAME[gCOLUMN_ID.numBlock] },
                { "data": gCOL_NAME[gCOLUMN_ID.numFloors] },
                { "data": gCOL_NAME[gCOLUMN_ID.numApartment] },
                { "data": gCOL_NAME[gCOLUMN_ID.apartmentArea] },
                { "data": gCOL_NAME[gCOLUMN_ID.investor] },
                { "data": gCOL_NAME[gCOLUMN_ID.constructionContractor] },
                { "data": gCOL_NAME[gCOLUMN_ID.designUnit] },
                { "data": gCOL_NAME[gCOLUMN_ID.utilities] },
                { "data": gCOL_NAME[gCOLUMN_ID.regionLink] },
                { "data": gCOL_NAME[gCOLUMN_ID.photo] },
                { "data": gCOL_NAME[gCOLUMN_ID.latitude] },
                { "data": gCOL_NAME[gCOLUMN_ID.longitude] },
                { "data": gCOL_NAME[gCOLUMN_ID.action] }
            ],
            // Ghi đè nội dung của cột action
            "columnDefs": [

                {
                    targets: gCOLUMN_ID.stt,
                    render: function () {
                        return stt++
                    }
                }, {

                    targets: gCOLUMN_ID.action,
                    defaultContent: `
                              <img class="edit-project" src="https://cdn0.iconfinder.com/data/icons/glyphpack/45/edit-alt-512.png" style="width: 20px;cursor:pointer;">
                              <img class="delete-project" src="https://cdn4.iconfinder.com/data/icons/complete-common-version-6-4/1024/trash-512.png" style="width: 20px;cursor:pointer;">
                            `,
                    createdCell: (cell, cellData, rowData, rowIndex, colIndex) => {
                        if (colIndex === gCOLUMN_ID.action) {
                            $(cell).find('.edit-project').on('click', () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                this.vModal.openModal('edit', vData);
                            });

                            $(cell).find('.delete-project').on('click', () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                console.log(vData)
                                this.vModal.openModal('delete', vData);
                            });
                        }
                    }
                }],

        });
        vOrderTable.clear() // xóa toàn bộ dữ liệu trong bảng
        vOrderTable.rows.add(paramProject) // cập nhật dữ liệu cho bảng
        vOrderTable.draw()// hàm vẻ lại bảng
        vOrderTable.buttons().container().appendTo('.example1_wrapper .col-md-6:eq(0)')
        $('.table-container').css('overflow-x', 'auto');
    }
    _saveExcelFile(data, filename) {
        const blob = new Blob([data], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
        const url = window.URL.createObjectURL(blob);
        const link = document.createElement("a");
        link.href = url;
        link.download = filename;
        link.click();
      }
      
    _exportToExcel() {
        $("#btn-export-excel").on("click",()=> {
            this.vApi.onExportExcelClick((data)=> {
                console.log(data)
              this._saveExcelFile(data, "order-detail.xlsx");
            });
        });
    }


    // Hàm sẽ được gọi ở class Main 
    renderPage() {
        this._exportToExcel()
        this._getProjectList()
    }
}

/*** REGION 3 - vùng hiện modal*/

class Modal {
    constructor() {
        this.vApi = new CallApi()
    }

    _onEventListner() {
        $("#btn-add-project").on("click", () => {
            $("#create-project-modal").modal("show")
        })
    }

    _createroject() {
        this._clearInput();
        $("#btn-create-project").on("click", () => {
          this._clearInValid();
      
          let isValid = true;
          const requiredFields = ["input-create-quantity", "input-create-price"];
      
          requiredFields.forEach((field) => {
            const value = $(`#${field}`).val().trim();
            if (!value) {
              isValid = false;
              $(`#${field}`).addClass("is-invalid");
              $(`#${field}`).after(`<div class="invalid-feedback error-message">Vui lòng nhập và giá trị hợp lệ!.</div>`);
            }
          });
      
          if (isValid) {
            const projectData = {
              quantityOrder: $("#input-create-quantity").val().trim(),
              priceEach: parseFloat($("#input-create-price").val().trim()),
            };
            this.vApi.onCreateProjectClick(projectData, (data) => {
              console.log(data);
            });
          }
        }).bind(this);
      }
      
      _updateProject(project) {
        $('#input-update-quantity').val(project.quantityOrder);
        $('#input-update-price').val(project.priceEach);
      
        $('#btn-update-project').off('click').on('click', () => {
          this._clearInValid();
          let isValid = true;
          const requiredFields = ['input-update-quantity', 'input-update-price'];
      
          requiredFields.forEach((field) => {
            if (!$(`#${field}`).val().trim()) {
              isValid = false;
              $(`#${field}`).addClass('is-invalid');
              $(`#${field}`).after('<div class="invalid-feedback">Vui lòng nhập giá trị hợp lệ!</div>');
            }
          });
      
          if (isValid) {
            const updatedproject = {
              quantityOrder: $('#input-update-quantity').val().trim(),
              priceEach: parseFloat($("#input-update-price").val().trim()),
            };
      
            this.vApi.onUpdateProjectClick(project.id, updatedproject, (data) => {
              console.log(data);
            });
          }
        }).bind(this);
      }
      

    _deleteProject(data) {
        $("#btn-confirm-delete-project").on("click", () => {
            this.vApi.onDeleteProjectClick(data.id)
        })

    }
    _clearInValid(input) {
        if (input) {
            input.removeClass("is-invalid");
            $(".invalid-feedback").remove();
        }
        $(".form-control").removeClass("is-invalid");
        $(".invalid-feedback").remove();
    }

    _clearInput() {
        $("#create-project-form").find("input").val("");
        $("#create-project-form").find("select").val("")
    }
    openModal(type, data) {
        if (type === "create") {
            this._clearInput()
            this._onEventListner()
            this._createroject()
        } else {
            if (type === "edit") {
                this._updateProject(data)
                $("#update-project-modal").modal("show")
            } else {
                this._deleteProject(data)
                $("#delete-confirm-modal").modal("show")
            }
        }

    }
}

/*** REGION 4 - vùng để gọi lên cơ sở dữ liệu lấy đa ta về*/
class CallApi {
    constructor() {

    }
    onShowToast(paramTitle, paramMessage) {
        $('#myToast .mr-auto').text(paramTitle)
        $('#myToast .toast-body').text(paramMessage);
        $('#myToast').toast('show');
    }
  

    onGetProjectsClick(paramCallbackFn) {
        $.ajax({
            url: gPROJECT_URL,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    onGetProjectByIdClick(projectId, paramCallbackFn) {
        $.ajax({
            url: gPROJECT_URL + "/" + projectId,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onCreateProjectClick(projectData, paramCallbackFn) {
        $.ajax({
            url: gPROJECT_URL,
            method: 'POST',
            data: JSON.stringify(projectData),
            contentType: 'application/json',
            success: (data) => {
                paramCallbackFn(data);
                const render = new RenderPage()
                render.renderPage()
                this.onShowToast("Tạo thành công", "Tạo project thành công!!")
                $("#create-project-modal").modal("hide");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown, textStatus);
            }
        });
    }
    onUpdateProjectClick(projectId, projectData, paramCallbackFn) {
        $.ajax({
            url: gPROJECT_URL + "/" + projectId,
            method: 'PUT',
            data: JSON.stringify(projectData),
            contentType: 'application/json',
            success: (data) => {
                const render = new RenderPage()
                render.renderPage()
                paramCallbackFn(data);
                $('#update-project-modal').modal('hide');
                this.onShowToast("Sửa thành công", "Sửa project thành công!!")
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    onDeleteProjectClick(projectId) {
        $.ajax({
            url: gPROJECT_URL + "/" + projectId,
            method: 'DELETE',
            success:  ()=> {
                this.onShowToast("Xóa thành công", "bạn đã xóa project thành công!!")
                const render = new RenderPage()
                render.renderPage()
                $("#delete-confirm-modal").modal("hide");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    onExportExcelClick(paramCallbackFn) {
        $.ajax({
            url: `http://localhost:8080/api/export/order-details/excel`,
            method: 'GET',
            xhrFields: {
                responseType: "blob"
              },
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
}