/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gUSER_URL = "http://localhost:8080/api/users";
const gUPLOAD_FILE_URL = "http://localhost:8080/api/upload";
const gCUSTOMER_URL = "http://localhost:8080/api/customers";
const gCOLUMN_ID = {
    stt: 0,
    action: 1,
    email: 2,
    username: 3,
    customerId: 4,
    roles: 5



}
const gCOL_NAME = [
    "id",
    "action",
    "email",
    "username",
    "customerId",
    "roles"


]
//Hàm chính để load html hiển thị ra bảng
class Main {
    constructor() {
        $(document).ready(() => {
            this.vOrderList = new RenderPage()
            this.vOrderList.renderPage()
            this.vModal = new Modal()
            this.vModal.openModal("create")
            $('.select2').select2()
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })
        })
    }

}
new Main()
/*** REGION 2 - vùng để render ra bảng*/
class RenderPage {
    constructor() {
        this.vApi = new CallApi()
        this.vModal = new Modal()

    }

    //Hàm gọi api lấy danh sách đơn hàng
    _getuserList() {
        this.vApi.onGetUsersClick((paramUser) => {
            this._createUserTable(paramUser)
        })
    }
    //Hiển thị tên sau khi login
    _showAdminName() {
        const name = JSON.parse(localStorage.getItem('login'));
        if (name.accessToken) {
            $(".profile-name").text(name?.username)

            if (!name.roles.includes("ROLE_ADMIN")) {
                $('.user-item').hide();
            }
        } else {
            this.vApi.redirectToLogin()
        }

    }


    //Hàm tạo các thành phần của bảng
    async _createUserTable(paramUser) {
        let customerData
        let stt = 1
        if ($.fn.DataTable.isDataTable('#table-user')) {
            $('#table-user').DataTable().destroy();
        }
        try {
            const customerPromises = paramUser.map((row) => this.vApi.onGetCustomerById(row.customerId));
            customerData = await Promise.all(
                customerPromises.map((promise) =>
                    Promise.resolve(promise).catch((error) => {

                        console.error("Error fetching customer data:", error);
                        return {};
                    })
                )
            );
        } catch (error) {
            console.log(error)
        }


        const vOrderTable = $("#table-user").DataTable({
            // Khai báo các cột của datatable
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false,
            "processing": true,
            "pageLength": 5,
            "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],
            "columns": [
                { "data": null },
                { "data": gCOL_NAME[gCOLUMN_ID.action] },
                { "data": gCOL_NAME[gCOLUMN_ID.email] },
                { "data": gCOL_NAME[gCOLUMN_ID.username] },
                { "data": gCOL_NAME[gCOLUMN_ID.customerId] },
                { "data": gCOL_NAME[gCOLUMN_ID.roles] },
            ],
            "createdRow": function (row, data, dataIndex) {
                // Update the cells with the correct datax`
                $(row).find('td:eq(4)').text(customerData[dataIndex]?.contactName || '');
            },
            // Ghi đè nội dung của cột action
            "columnDefs": [

                {
                    targets: gCOLUMN_ID.stt,
                    render: function (data, type, row, meta) {
                        // Increment the stt value for each row
                        return meta.row + 1;
                    }
                }, {

                    targets: gCOLUMN_ID.action,
                    defaultContent: `
                              <img class="edit-user" src="https://cdn0.iconfinder.com/data/icons/glyphpack/45/edit-alt-512.png" style="width: 20px;cursor:pointer;">
                              <img class="delete-user" src="https://cdn4.iconfinder.com/data/icons/complete-common-version-6-4/1024/trash-512.png" style="width: 20px;cursor:pointer;">
                            `,
                    createdCell: (cell, cellData, rowData, rowIndex, colIndex) => {
                        if (colIndex === gCOLUMN_ID.action) {
                            $(cell).find('.edit-user').on('click', () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                this.vModal.openModal('edit', vData);
                            });

                            $(cell).find('.delete-user').on('click', () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                console.log(vData)
                                this.vModal.openModal('delete', vData);
                            });
                        }
                    }
                }],

        });
        vOrderTable.clear() // xóa toàn bộ dữ liệu trong bảng
        vOrderTable.rows.add(paramUser) // cập nhật dữ liệu cho bảng
        vOrderTable.draw()// hàm vẻ lại bảng
        vOrderTable.buttons().container().appendTo('.example1_wrapper .col-md-6:eq(0)')
        $('.table-container').css('overflow-x', 'auto');
    }
    _saveExcelFile(data, filename) {
        const blob = new Blob([data], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
        const url = window.URL.createObjectURL(blob);
        const link = document.createElement("a");
        link.href = url;
        link.download = filename;
        link.click();
    }

    _exportToExcel() {
        $("#btn-export-excel").on("click", () => {
            this.vApi.onExportExcelClick((data) => {
                console.log(data)
                this._saveExcelFile(data, "user.xlsx");
            });
        });
    }


    // Hàm sẽ được gọi ở class Main 
    renderPage() {
        this._showAdminName();
        this._exportToExcel();
        this._getuserList();
    }
}

/*** REGION 3 - vùng hiện modal*/

class Modal {
    constructor() {
        this.vApi = new CallApi()
    }

    _onEventListner() {
        $("#btn-add-user").on("click", () => {
            $("#create-user-modal").modal("show")

        })
        $(".logout").on("click", () => {
            $("#logout-confirm-modal").modal("show")
        })

    }

    _logout() {
        $("#btn-confirm-logout").on("click", () => {
            this.vApi.redirectToLogin()
        })

    }
    _getCustomerList(type, name) {
        let customerDropdown
        if (type === "update") {
            customerDropdown = $("#input-update-customer");
        } else {
            customerDropdown = $("#input-create-customer");
        }
        this.vApi.onGetCustomerList((customers) => {
            customerDropdown.empty();
            customerDropdown.append(`<option value="0" selected>${name|| "Chọn"}</option>`);

            customers.forEach((customer) => {
                const option = `<option value="${customer.id}">${customer.contactName}</option>`;
                customerDropdown.append(option);
            });
        })
    }
    _createUser() {
        this._clearInput();
        this._getCustomerList("create")
        $("#btn-create-user").on("click", () => {
            this._clearInValid()

            let isValid = true;
            const vFields = [
                "input-create-name",
                "input-create-password",
            ];

            vFields.forEach((field) => {
                const value = $(`#${field}`).val().trim();
                if (!value) {
                    isValid = false;
                    $(`#${field}`).addClass("is-invalid");
                    $(`#${field}`).after(`<div class="invalid-feedback error-message">Vui lòng nhập và giá trị hợp lệ!</div>`);
                }
            });

            if (isValid) {

                const userData = {

                    username: $("#input-create-name").val().trim(),
                    password: $("#input-create-password").val().trim(),
                    email: $("#input-create-email").val().trim(),
                    customerId: $("#input-create-customer").val().trim(),
                    roles: [parseInt($("#input-create-role").val())]

                };

                this.vApi.onCreateUserClick(userData, (data) => {
                    console.log(data);
                });
            }
        });
    }


    async _updateUser(user) {
        this._getCustomerList("update")
        // Show values in the input fields
        $('#input-update-name').val(user.username);
        $('#input-update-email').val(user.email);

        if (user.customerId) {
            const customerData = await this.vApi.onGetCustomerById(user.customerId);
            this._getCustomerList("update", customerData.contactName)
            $('#input-update-customer').val(customerData.id || '');
        }

        const roles = user.roles; // Replace with the actual role name
        $('#input-update-role option').filter(function () {
            return $(this).text() === roles;
        }).prop('selected', true);

        $("#btn-update-user").on("click", () => {
            this._clearInValid();
            let isValid = true;
            const vFields = [

            ];

            vFields.forEach((field) => {
                const value = $(`#${field}`).val().trim();
                if (!value) {
                    isValid = false;
                    $(`#${field}`).addClass("is-invalid");
                    $(`#${field}`).after(`<div class="invalid-feedback error-message">Vui lòng nhập và giá trị hợp lệ!</div>`);
                }
            });

            if (isValid) {
                console.log("yes")
                const userData = {
                    username: $("#input-update-name").val().trim(),
                    customerId: $("#input-update-customer").val().trim(),
                    email: $("#input-update-email").val().trim(),
                    roles: [parseInt($("#input-update-role").val())]

                };


                // Send the updated userData to the server to update the user
                this.vApi.onUpdateUserClick(user.id, userData, (data) => {
                    console.log(data);
                });
            }
        });
    }



    _deleteUser(data) {
        $("#btn-confirm-delete-user").on("click", () => {
            this.vApi.onDeleteuserClick(data.id)
        })

    }
    _clearInValid(input) {
        if (input) {
            input.removeClass("is-invalid");
            $(".invalid-feedback").remove();
        }
        $(".form-control").removeClass("is-invalid");
        $(".invalid-feedback").remove();
    }

    _clearInput() {
        $("#create-user-form").find("input").val("");
        $("#create-user-form").find("select").val("")
    }
    openModal(type, data) {
        this._logout();
        if (type === "create") {
            this._clearInput()
            this._onEventListner()
            this._createUser()
        } else {
            if (type === "edit") {
                this._updateUser(data)
                $("#update-user-modal").modal("show")
            } else {
                this._deleteUser(data)
                $("#delete-confirm-modal").modal("show")
            }
        }

    }
}

/*** REGION 4 - vùng để gọi lên cơ sở dữ liệu lấy đa ta về*/
class CallApi {
    constructor() {
        this.token = this.getCookie("token");
    }

    setToken(token) {
        this.token = token;
    }

    getHeaders() {
        return {
            Authorization: "Bearer " + this.token
        };
    }
    setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    redirectToLogin() {
        // Trước khi logout cần xóa token đã lưu trong cookie
        this.setCookie("token", "", 1);
        window.location.href = "http://127.0.0.1:5500/real-estates-frontend/html/login.html";
    }

    onShowToast(paramTitle, paramMessage) {
        $('#myToast .mr-auto').text(paramTitle)
        $('#myToast .toast-body').text(paramMessage);
        $('#myToast').toast('show');
    }

    onGetUsersClick(paramCallbackFn) {
        $.ajax({
            url: gUSER_URL,
            method: 'GET',
            headers: this.getHeaders(),
            success: function (data) {
                paramCallbackFn(data);
            },
            error: (jqXHR, textStatus, errorThrown) => {
                console.log('Error:', errorThrown);
                // this.redirectToLogin();
            }
        });
    }

    onGetUserByIdClick(userId, paramCallbackFn) {
        $.ajax({
            url: gUSER_URL + "/" + userId,
            method: 'GET',
            headers: this.getHeaders(),
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onCreateUserClick(userData, paramCallbackFn) {
        $.ajax({
            url: gUSER_URL,
            method: 'POST',
            data: JSON.stringify(userData),
            headers: this.getHeaders(),
            contentType: 'application/json',
            success: (data) => {
                paramCallbackFn(data);
                const render = new RenderPage()
                render.renderPage()
                this.onShowToast("Tạo thành công", "Tạo user thành công!!")
                $("#create-user-modal").modal("hide");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown, textStatus);
            }
        });
    }

    onUpdateUserClick(userId, userData, paramCallbackFn) {
        $.ajax({
            url: gUSER_URL + "/" + userId,
            method: 'PUT',
            data: JSON.stringify(userData),
            headers: this.getHeaders(),
            contentType: 'application/json',
            success: (data) => {
                const render = new RenderPage()
                render.renderPage()
                paramCallbackFn(data);
                $('#update-user-modal').modal('hide');
                this.onShowToast("Sửa thành công", "Sửa user thành công!!")
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onDeleteuserClick(userId) {
        $.ajax({
            url: gUSER_URL + "/" + userId,
            method: 'DELETE',
            headers: this.getHeaders(),
            success: () => {
                this.onShowToast("Xóa thành công", "bạn đã xóa user thành công!!")
                const render = new RenderPage()
                render.renderPage()
                $("#delete-confirm-modal").modal("hide");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onUploadFile(file, paramCallbackFn) {
        const formData = new FormData();
        formData.append('image', file);

        $.ajax({
            url: gUPLOAD_FILE_URL,
            method: 'POST',
            data: formData,
            processData: false,
            contentType: false,
            headers: this.getHeaders(),
            success: (response) => {
                paramCallbackFn(response);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    onGetCustomerList(paramCallbackFn) {
        $.ajax({
            url: gCUSTOMER_URL,
            method: 'GET',
            headers: this.getHeaders(),
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }


    onExportExcelClick(paramCallbackFn) {
        $.ajax({
            url: `http://localhost:8080/api/export/users/excel`,
            method: 'GET',
            headers: this.getHeaders(),
            xhrFields: {
                responseType: "blob"
            },
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    onGetCustomerById(id) {
        return new Promise((resolve, reject) => {
            $.ajax({
                url: `${gCUSTOMER_URL}/${id}`,
                method: 'GET',
                headers: this.getHeaders(),
                success: function (data) {
                    resolve(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    reject(errorThrown);
                }
            });
        });
    }
    onGetCustomerById(id) {
        return new Promise((resolve, reject) => {
            $.ajax({
                url: `${gCUSTOMER_URL}/${id}`,
                method: 'GET',
                headers: this.getHeaders(),
                success: function (data) {
                    resolve(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    reject(errorThrown);
                }
            });
        });
    }
    getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
}
