/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gutility_URL = "http://localhost:8080/api/utilities";
const gUPLOAD_FILE_URL = "http://localhost:8080/api/upload";
const gCOLUMN_ID = {
    stt: 0,
    action: 1,
    name: 2,
    description: 3,

}
const gCOL_NAME = [
    "stt",
    "action",
    "name",
    "description",



]
//Hàm chính để load html hiển thị ra bảng
class Main {
    constructor() {
        $(document).ready(() => {
            this.vOrderList = new RenderPage()
            this.vOrderList.renderPage()
            this.vModal = new Modal()
            this.vModal.openModal("create")
            $('.select2').select2()
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })
        })
    }

}
new Main()
/*** REGION 2 - vùng để render ra bảng*/
class RenderPage {
    constructor() {
        this.vApi = new CallApi()
        this.vModal = new Modal()

    }

    //Hàm gọi api lấy danh sách đơn hàng
    _getUtilityList() {
        this.vApi.onGetUtilitysClick((paramUtility) => {
            this._createUtilityTable(paramUtility)
        })
    }
    //Hiển thị tên sau khi login
    _showAdminName() {
        const name = JSON.parse(localStorage.getItem('login'));
        if(name.accessToken) {
          $(".profile-name").text(name?.username)
          if(!name.roles.includes("ROLE_ADMIN")) {
            $('.user-item').hide();
        }
        }else {
          this.vApi.redirectToLogin()
        }
    
      }

    //Hàm tạo các thành phần của bảng
    _createUtilityTable(paramUtility) {
        let stt = 1
        if ($.fn.DataTable.isDataTable('#table-utility')) {
            $('#table-utility').DataTable().destroy();
        }
        const vOrderTable = $("#table-utility").DataTable({
            // Khai báo các cột của datatable
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false,
            "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],
            "columns": [
                { "data": null },
                { "data": gCOL_NAME[gCOLUMN_ID.action] },
                { "data": gCOL_NAME[gCOLUMN_ID.name] },
                { "data": gCOL_NAME[gCOLUMN_ID.description] },


            ],
            // Ghi đè nội dung của cột action
            "columnDefs": [
                {
                    targets: gCOLUMN_ID.stt,
                    render: function () {
                        return stt++
                    }
                }, {

                    targets: gCOLUMN_ID.action,
                    defaultContent: `
                              <img class="edit-utility" src="https://cdn0.iconfinder.com/data/icons/glyphpack/45/edit-alt-512.png" style="width: 20px;cursor:pointer;">
                              <img class="delete-utility" src="https://cdn4.iconfinder.com/data/icons/complete-common-version-6-4/1024/trash-512.png" style="width: 20px;cursor:pointer;">
                            `,
                    createdCell: (cell, cellData, rowData, rowIndex, colIndex) => {
                        if (colIndex === gCOLUMN_ID.action) {
                            $(cell).find('.edit-utility').on('click', () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                this.vModal.openModal('edit', vData);
                            });

                            $(cell).find('.delete-utility').on('click', () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                console.log(vData)
                                this.vModal.openModal('delete', vData);
                            });
                        }
                    }
                }],

        });
        vOrderTable.clear() // xóa toàn bộ dữ liệu trong bảng
        vOrderTable.rows.add(paramUtility) // cập nhật dữ liệu cho bảng
        vOrderTable.draw()// hàm vẻ lại bảng
        vOrderTable.buttons().container().appendTo('.example1_wrapper .col-md-6:eq(0)')
    }

    _saveExcelFile(data, filename) {
        const blob = new Blob([data], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
        const url = window.URL.createObjectURL(blob);
        const link = document.createElement("a");
        link.href = url;
        link.download = filename;
        link.click();
    }

    _exportToExcel() {
        $("#btn-export-excel").on("click", () => {
            this.vApi.onExportExcelClick((data) => {
                console.log(data)
                this._saveExcelFile(data, "utility.xlsx");
            });
        });
    }


    // Hàm sẽ được gọi ở class Main 
    renderPage() {
        this._showAdminName()
        this._exportToExcel()
        this._getUtilityList()
    }
}

/*** REGION 3 - vùng hiện modal*/

class Modal {
    constructor() {
        this.vApi = new CallApi()
    }

    _onEventListner() {
        $("#btn-add-utility").on("click", () => {
            $("#create-utility-modal").modal("show")
        })
        $(".logout").on("click", () => {
            $("#logout-confirm-modal").modal("show")
        })

    }

    _logout() {
        $("#btn-confirm-logout").on("click", () => {
            this.vApi.redirectToLogin()
        })

    }

    _createUtility() {
        this._clearInput();
        let imageURL;
        $("#input-create-photo").on("change", (event) => {
            const fileInput = event.target;
            const file = fileInput.files[0];
            this.vApi.onUploadFile(file, (response) => {
                imageURL = response
                $(".imagePreview2 ").css("background-image", "url(" + response + ")");
            });
        });

        $("#btn-create-utility").on("click", () => {
            this._clearInValid()
            let isValid = true;
            const vFields = [
                "input-create-name",
            ];

            vFields.forEach((field) => {
                const value = $(`#${field}`).val().trim();
                if (!value) {
                    isValid = false;
                    $(`#${field}`).addClass("is-invalid");
                    $(`#${field}`).after(`<div class="invalid-feedback error-message">Please enter a valid value!</div>`);
                }
            });

            if (isValid) {
                const utilityData = {
                    name: $("#input-create-name").val().trim(),
                    description: $("#input-create-desc").val().trim(),
                    photo: imageURL
                };

                this.vApi.onCreateUtilityClick(utilityData, (data) => {
                    console.log(data);
                });
            }
        }).bind(this);
    }

    _updateUtility(utility) {
        // Show values in the input fields
        $('#input-update-name').val(utility.name);
        $('#input-update-desc').val(utility.description);
        let imageURL;
        $("#input-update-photo").on("change", (event) => {
            const fileInput = event.target;
            const file = fileInput.files[0];
            this.vApi.onUploadFile(file, (response) => {
                imageURL = response
            });
        });
        if (utility.photo) {
            $(`.imagePreview`).css("background-image", `url(${utility.photo})`);
        } else {
            $(`.imagePreview`).css("background-image", `url(${""})`);
        }


        this._clearInValid();
        $("#btn-update-utility").on("click", () => {

            let isValid = true;
            const vFields = [
                "input-update-name",
            ];

            vFields.forEach((field) => {
                const value = $(`#${field}`).val().trim();
                if (!value) {
                    isValid = false;
                    $(`#${field}`).addClass("is-invalid");
                    $(`#${field}`).after(`<div class="invalid-feedback error-message">Please enter a valid value!</div>`);
                }
            });

            if (isValid) {
                const utilityData = {
                    name: $("#input-update-name").val().trim(),
                    description: $("#input-update-desc").val().trim(),
                    photo: imageURL
                };

                this.vApi.onUpdateUtilityClick(utility.id, utilityData, (data) => {
                    console.log(data);
                });
            }
        }).bind(this);
    }



    _deleteUtility(data) {
        $("#btn-confirm-delete-utility").on("click", () => {
            this.vApi.onDeleteUtilityClick(data.id)
        })

    }
    _clearInValid(input) {
        if (input) {
            input.removeClass("is-invalid");
            $(".invalid-feedback").remove();
        }
        $(".form-control").removeClass("is-invalid");
        $(".invalid-feedback").remove();
    }

    _clearInput() {
        $("#create-utility-form").find("input").val("");
        $("#create-utility-form").find("select").val("")
    }
    openModal(type, data) {
        this._logout();
        if (type === "create") {
            this._clearInput();
            this._onEventListner();
            this._createUtility();
        } else {
            if (type === "edit") {
                this._updateUtility(data)
                $("#update-utility-modal").modal("show")
            } else {
                this._deleteUtility(data)
                $("#delete-confirm-modal").modal("show")
            }
        }

    }
}

/*** REGION 4 - vùng để gọi lên cơ sở dữ liệu lấy đa ta về*/
class CallApi {
    constructor() {
        this.token = this.getCookie("token");
    }

    setToken(token) {
        this.token = token;
    }

    getHeaders() {
        return {
            Authorization: "Bearer " + this.token
        };
    }
    setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    redirectToLogin() {
        // Trước khi logout cần xóa token đã lưu trong cookie
        this.setCookie("token", "", 1);
        window.location.href = "http://127.0.0.1:5500/real-estates-frontend/html/login.html";
    }

    onShowToast(paramTitle, paramMessage) {
        $('#myToast .mr-auto').text(paramTitle)
        $('#myToast .toast-body').text(paramMessage);
        $('#myToast').toast('show');
    }

    onGetUtilitysClick(paramCallbackFn) {
        $.ajax({
            url: gutility_URL,
            method: 'GET',
            headers: this.getHeaders(),
            success: function (data) {
                paramCallbackFn(data);
            },
            error: (jqXHR, textStatus, errorThrown) => {
                console.log('Error:', errorThrown);
                this.redirectToLogin();
            }
        });
    }

    onGetUtilityByIdClick(utilityId, paramCallbackFn) {
        $.ajax({
            url: gutility_URL + "/" + utilityId,
            method: 'GET',
            headers: this.getHeaders(),
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onCreateUtilityClick(utilityData, paramCallbackFn) {
        $.ajax({
            url: gutility_URL,
            method: 'POST',
            data: JSON.stringify(utilityData),
            headers: this.getHeaders(),
            contentType: 'application/json',
            success: (data) => {
                paramCallbackFn(data);
                const render = new RenderPage()
                render.renderPage()
                this.onShowToast("Tạo thành công", "Tạo utility thành công!!")
                $("#create-utility-modal").modal("hide");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown, textStatus);
            }
        });
    }

    onUpdateUtilityClick(utilityId, utilityData, paramCallbackFn) {
        $.ajax({
            url: gutility_URL + "/" + utilityId,
            method: 'PUT',
            data: JSON.stringify(utilityData),
            headers: this.getHeaders(),
            contentType: 'application/json',
            success: (data) => {
                const render = new RenderPage()
                render.renderPage()
                paramCallbackFn(data);
                $('#update-utility-modal').modal('hide');
                this.onShowToast("Sửa thành công", "Sửa utility thành công!!")
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onDeleteUtilityClick(utilityId) {
        $.ajax({
            url: gutility_URL + "/" + utilityId,
            method: 'DELETE',
            headers: this.getHeaders(),
            success: () => {
                this.onShowToast("Xóa thành công", "bạn đã xóa utility thành công!!")
                const render = new RenderPage()
                render.renderPage()
                $("#delete-confirm-modal").modal("hide");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onUploadFile(file, paramCallbackFn) {
        const formData = new FormData();
        formData.append('image', file);

        $.ajax({
            url: gUPLOAD_FILE_URL,
            method: 'POST',
            data: formData,
            processData: false,
            contentType: false,
            headers: this.getHeaders(),
            success: (response) => {
                paramCallbackFn(response);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onExportExcelClick(paramCallbackFn) {
        $.ajax({
            url: `http://localhost:8080/api/export/utilitys/excel`,
            method: 'GET',
            xhrFields: {
                responseType: "blob"
            },
            headers: this.getHeaders(),
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
}
