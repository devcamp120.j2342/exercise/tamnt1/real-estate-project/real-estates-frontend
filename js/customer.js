/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gCUSTOMER_URL = "http://localhost:8080/api/customers";
const gCOLUMN_ID = {
    stt: 0,
    action: 1,
    contactName: 2,
    contactTitle: 3,
    address: 4,
    mobile: 5,
    email: 6,
    createdBy: 7,
    updatedBy: 8,
    createDate: 9,
    updateDate: 10,

}
const gCOL_NAME = [
    "stt",
    "action",
    "contactName",
    "contactTitle",
    "address",
    "mobile",
    "email",
    "createdBy",
    "updatedBy",
    "createDate",
    "updateDate"

]
//Hàm chính để load html hiển thị ra bảng
class Main {
    constructor() {
        $(document).ready(() => {
            this.vOrderList = new RenderPage()
            this.vOrderList.renderPage()
            this.vModal = new Modal()
            this.vModal.openModal("create")
            $('.select2').select2()
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })
        })
    }



}
new Main()
/*** REGION 2 - vùng để render ra bảng*/
class RenderPage {
    constructor() {
        this.vApi = new CallApi()
        this.vModal = new Modal()
        this.token = this.vApi.getCookie("token")
    }

    //Hàm gọi api lấy danh sách đơn hàng
    _getCustomerList() {
        this._createCustomerTable()

    }
    _showAdminName() {
        const name = JSON.parse(localStorage.getItem('login'));
        if (name.accessToken) {
            $(".profile-name").text(name?.username)
            if(!name.roles.includes("ROLE_ADMIN")) {
                $('.user-item').hide();
            }
        } else {
            this.vApi.redirectToLogin()
        }

    }


    //Hàm tạo các thành phần của bảng
    _createCustomerTable() {
        if ($.fn.DataTable.isDataTable('#table-data')) {
            $('#table-data').DataTable().destroy();
        }
        $.fn.dataTable.ext.errMode = 'none';
        const gNAME = ["action", "id", "contactTitle", "contactName", "address", "mobile", "email", "note", "createBy", "updateBy", "createDate", "updateDate"];
        var colname;
        var vTable = $("#table-data").DataTable({
            searching: false,
            processing: true, // shows loading image while fetching data
            serverSide: true, // activates server side pagination
            ajax: {
                headers: {
                    Authorization: "Bearer " + this.token,
                },
                url: gCUSTOMER_URL + "/dataTable", // API
            },
            columns: [{ data: gNAME[0] }, { data: gNAME[1] }, { data: gNAME[2] }, { data: gNAME[3] }, { data: gNAME[4] }, { data: gNAME[5] }, { data: gNAME[6] }, { data: gNAME[7] }, { data: gNAME[8] }, { data: gNAME[9] }, { data: gNAME[10] }, { data: gNAME[11] }],
            order: [[1, "asc"]],
            scrollX: true,
            columnDefs: [
                {
        
                  targets: 0,
                  defaultContent: `
                                         <img class="edit-customer" src="https://cdn0.iconfinder.com/data/icons/glyphpack/45/edit-alt-512.png" style="width: 20px;cursor:pointer;">
                                         <img class="delete-customer" src="https://cdn4.iconfinder.com/data/icons/complete-common-version-6-4/1024/trash-512.png" style="width: 20px;cursor:pointer;">
                                       `,
                  createdCell: (cell, cellData, rowData, rowIndex, colIndex) => {
                    if (colIndex === 0) {
                      $(cell).find('.edit-customer').on('click', () => {
                        const vData = vTable.row(rowIndex).data();
                        this.vModal.openModal('edit', vData);
                      });
        
                      $(cell).find('.delete-customer').on('click', () => {
                        const vData = vTable.row(rowIndex).data();
                        this.vModal.openModal('delete', vData);
                      });
                    }
                  }
                }
              ],
        });

        vTable.buttons().container().appendTo('.example1_wrapper .col-md-6:eq(0)');
        $('.table-container').css('overflow-x', 'auto');
    }

    _saveExcelFile(data, filename) {
        const blob = new Blob([data], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
        const url = window.URL.createObjectURL(blob);
        const link = document.createElement("a");
        link.href = url;
        link.download = filename;
        link.click();
    }

    _exportToExcel() {
        $("#btn-export-excel").on("click", () => {
            this.vApi.onExportExcelClick((data) => {
                console.log(data)
                this._saveExcelFile(data, "users.xlsx");
            });
        });
    }
    // Hàm sẽ được gọi ở class Main 
    renderPage() {
        this._showAdminName();
        this._exportToExcel();
        this._getCustomerList();
    }
}

/*** REGION 3 - vùng hiện modal*/

class Modal {
    constructor() {
        this.vApi = new CallApi()
    }

    _onEventListner() {
        $("#btn-add-customer").on("click", () => {
            $("#create-customer-modal").modal("show")
        })
        $(".logout").on("click", () => {
            console.log('yyess')
            $("#logout-confirm-modal").modal("show")
        })

    }

    _logout() {
        $("#btn-confirm-logout").on("click", () => {
            this.vApi.redirectToLogin()
        })

    }
    _createCustomer() {
        this._clearInput();
        $("#btn-create-customer").on("click", () => {
            this._clearInValid()

            let isValid = true;
            const vFields = [
                "input-create-customer-contactName",
                "input-create-customer-address",
                "input-create-customer-mobile",
            ];

            vFields.forEach((field) => {
                const value = $(`#${field}`).val().trim();
                if (!value) {
                    isValid = false;
                    $(`#${field}`).addClass("is-invalid");
                    $(`#${field}`).after(`<div class="invalid-feedback error-message">Please enter a valid value!</div>`);
                }
            });

            if (isValid) {
                const customerData = {
                    contactName: $("#input-create-customer-contactName").val().trim(),
                    contactTitle: $("#input-create-customer-contactTitle").val().trim(),
                    address: $("#input-create-customer-address").val().trim(),
                    mobile: $("#input-create-customer-mobile").val().trim(),
                    email: $("#input-create-customer-email").val().trim(),
                    note: $("#input-create-customer-note").val().trim()
                };

                this.vApi.onCreateCustomerClick(customerData, (data) => {
                    console.log(data);
                });
            }
        }).bind(this);
    }


    _updateCustomer(customer) {
        // Show values in the input fields
        $('#input-update-customer-contactName').val(customer.contactName);
        $('#input-update-customer-contactTitle').val(customer.contactTitle);
        $('#input-update-customer-address').val(customer.address);
        $('#input-update-customer-mobile').val(customer.mobile);
        $('#input-update-customer-email').val(customer.email);
        $('#input-update-customer-note').val(customer.note);

        $("#btn-update-customer").on("click", () => {
            this._clearInput();
            this._clearInValid();
            let isValid = true;
            const vFields = [
                "input-update-customer-contactName",
                "input-update-customer-address",
                "input-update-customer-mobile",
            ];

            vFields.forEach((field) => {
                const value = $(`#${field}`).val().trim();
                if (!value) {
                    isValid = false;
                    $(`#${field}`).addClass("is-invalid");
                    $(`#${field}`).after(`<div class="invalid-feedback error-message">Please enter a valid value!</div>`);
                }
            });

            if (isValid) {
                const customerData = {
                    contactName: $("#input-update-customer-contactName").val().trim(),
                    contactTitle: $("#input-update-customer-contactTitle").val().trim(),
                    address: $("#input-update-customer-address").val().trim(),
                    mobile: $("#input-update-customer-mobile").val().trim(),
                    email: $("#input-update-customer-email").val().trim(),
                    note: $("#input-update-customer-note").val().trim()
                };

                this.vApi.onUpdateCustomerClick(customer.id, customerData, (data) => {
                    console.log(data);
                });
            }
        }).bind(this);
    }



    _deleteCustomer(data) {
        $("#btn-confirm-delete-customer").on("click", () => {
            this.vApi.onDeleteCustomerClick(data.id)
        })

    }
    _clearInValid(input) {
        if (input) {
            input.removeClass("is-invalid");
            $(".invalid-feedback").remove();
        }
        $(".form-control").removeClass("is-invalid");
        $(".invalid-feedback").remove();
    }

    _clearInput() {
        $("#create-customer-form").find("input").val("");
        $("#create-customer-form").find("select").val("")
    }
    openModal(type, data) {
        this._logout()
        if (type === "create") {
            this._clearInput()
            this._onEventListner()
            this._createCustomer()
        } else {
            if (type === "edit") {
                this._updateCustomer(data)
                $("#update-customer-modal").modal("show")
            } else {
                this._deleteCustomer(data)
                $("#delete-confirm-modal").modal("show")
            }
        }

    }
}

/*** REGION 4 - vùng để gọi lên cơ sở dữ liệu lấy đa ta về*/
class CallApi {
    constructor() {
        this.token = this.getCookie("token");
    }

    setToken(token) {
        this.token = token;
    }

    getHeaders() {
        return {
            Authorization: "Bearer " + this.token
        };
    }
    setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    redirectToLogin() {
        // Trước khi logout cần xóa token đã lưu trong cookie
        this.setCookie("token", "", 1);
        window.location.href = "http://127.0.0.1:5500/real-estates-frontend/html/login.html";
    }

    onShowToast(paramTitle, paramMessage) {
        $('#myToast .mr-auto').text(paramTitle)
        $('#myToast .toast-body').text(paramMessage);
        $('#myToast').toast('show');
    }

    onGetCustomersClick(paramCallbackFn) {
        $.ajax({
            url: gCUSTOMER_URL,
            method: 'GET',
            headers: this.getHeaders(),
            success: function (data) {
                paramCallbackFn(data);
            },
            error: (jqXHR, textStatus, errorThrown) => {
                console.log('Error:', errorThrown);
                this.redirectToLogin();
            }
        });
    }

    onGetCustomersPerCountry(paramCallbackFn) {
        $.ajax({
            url: `${gCUSTOMER_URL}/country`,
            method: 'GET',
            headers: this.getHeaders(),
            success: function (data) {
                paramCallbackFn(data);
            },
            error: (jqXHR, textStatus, errorThrown) => {
                console.log('Error:', errorThrown);
                this.redirectToLogin();
            }
        });
    }

    onExportExcelClick(paramCallbackFn) {
        $.ajax({
            url: `http://localhost:8080/api/export/customers/excel`,
            method: 'GET',
            xhrFields: {
                responseType: "blob"
            },
            headers: this.getHeaders(),
            success: function (data) {
                paramCallbackFn(data);
            },
            error: (jqXHR, textStatus, errorThrown) => {
                console.log('Error:', errorThrown);
                this.redirectToLogin();
            }
        });
    }

    onGetCustomerByIdClick(customerId, paramCallbackFn) {
        $.ajax({
            url: gcustomer_URL + "/" + customerId,
            method: 'GET',
            headers: this.getHeaders(),
            success: function (data) {
                paramCallbackFn(data);
            },
            error: (jqXHR, textStatus, errorThrown) => {
                console.log('Error:', errorThrown);
                this.redirectToLogin();
            }
        });
    }

    onCreateCustomerClick(customerData, paramCallbackFn) {
        $.ajax({
            url: gCUSTOMER_URL,
            method: 'POST',
            data: JSON.stringify(customerData),
            headers: this.getHeaders(),
            contentType: 'application/json',
            success: (data) => {
                paramCallbackFn(data);
                const render = new RenderPage()
                render.renderPage()
                this.onShowToast("Tạo thành công", "Tạo khách hàng thành công!!")
                $("#create-customer-modal").modal("hide");
            },
            error: (jqXHR, textStatus, errorThrown) => {
                console.log('Error:', errorThrown, textStatus);
                this.redirectToLogin();
            }
        });
    }

    onUpdateCustomerClick(customerId, customerData, paramCallbackFn) {
        $.ajax({
            url: gCUSTOMER_URL + "/" + customerId,
            method: 'PUT',
            data: JSON.stringify(customerData),
            headers: this.getHeaders(),
            contentType: 'application/json',
            success: (data) => {
                const render = new RenderPage()
                render.renderPage()
                paramCallbackFn(data);
                $('#update-customer-modal').modal('hide');
                this.onShowToast("Sửa thành công", "Sửa khách hàng thành công!!")
            },
            error: (jqXHR, textStatus, errorThrown) => {
                console.log('Error:', errorThrown);
                this.redirectToLogin();
            }
        });
    }

    onDeleteCustomerClick(customerId) {
        $.ajax({
            url: gCUSTOMER_URL + "/" + customerId,
            method: 'DELETE',
            headers: this.getHeaders(),
            success: () => {
                this.onShowToast("Xóa thành công", "bạn đã xóa khách hàng thành công!!")
                const render = new RenderPage()
                render.renderPage()
                $("#delete-confirm-modal").modal("hide");
            },
            error: (jqXHR, textStatus, errorThrown) => {
                console.log('Error:', errorThrown);
                this.redirectToLogin();
            }
        });
    }

    getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
}
