const gLONGIN_URL = 
$(document).ready(function () {
    //----Phần này làm sau khi đã làm trang info.js
    //Kiểm tra token nếu có token tức người dùng đã đăng nhập
    const token = getCookie("token");

    if (token) {
        window.location.href = "http://127.0.0.1:5500/real-estates-frontend/html/contractor.html";
    }
    //----Phần này làm sau khi đã làm trang info.js

    //Sự kiện bấm nút login
    $("#submit").on("click", function (event) {
        event.preventDefault();
        var username = $("#inputUsername").val().trim();
        var password = $("#inputPassword").val().trim();

        if (validateForm(username, password)) {
            signinForm(username, password);
        }
        return false
    });

    function signinForm(username, password) {
        var loginUrl = "http://localhost:8080/api/auth/signin";

        var vLoginData = {
            username: username,
            password: password
        }

        $.ajax({
            url: loginUrl,
            type: "POST",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(vLoginData),
            success: function (responseObject) {
                responseHandler(responseObject);
            },
            error: function (xhr) {
                // Lấy error message
                var errorMessage = xhr.responseJSON.message;
                showError(errorMessage);
            }
        });
    }

    //Xử lý object trả về khi login thành công
    function responseHandler(data) {
        localStorage.setItem('login', JSON.stringify(data));
        //Lưu token vào cookie trong 1 ngày
        setCookie("token", data.accessToken, 1);
        const userRoles = data.roles;

        const hasDefaultRole = userRoles?.includes("ROLE_DEFAULT");

        if (hasDefaultRole) {
            // Redirect to "for-customer.html" for users with "ROLE_DEFAULT" role
            window.location.href = "http://127.0.0.1:5500/real-estates-frontend/html/for-customer.html";
        } else {
            // Redirect to "contractor.html" for users without "ROLE_DEFAULT" role
            window.location.href = "http://127.0.0.1:5500/real-estates-frontend/html/contractor.html";
        }
    }



    //Hàm setCookie đã giới thiệu ở bài trước
    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    //Hiển thị lỗi lên form 
    function showError(message) {
        var errorElement = $("#error");

        errorElement.html(message);
        errorElement.addClass("d-block");
        errorElement.addClass("d-none");
    }

    //Validate dữ liệu từ form
    function validateForm(username, password) {
        if (username === "") {
            alert("Username không phù hợp");
            return false;
        };

        if (password === "") {
            alert("Password không phù hợp");
            return false;
        }

        return true;
    }

    //Hàm get Cookie đã giới thiệu ở bài trước
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
});