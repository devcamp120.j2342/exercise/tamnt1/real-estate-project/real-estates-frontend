/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gREAL_ESTATE_URL = "http://localhost:8080/api/real-estates";
const gPROVINCE_URL = "http://localhost:8080/api/provinces";
const gDISTRICT_URL = "http://localhost:8080/api/districts";
const gSTREET_URL = "http://localhost:8080/api/streets";
const gWARD_URL = "http://localhost:8080/api/wards";
const gPROJECT_URL = "http://localhost:8080/api/projects";
const gCUSTOMER_URL = "http://localhost:8080/api/customers";
const gUPLOAD_FILE_URL = "http://localhost:8080/api/upload";
const gCUSTOMER_INFO_URL = "http://localhost:8080/users/me"
const gCOLUMN_ID = {
  stt: 0,
  action: 1,
  provinceId: 2,
  districtId: 3,
  wardId: 4,
  streetId: 5,
  title: 6,
  customerId: 7,
  name: 8,
  price: 9,
  dateCreate: 10,
  acreage: 11,
  priceTime: 12,
  proccess: 13,
};
const gCOL_NAME = [
  "stt",
  "action",
  "provinceId",
  "districtId",
  "wardId",
  "streetId",
  "title",
  "customerId",
  "name",
  "price",
  "dateCreate",
  "acreage",
  "priceTime",
  "proccess"

];
//Hàm chính để load html hiển thị ra bảng
class Main {
  constructor() {
    $(document).ready(() => {
      this.vOrderList = new RenderPage()
      this.vOrderList.renderPage()
      this.vModal = new Modal()
      this.vModal.openModal("create")
      $('.select2').select2()
      $('.select2bs4').select2({
        theme: 'bootstrap4'
      })
    })
  }

}
new Main()
/*** REGION 2 - vùng để render ra bảng*/
class RenderPage {
  constructor() {
    this.vApi = new CallApi()
    this.token = this.vApi.getCookie("token")
    this.vModal = new Modal()

  }

  //Hàm gọi api lấy danh sách đơn hàng
  _getRealEstateList() {
    $.fn.dataTable.ext.errMode = 'none';
    let pendingTableInitialized = false;
    this.vApi.onGetRealEstatesClick((paramRealEstate) => {
      const allRealEstates = paramRealEstate.data;
      const pendingRealEstates = allRealEstates.filter(item => item.proccess === "pending");
    
      if (!pendingTableInitialized) {
        this._createRealEstateTablePending(pendingRealEstates);
        pendingTableInitialized = true;
      }
    });
  this._createRealEstateTable()
  }
  //Hiển thị tên sau khi login
  _showAdminName() {
    const name = JSON.parse(localStorage.getItem('login'));
    if(name.accessToken) {
      $(".profile-name").text(name?.username)
      if(!name.roles.includes("ROLE_ADMIN")) {
        $('.user-item').hide();
    }
    }else {
     redirectToLogin()
    }

  }

  //Hàm tạo các thành phần của bảng
  _createRealEstateTable() {
    const gNAME = [ "id","action", "proccess", "provinceId", "districtId", "wardsId", "streetId", "type", "request", "customerId", "price", "dateCreate", "acreage", "totalFloors", "numberFloors", "widthY", "longX", "priceTime"];
    var colname;
    var gTable = $("#table-data").DataTable({
      searching: true,
      processing: true, // shows loading image while fetching data
      serverSide: true, // activates server side pagination
      ajax: {
        headers: {
          Authorization:"Bearer " + this.token,
        },
        url: gREAL_ESTATE_URL + "/dataTable", // API
      },
      columns: [
        { data: gNAME[0] },
        { data: gNAME[1] },
        { data: gNAME[2] },
        { data: gNAME[3] },
        { data: gNAME[4] },
        { data: gNAME[5] },
        { data: gNAME[6] },
        { data: gNAME[7] },
        { data: gNAME[8] },
        { data: gNAME[9] },
        { data: gNAME[10] },
        { data: gNAME[11] },
        { data: gNAME[12] },
        { data: gNAME[13] },
        { data: gNAME[14] },
        { data: gNAME[15] },
        { data: gNAME[16] },
        { data: gNAME[17] },
        { data: gNAME[18] },
      ],
      order: [[2, "asc"]],
      scrollX: true,
      rowCallback: function (row, data) {
        // add provinces
        $.ajax({
          headers: {
           Authorization: "Bearer " + this.token,
          },
          type: "GET",
          url: gPROVINCE_URL + "/"+ data.provinceId,
          success: function (response) {
            $("td:eq(3)", row).html(response.name);
          },
          error: function (error) {
            console.log(error);
          },
        });
  
        // add district
        $.ajax({
          headers: {
            Authorization:"Bearer " + this.token,
          },
          type: "GET",
          url: gDISTRICT_URL + "/"+ data.districtId,
          success: function (response) {
            $("td:eq(4)", row).html(response.prefix + " " + response.name);
          },
          error: function (error) {
            console.log(error);
          },
        });
  
        // add ward
        $.ajax({
          headers: {
            Authorization:"Bearer " + this.token,
          },
          type: "GET",
          url: gWARD_URL + "/"+ data.wardsId,
          success: function (response) {
            $("td:eq(5)", row).html(response.prefix + " " + response.name);
          },
          error: function (error) {
            console.log(error);
          },
        });
  
        // add street
        $.ajax({
          headers: {
            Authorization:"Bearer " + this.token,
          },
          type: "GET",
          url: gSTREET_URL  + "/"+ data.streetId,
          success: function (response) {
            $("td:eq(6)", row).html(response.prefix + " " + response.name);
          },
          error: function (error) {
            console.log(error);
          },
        });
  
        //add type
        switch (data.type) {
          case 0:
            $("td:eq(7)", row).html("Đất");
            break;
          case 1:
            $("td:eq(7)", row).html("Nhà ở");
            break;
          case 2:
            $("td:eq(7)", row).html("Căn hộ/Chung cư");
            break;
          case 3:
            $("td:eq(7)", row).html("Văn phòng, Mặt bằng");
            break;
          case 4:
            $("td:eq(7)", row).html("Kinh doanh");
            break;
          case 5:
            $("td:eq(7)", row).html("Phòng trọ");
        }
  
        // add request
        switch (data.request) {
          case 0:
            $("td:eq(8)", row).html("Cần bán");
            break;
          case 1:
            $("td:eq(8)", row).html("Cần mua");
            break;
          case 2:
            $("td:eq(8)", row).html("Cho thuê");
            break;
          case 3:
            $("td:eq(8)", row).html("Cần thuê");
            break;
        }
        // add customer
        $.ajax({
          headers: {
            Authorization:"Bearer " + this.token,
          },
          type: "GET",
          url: gCUSTOMER_URL / + data.customerId,
          success: function (response) {
            $("td:eq(9)", row).html(response.contactName);
          },
          error: function (error) {
            console.log(error);
          },
        });
  
        //add selling time
        switch (data.priceTime) {
          case 0:
            $("td:eq(18)", row).html("Bán nhanh 24h");
            break;
          case 1:
            $("td:eq(18)", row).html("Bán nhanh 72h");
            break;
          case 2:
            $("td:eq(18)", row).html("Bán nhanh 1 tuần");
            break;
          case 3:
            $("td:eq(18)", row).html("Cần thuê");
            break;
        }

        if (data.proccess == "active") {
          $("td:eq(2)", row).html("active");
        } else {
          $("td:eq(2)", row).html("pending");
        }
      },
      columnDefs: [
     {

          targets: 1,
          defaultContent: `
                              <img class="edit-realEstate" src="https://cdn0.iconfinder.com/data/icons/glyphpack/45/edit-alt-512.png" style="width: 20px;cursor:pointer;">
                              <img class="delete-realEstate" src="https://cdn4.iconfinder.com/data/icons/complete-common-version-6-4/1024/trash-512.png" style="width: 20px;cursor:pointer;">
                            `,
          createdCell: (cell, cellData, rowData, rowIndex, colIndex) => {
            if (colIndex === 1) {
              $(cell).find('.edit-realEstate').on('click', () => {
                const vData = gTable.row(rowIndex).data();
                this.vModal.openModal('edit', vData);
              });

              $(cell).find('.delete-realEstatet').on('click', () => {
                const vData = gTable.row(rowIndex).data();
                this.vModal.openModal('delete', vData);
              });
            }
          }
        }
      ],
    });

    gTable.buttons().container().appendTo('.example1_wrapper .col-md-6:eq(0)')
    $('.table-container').css('overflow-x', 'auto');
  }

  async _createRealEstateTablePending(paramRealEstate) {
    let stt = 1
    let dataReady = false;
    let provinceData, districtData, wardData, customerData;
    if ($.fn.DataTable.isDataTable('#table-realEstate-pending')) {
      $('#table-realEstate-pending').DataTable().destroy();
    }
    try {


      provinceData = await Promise.all(
        paramRealEstate.map((row) => {
          if (row && row.districtId) {
            return this.vApi.onGetProvinceById(row.provinceId)
          } else {

            return Promise.resolve({});
          }
        })
      );


      districtData = await Promise.all(
        paramRealEstate.map((row) => {
          if (row && row.districtId) {
            return this.vApi.onGetDistrictListById(row.districtId);
          } else {

            return Promise.resolve({});
          }
        })
      );

      wardData = await Promise.all(
        paramRealEstate.map((row) => {
          if (row && row.wardsId) {
            return this.vApi.onGetWardById(row.wardsId);
          } else {

            return Promise.resolve({});
          }
        })
      );
      const customerPromises = paramRealEstate.map((row) => this.vApi.onGetCustomerById(row.customerId));
      customerData = await Promise.all(
        customerPromises.map((promise) =>
          Promise.resolve(promise).catch((error) => {

            console.error("Error fetching customer data:", error);
            return {};
          })
        )
      );
      dataReady = true;
    } catch (error) {
      console.error("Error fetching data:", error);
    }
    const vTable = $("#table-realEstate-pending").DataTable({
      // Khai báo các cột của datatable
      "responsive": true,
      "lengthChange": false,
      "pageLength": 5,
      "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],
      "columns": [
        { "data": null },
        { "data": gCOL_NAME[gCOLUMN_ID.action] },
        { "data": null },
        { "data": null },
        { "data": null },
        { "data": gCOL_NAME[gCOLUMN_ID.streetId] },
        { "data": gCOL_NAME[gCOLUMN_ID.title] },
        { "data": null },
        { "data": null },
        { "data": gCOL_NAME[gCOLUMN_ID.price] },
        { "data": gCOL_NAME[gCOLUMN_ID.dateCreate] },
        { "data": gCOL_NAME[gCOLUMN_ID.acreage] },
        { "data": gCOL_NAME[gCOLUMN_ID.priceTime] },
        { "data": gCOL_NAME[gCOLUMN_ID.proccess] },
      ],
      "createdRow": function (row, data, dataIndex) {
        // Update the cells with the correct data
        if (dataReady) {
          $(row).find('td:eq(2)').text(provinceData[dataIndex]?.name || '');
          $(row).find('td:eq(3)').text(districtData[dataIndex]?.name || '');
          $(row).find('td:eq(4)').text(wardData[dataIndex]?.name || '');
          const type = data.type;
          let typeText = "";
          switch (type) {
            case 1:
              typeText = "Cần bán";
              break;
            case 2:
              typeText = "Cần mua";
              break;
            case 3:
              typeText = "Cần thuê";
              break;
            case 4:
              typeText = "Cho thuê";
              break;
            default:
              typeText = "Unknown Type";
              break;
          }



          $(row).find('td:eq(7)').text(typeText);
          $(row).find('td:eq(8)').text(customerData[dataIndex]?.contactName || '');
        } 

      },
      // Ghi đè nội dung của cột action
      "columnDefs": [
        {
          targets: gCOLUMN_ID.stt,
          render: function (data, type, row, meta) {
            if (type === "display") {
              const rowNumber = meta.row + 1;
              return rowNumber;
            } else {
              return data;
            }
          }
        }, {

          targets: gCOLUMN_ID.action,
          defaultContent: `
                              <img class="edit-realEstate" src="https://cdn2.iconfinder.com/data/icons/science-and-technology-6-5/48/260-512.png" style="width: 20px;cursor:pointer;">
                              <img class="delete-realEstate" src="https://cdn1.iconfinder.com/data/icons/material-core/20/check-circle-outline-512.png" style="width: 20px;cursor:pointer;">
                            `,
          createdCell: (cell, cellData, rowData, rowIndex, colIndex) => {
            if (colIndex === gCOLUMN_ID.action) {
              $(cell).find('.edit-realEstate').on('click', () => {
                const vData = vTable.row(rowIndex).data();
                this.vModal.openModal('edit', vData);
              });

              $(cell).find('.delete-realEstate').on('click', () => {
                const vData = vTable.row(rowIndex).data();
                this.vModal.openModal('check', vData);
              });
            }
          }
        }],

    });
    vTable.clear() // xóa toàn bộ dữ liệu trong bảng
    vTable.rows.add(paramRealEstate) // cập nhật dữ liệu cho bảng
    vTable.draw()// hàm vẻ lại bảng
    vTable.buttons().container().appendTo('.example1_wrapper .col-md-6:eq(0)')
    $('.table-container').css('overflow-x', 'auto');
  }

  _saveExcelFile(data, filename) {
    const blob = new Blob([data], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
    const url = window.URL.createObjectURL(blob);
    const link = document.createElement("a");
    link.href = url;
    link.download = filename;
    link.click();
  }

  _exportToExcel() {
    $("#btn-export-excel").on("click", () => {
      this.vApi.onExportExcelClick((data) => {
        this._saveExcelFile(data, "order-detail.xlsx");
      });
    });
  }


  // Hàm sẽ được gọi ở class Main 
  renderPage() {
    this._showAdminName()
    this._exportToExcel()
    this._getRealEstateList()
  }
}

/*** REGION 3 - vùng hiện modal*/

class Modal {
  constructor() {
    this.vApi = new CallApi()
  }

  _onEventListner(data) {
    $("#btn-add-realEstate").on("click", () => {
      $("#create-realEstate-modal").modal("show")
    })
    $(".logout").on("click", () => {
      $("#logout-confirm-modal").modal("show")
    })
    $("#btn-confirm-check").on("click", () => {
      this._checkProcessRealEstate(data)
    })

  }

  _logout() {
    $("#btn-confirm-logout").on("click", () => {
      this.vApi.redirectToLogin()
    })

  }
  _getProvinceList(type) {
    let provinceDropdown
    if (type === "update") {
      provinceDropdown = $("#input-update-provinceId");
    } else {
      provinceDropdown = $("#input-create-provinceId");
    }
    this.vApi.onGetProvinceList((provinces) => {
      provinceDropdown.empty();
      provinceDropdown.append('<option value="" selected>Tỉnh</option>');

      provinces.forEach((province) => {
        const option = `<option value="${province.id}">${province.name}</option>`;
        provinceDropdown.append(option);
      });
    });
  }
  _getDistrictList(type, provinceId) {
    let districtDropdown
    if (type === "update") {
      districtDropdown = $("#input-update-districtId");
    } else {
      districtDropdown = $("#input-create-districtId");
    }

    this.vApi.onGetDistrictListByProvinceId(provinceId, (districts) => {
      districtDropdown.empty();
      districtDropdown.append('<option value="" selected>Huyện</option>');

      districts.forEach((district) => {
        const option = `<option value="${district.id}">${district.name}</option>`;
        districtDropdown.append(option);
      });
    })

  }
  _getWardList(type, districtId) {
    let wardDropdown
    if (type === "update") {
      wardDropdown = $("#input-update-wardId");
    } else {
      wardDropdown = $("#input-create-wardId");
    }

    this.vApi.onGetWardByDistrictId(districtId, (wards) => {
      wardDropdown.empty();
      wardDropdown.append('<option value="" selected>Xã</option>');

      wards.forEach((ward) => {
        const option = `<option value="${ward.id}">${ward.name}</option>`;
        wardDropdown.append(option);
      });
    })

  }
  _getStreetList(type, districtId) {
    let streetDropdown
    if (type === "update") {
      streetDropdown = $("#input-update-streetId");
    } else {
      streetDropdown = $("#input-create-streetId");
    }

    this.vApi.onGetStreetByDistrictId(districtId, (streets) => {
      streetDropdown.empty();
      streetDropdown.append('<option value="" selected>Đường</option>');

      streets.forEach((street) => {
        const option = `<option value="${street.id}">${street.name}</option>`;
        streetDropdown.append(option);
      });
    })

  }
  _getProjectList(type) {
    let projectDropdown
    if (type === "update") {
      projectDropdown = $("#input-update-project");
    } else {
      projectDropdown = $("#input-create-project");
    }

    this.vApi.onGetProjectList((projects) => {
      projectDropdown.empty();
      projectDropdown.append('<option value="" selected>Dự án</option>');

      projects.forEach((project) => {
        const option = `<option value="${project.id}">${project.name}</option>`;
        projectDropdown.append(option);
      });
    })
  }

  _getCustomerList(type) {
    let customerDropdown
    if (type === "update") {
      customerDropdown = $("#input-update-customer");
    } else {
      customerDropdown = $("#input-create-customer");
    }
    this.vApi.onGetCustomerList((customers) => {
      customerDropdown.empty();
      customerDropdown.append('<option value="" selected>Khách hàng</option>');

      customers.forEach((customer) => {
        const option = `<option value="${customer.id}">${customer.contactName}</option>`;
        customerDropdown.append(option);
      });
    })
  }

  _createRealEstate() {
    this._getProvinceList("create");
    this._clearInput();
    this._getProjectList();
    this._getCustomerList();
    let imageURL;
    $("#input-create-provinceId").on("change", () => {
      const provinceId = $("#input-create-provinceId").val();
      this._getDistrictList("create",provinceId);
    });

    $("#input-create-districtId").on("change", () => {
      const districtId = $("#input-create-districtId").val();
      this._getWardList("create",districtId);
      this._getStreetList("create",districtId);
    });
    $("#input-create-photo").on("change", (event) => {
      const fileInput = event.target;
      const file = fileInput.files[0];
      this.vApi.onUploadFile(file, (response) => {
        imageURL = response
        $(".imagePreview2 ").css("background-image", "url(" + response + ")");
      });
    });

    $("#btn-create-realestate").on("click", (event) => {
      this._clearInValid();
      event.preventDefault();
      let isValid = true;
      const vFields = [
        "input-create-title",

      ];

      vFields.forEach((field) => {
        const value = $(`#${field}`).val();
        if (!value) {
          isValid = false;
          $(`#${field}`).addClass("is-invalid");
          $(`#${field}`).after(`<div class="invalid-feedback error-message">Please enter a valid value!</div>`);
        }
      });

      if (isValid) {
        // Get values from the form
        const address = $("#input-create-address").val();
        const photo = imageURL; //need the response
        const provinceId = parseInt($("#input-create-provinceId").val());
        const districtId = parseInt($("#input-create-districtId").val());
        const wardId = parseInt($("#input-create-wardId").val());
        const streetId = parseInt($("#input-create-streetId").val());
        const projectId = parseInt($("#input-create-projectId").val());
        const title = $("#input-create-title").val();
        const type = parseInt($("#input-create-type").val());
        const request = parseInt($("#input-create-request").val());
        const customerId = parseInt($("#input-create-customer").val());
        const price = $("#input-create-price").val();
        const minPrice = $("#input-create-minPrice").val();
        const priceTime = parseInt($("#input-create-priceTime").val());
        const apartCode = $("#input-create-apartCode").val();
        const acreage = $("#input-create-acreage").val();
        const bedroom = $("#input-create-bedroom").val();
        const balcony = parseInt($("#input-create-balcony").val());
        const landscapeView = $("#input-create-landscapeView").val();
        const apartLoca = $("#input-create-apartLoca").val();
        const apartType = $("#input-create-apartType").val();
        const rentPrice = parseFloat($("#input-create-rent").val());
        const bdsArea = $("#input-create-acreage").val();
        const houseDirection = $("#input-create-acreage").val();
        const totalFloors = parseInt($("#input-create-totalFloors").val());
        const numberFloors = parseInt($("#input-create-floorNum").val());;
        const description = $("#input-create-desc").val();
        const width = parseFloat($("#input-create-width").val());
        const length = parseFloat($("#input-create-long").val());
        const createdBy = parseFloat($("#input-create-createdBy").prop('checked'));
        const shape = $("#input-create-shape").val();
        const adjacentRoad = $("#input-create-adjacentRoad").val();
        const alleyMinWidth = $("#input-create-alleyMinWidth").val();
        const ctxdPrice = $("#input-create-ctxdPrice").val();
        const ctxdValue = $("#input-create-ctxdValue").val();
        const alleyWidth = $("#input-create-alleyMinWidth").val();
        const factor = parseInt($("#input-create-factor").val());
        const structure = $("#input-create-structure").val();
        const clcl = parseInt($("#input-create-clcl").val());
        const lat = parseInt($("#input-create-lat").val());
        const long = parseInt(($("#input-create-long").val()));

        const realEstateData = {
          address,
          photo,
          bedroom,
          provinceId,
          districtId,
          wardId,
          streetId,
          projectId,
          title,
          type,
          request,
          customerId,
          price,
          minPrice,
          priceTime,
          apartCode,
          acreage,
          balcony,
          landscapeView,
          apartLoca,
          apartType,
          rentPrice,
          bdsArea,
          houseDirection,
          totalFloors,
          numberFloors,
          description,
          width,
          length,
          createdBy,
          shape,
          adjacentRoad,
          alleyMinWidth,
          ctxdPrice,
          ctxdValue,
          alleyWidth,
          factor,
          structure,
          clcl,
          lat,
          long,
        };

        this.vApi.onCreateRealEstateClick(realEstateData, (data) => {
          console.log(data)
        })

      }
    })
  }

  async _updateRealEstate(data) {
    this._getProvinceList("update")
    this._getCustomerList("update")
    this._getProjectList("update")
    let imageURL;
    $("#input-update-provinceId").on("change", () => {
      const provinceId = $("#input-update-provinceId").val();
      this._getDistrictList("update", provinceId);
    });

    $("#input-update-districtId").on("change", () => {
      const districtId = $("#input-update-districtId").val();
      this._getWardList("update", districtId);
      this._getStreetList("update", districtId)
    });
    try {

      this._getDistrictList("update", data.provinceId);
      this._getWardList("update", data.districtId);
      this._getStreetList("update", data.districtId);
      if (data.provinceId) {
        const provinceData = await this.vApi.onGetProvinceById(data.provinceId);
        $('#input-update-provinceId').val(provinceData.id || '');
      }

      // Get the district name
      if (data.districtId) {
        const districtData = await this.vApi.onGetDistrictListById(data.districtId);
        $('#input-update-districtId').val(districtData.id || '')

      }

      // Get the ward name
      if (data.wardsId) {
        const wardData = await this.vApi.onGetWardById(data?.wardsId);
        $('#input-update-wardId').val(wardData.id || '')
      }

      if (data.streetId) {
        const streetData = await this.vApi.onGetStreetById(data.streetId);
        $('#input-update-streetId').val(streetData.id || '')
      }


      if (data.customerId) {
        const customerData = await this.vApi.onGetCustomerById(data.customerId);
        $('#input-update-customer').val(customerData.id || '');
      }

      if (data.projectId) {
        const projectData = await this.vApi.onGetProjectById(data.projectId);
        $('#input-update-project').val(projectData.id || '');
      }

      $("#input-update-photo").on("change", (event) => {
        const fileInput = event.target;
        const file = fileInput.files[0];
        this.vApi.onUploadFile(file, (response) => {
          imageURL = response
          $(".imagePreview ").css("background-image", "url(" + response + ")");
        });
      });

    } catch (error) {
      console.log(error)
    }
    if (data.photo) {

      $(`.imagePreview`).css("background-image", `url(${data.photo})`);
    } else {
      $(`.imagePreview`).css("background-image", `url(${""})`);
    }

    $('#input-update-address').val(data.address);
    $('#input-update-title').val(data.title);
    $('#input-update-type').val(data.type);
    $('#input-update-request2').val(data.request);
    $('#input-update-price').val(data.price);
    $('#input-update-priceMin').val(data.priceMin);
    $('#input-update-priceTime').val(data.priceTime);
    $('#input-update-apartCode').val(data.apartCode);
    $('#input-update-acreage').val(data.acreage);
    $('#input-update-bedroom').val(data.bedroom);
    $('#input-update-balcony').val(data.balcony);
    $('#input-update-landscapeView').val(data.landscapeView);
    $('#input-update-apartLoca').val(data.apartLoca);
    $('#input-update-apartType').val(data.apartType);
    $('#input-update-rent').val(data.priceRent);
    $('#input-update-direction').val(data.direction);
    $('#input-update-totalFloors').val(data.totalFloors);
    $('#input-update-floorNum').val(data.numberFloors);
    $('#input-update-balcony').val(data.balcony);
    $('#input-update-desc').val(data.description);
    $('#input-update-furniture').val(data.furniture);
    $('#input-update-width').val(data.widthY);
    $('#input-update-long').val(data.longX);
    $('#input-update-shape').val(data.shape);
    $('#input-update-longi').val(data.long);
    $('#input-update-lat').val(data.lat);
    $('#input-update-alleyMinWidth').val(data.alleyMinWidth);
    $('#input-update-clcl').val(data.clcl);

    $("#btn-update-realestate").off("click").on("click", (event) => {
      event.preventDefault();
      const address = $("#input-update-address").val();
      const photo = imageURL;
      const provinceId = parseInt($("#input-update-provinceId").val());
      const districtId = parseInt($("#input-update-districtId").val());
      const wardsId = parseInt($("#input-update-wardId").val());
      const streetId = parseInt($("#input-update-streetId").val());
      const projectId = parseInt($("#input-update-project").val());
      const title = $("#input-update-title").val();
      const type = parseInt($("#input-update-type").val());
      const request = parseInt($("#input-update-request").val());
      const customerId = parseInt($("#input-update-customer").val());
      const price = $("#input-update-price").val();
      const priceMin = $("#input-update-priceMin").val();
      const priceTime = parseInt($("#input-update-priceTime").val());
      const apartCode = $("#input-update-apartCode").val();
      const acreage = $("#input-update-acreage").val();
      const bedroom = $("#input-update-bedroom").val();
      const balcony = $("#input-update-balcony").val();
      const landscapeView = $("#input-update-landscapeView").val();
      const apartLoca = $("#input-update-apartLoca").val();
      const apartType = $("#input-update-apartType").val();
      const rentPrice = $("#input-update-rent").val();
      const bdsArea = $("#input-update-acreage").val();
      const houseDirection = $("#input-update-acreage").val();
      const totalFloors = parseInt($("#input-update-totalFloors").val());
      const numberFloors = parseInt($("#input-update-floorNum").val());
      const description = $("#input-update-desc").val();
      const width = $("#input-update-width").val();
      const length = $("#input-update-long").val();
      const createdBy = $("#input-update-createdBy").prop('checked');
      const shape = $("#input-update-shape").val();
      const adjacentRoad = $("#input-update-adjacentRoad").val();
      const alleyMinWidth = $("#input-update-alleyMinWidth").val();
      const ctxdPrice = $("#input-update-ctxdPrice").val();
      const ctxdValue = $("#input-update-ctxdValue").val();
      const alleyWidth = $("#input-update-alleyMinWidth").val();
      const factor = $("#input-update-factor").val();
      const structure = $("#input-update-structure").val();
      const clcl = $("#input-update-clcl").val();
      const lat = $("#input-update-lat").val();
      const long = ($("#input-update-long").val());

      const realEstateData = {
        address,
        photo,
        provinceId,
        districtId,
        wardsId,
        streetId,
        projectId,
        title,
        type,
        request,
        customerId,
        price,
        priceMin,
        priceTime,
        apartCode,
        acreage,
        bedroom,
        balcony,
        landscapeView,
        apartLoca,
        apartType,
        rentPrice,
        bdsArea,
        houseDirection,
        totalFloors,
        numberFloors,
        description,
        width,
        length,
        createdBy,
        shape,
        adjacentRoad,
        alleyMinWidth,
        ctxdPrice,
        ctxdValue,
        alleyWidth,
        factor,
        structure,
        clcl,
        lat,
        long,
      };

      this.vApi.onUpdateRealEstateClick(data?.id, realEstateData, (data) => {
        console.log(data)
      })
    })

  }

  _deleteRealEstate(data) {
    $("#btn-confirm-delete-realEstate").on("click", () => {
      this.vApi.onDeleterealEstateClick(data.id)
    })
  }
  _checkProcessRealEstate(data) {
    if (data) {
      this.vApi.onCheckProcessRealEstateClick('active', data?.id, (paramData) => {
        console.log(paramData)
      })
    }
  }
  _getCheckedValues(checkboxes) {
    return checkboxes
      .filter(([checkboxId, checkboxValue]) => $(`#${checkboxId}`).prop("checked"))
      .map(([, checkboxValue, checkboxName]) => checkboxValue);
  }
  _clearInValid(input) {
    if (input) {
      input.removeClass("is-invalid");
      $(".invalid-feedback").remove();
    }
    $(".form-control").removeClass("is-invalid");
    $(".invalid-feedback").remove();
  }

  _clearInput() {
    $("#create-realEstate-form").find("input").val("");
    $("#create-realEstate-form").find("select").val("")
  }
  openModal(type, data) {
    if (type === "create") {
      this._clearInput();

      this._createRealEstate();
    }
    if (type === "edit") {
      this._updateRealEstate(data)
      $("#update-realEstate-modal").modal("show")
    }
    if (type === "delete") {
      this._deleteRealEstate(data)
      $("#delete-confirm-modal").modal("show")
    }

    if (type === "check") {
      $("#check-confirm-modal").modal("show")
    }
    this._logout();
    this._onEventListner(data);
  }
}

/*** REGION 4 - vùng để gọi lên cơ sở dữ liệu lấy đa ta về*/
class CallApi {
  constructor() {
    this.token = this.getCookie("token");
  }

  setToken(token) {
    this.token = token;
  }

  getHeaders() {
    return {
      Authorization: "Bearer " + this.token
    };
  }
  setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  }

  redirectToLogin() {
    // Trước khi logout cần xóa token đã lưu trong cookie
    this.setCookie("token", "", 1);
    window.location.href = "http://127.0.0.1:5500/real-estates-frontend/html/login.html";
  }

  onShowToast(paramTitle, paramMessage) {
    $('#myToast .mr-auto').text(paramTitle)
    $('#myToast .toast-body').text(paramMessage);
    $('#myToast').toast('show');
  }
  onGetCustomerInfo(paramCallbackFn) {
    $.ajax({
      url: gCUSTOMER_INFO_URL,
      method: 'GET',
      headers: this.getHeaders(),
      success: function (data) {
        paramCallbackFn(data);
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log('Error:', errorThrown);
      }
    });
  }

  onCheckProcessRealEstateClick(processState, id, paramCallbackFn) {
    $.ajax({
      url: `${gREAL_ESTATE_URL}/${id}/process`,
      method: 'PUT',
      headers: this.getHeaders(),
      data: processState,
      contentType: 'application/json',
      success: (data) => {
        paramCallbackFn(data);
        const render = new RenderPage()
        render.renderPage()
        $("#check-confirm-modal").modal("hide")
        this.onShowToast("Duyệt thành công", "Duyệt bất động sản thành công!!")

      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log('Error:', errorThrown);
      }
    });
  }

  onGetRealEstatesClick(paramCallbackFn) {
    $.ajax({
      url: gREAL_ESTATE_URL,
      method: 'GET',
      headers: this.getHeaders(),
      success: function (data) {
        paramCallbackFn(data);
      },
      error: (jqXHR, textStatus, errorThrown) => {
        console.log('Error:', errorThrown);
        this.redirectToLogin();
      }
    });
  }
  
  onGetRealEstatesDataTable(paramCallbackFn) {
    $.ajax({
      url: `${gREAL_ESTATE_URL}/dataTable`,
      method: 'GET',
      headers: this.getHeaders(),
      success: function (data) {
        paramCallbackFn(data);
      },
      error: (jqXHR, textStatus, errorThrown) => {
        console.log('Error:', errorThrown);
        // this.redirectToLogin();
      }
    });
  }

   onGetRealEstateClickActive(page, size, paramCallbackFn) {
    $.ajax({
      url: `${gREAL_ESTATE_URL}/active?page=${page}&size=${size}`,
      method: 'GET',
      success: function (data) {
        paramCallbackFn(data);
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log('Error:', errorThrown);
      }
    });
  }
  onGetProjectList(paramCallbackFn) {
    $.ajax({
      url: gPROJECT_URL,
      method: 'GET',
      headers: this.getHeaders(),
      success: function (data) {
        paramCallbackFn(data);
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log('Error:', errorThrown);
      }
    });
  }

  onGetCustomerList(paramCallbackFn) {
    $.ajax({
      url: gCUSTOMER_URL,
      method: 'GET',
      headers: this.getHeaders(),
      success: function (data) {
        paramCallbackFn(data);
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log('Error:', errorThrown);
      }
    });
  }

  onGetProvinceList(paramCallbackFn) {
    $.ajax({
      url: gPROVINCE_URL,
      method: 'GET',
      headers: this.getHeaders(),
      success: function (data) {
        paramCallbackFn(data);
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log('Error:', errorThrown);
      }
    });
  }

  onGetDistrictListByProvinceId(provinceId, paramCallbackFn) {
    $.ajax({
      url: `${gDISTRICT_URL}/${provinceId}/province`,
      method: 'GET',
      headers: this.getHeaders(),
      success: function (data) {
        paramCallbackFn(data);
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log('Error:', errorThrown);
      }
    });
  }

  onGetWardByDistrictId(districtId, paramCallbackFn) {
    $.ajax({
      url: `${gWARD_URL}/${districtId}/district`,
      method: 'GET',
      headers: this.getHeaders(),
      success: function (data) {
        paramCallbackFn(data);
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log('Error:', errorThrown);
      }
    });
  }

  onGetStreetByDistrictId(districtId, paramCallbackFn) {
    $.ajax({
      url: `${gSTREET_URL}/${districtId}/district`,
      method: 'GET',
      headers: this.getHeaders(),
      success: function (data) {
        paramCallbackFn(data);
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log('Error:', errorThrown);
      }
    });
  }
  onGetCustomerById(id) {
    return new Promise((resolve, reject) => {
      $.ajax({
        url: `${gCUSTOMER_URL}/${id}`,
        method: 'GET',
        headers: this.getHeaders(),
        success: function (data) {
          resolve(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
          reject(errorThrown);
        }
      });
    });
  }
  onGetDistrictListById(id) {
    return new Promise((resolve, reject) => {
      $.ajax({
        url: `${gDISTRICT_URL}/${id}`,
        method: 'GET',
        headers: this.getHeaders(),
        success: function (data) {
          resolve(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
          reject(errorThrown);
        }
      });
    });
  }
  onGetStreetById(id) {
    return new Promise((resolve, reject) => {
      $.ajax({
        url: `${gSTREET_URL}/${id}`,
        method: 'GET',
        headers: this.getHeaders(),
        success: function (data) {
          resolve(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
          reject(errorThrown);
        }
      });
    });
  }
  onGetDistrictListById(id) {
    return new Promise((resolve, reject) => {
      $.ajax({
        url: `${gDISTRICT_URL}/${id}`,
        method: 'GET',
        headers: this.getHeaders(),
        success: function (data) {
          resolve(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
          reject(errorThrown);
        }
      });
    });
  }
  onGetWardById(id) {
    return new Promise((resolve, reject) => {
      $.ajax({
        url: `${gWARD_URL}/${id}`,
        method: 'GET',
        headers: this.getHeaders(),
        success: function (data) {
          resolve(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
          reject(errorThrown);
        }
      });
    });
  }
  onGetProvinceById(id) {
    return new Promise((resolve, reject) => {
      $.ajax({
        url: `${gPROVINCE_URL}/${id}`,
        method: 'GET',
        headers: this.getHeaders(),
        success: function (data) {
          resolve(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
          reject(errorThrown);
        }
      });
    });
  }
  onGetProjectById(id) {
    return new Promise((resolve, reject) => {
      $.ajax({
        url: `${gPROJECT_URL}/${id}`,
        method: 'GET',
        headers: this.getHeaders(),
        success: function (data) {
          resolve(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
          reject(errorThrown);
        }
      });
    });
  }


  onGetRealEstateByIdClick(realEstateId, paramCallbackFn) {
    $.ajax({
      url: gREAL_ESTATE_URL + "/" + realEstateId,
      method: 'GET',
      headers: this.getHeaders(),
      success: function (data) {
        paramCallbackFn(data);
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log('Error:', errorThrown);
      }
    });
  }

  onCreateRealEstateClick(realEstateData, paramCallbackFn) {
    $.ajax({
      url: gREAL_ESTATE_URL,
      method: 'POST',
      headers: this.getHeaders(),
      data: JSON.stringify(realEstateData),
      contentType: 'application/json',
      success: (data) => {
        paramCallbackFn(data);
        const render = new RenderPage()
        render.renderPage()
        location.reload()
        this.onShowToast("Tạo thành công", "Tạo Bất động sản thành công!!")
        window.location.href = "real-estate.html";

      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log('Error:', errorThrown, textStatus);
      }
    });
  }
  
  onUpdateRealEstateClick(realEstateId, realEstateData, paramCallbackFn) {
    $.ajax({
      url: gREAL_ESTATE_URL + "/" + realEstateId,
      method: 'PUT',
      headers: this.getHeaders(),
      data: JSON.stringify(realEstateData),
      contentType: 'application/json',
      success: (data) => {
        const render = new RenderPage()
        render.renderPage()
        location.reload()
        paramCallbackFn(data);
        $('#update-realEstate-modal').modal('hide');
        this.onShowToast("Sửa thành công", "Sửa Bất động sản thành công!!")
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log('Error:', errorThrown);
      }
    });
  }
  onDeleterealEstateClick(realEstateId) {
    $.ajax({
      url: gREAL_ESTATE_URL + "/" + realEstateId,
      method: 'DELETE',
      headers: this.getHeaders(),
      success: () => {
        this.onShowToast("Xóa thành công", "bạn đã xóa Bất động sản thành công!!")
        const render = new RenderPage()
        render.renderPage()
        $("#delete-confirm-modal").modal("hide");
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log('Error:', errorThrown);
      }
    });
  }
  onUploadFile(file, paramCallbackFn) {
    const formData = new FormData();
    formData.append('image', file);

    $.ajax({
      url: gUPLOAD_FILE_URL,
      method: 'POST',
      headers: this.getHeaders(),
      data: formData,
      processData: false,
      contentType: false,
      success: (response) => {
        paramCallbackFn(response);
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log('Error:', errorThrown);
      }
    });
  }


  getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }
}
