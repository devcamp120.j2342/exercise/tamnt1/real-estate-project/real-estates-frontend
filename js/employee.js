/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gEMPLOYEE_URL = "http://localhost:8080/api/employees";
const gUPLOAD_FILE_URL = "http://localhost:8080/api/upload";
const gCOLUMN_ID = {
    stt: 0,
    action: 1,
    lastName: 2,
    firstName: 3,
    title: 4,
    birthDate: 5,
    hireDate: 6,
    address: 7,
    city: 8,
    region: 9,
    postalCode: 10,
    country: 11,
    homePhone: 12,
    extension: 13,
    photo: 14,
    notes: 15,
    reportsTo: 16,
    username: 17,
    email: 18,
    activated: 19,
    userLevel: 20,




}
const gCOL_NAME = [
    "id",
    "action",
    "lastName",
    "firstName",
    "title",
    "birthDate",
    "hireDate",
    "address",
    "city",
    "region",
    "postalCode",
    "country",
    "homePhone",
    "extension",
    "photo",
    "notes",
    "reportsTo",
    "username",
    "email",
    "activated",
    "userLevel"
]
//Hàm chính để load html hiển thị ra bảng
class Main {
    constructor() {
        $(document).ready(() => {
            this.vOrderList = new RenderPage()
            this.vOrderList.renderPage()
            this.vModal = new Modal()
            this.vModal.openModal("create")
            $('.select2').select2()
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })
        })
    }

}
new Main()
/*** REGION 2 - vùng để render ra bảng*/
class RenderPage {
    constructor() {
        this.vApi = new CallApi()
        this.vModal = new Modal()

    }

    //Hàm gọi api lấy danh sách đơn hàng
    _getEmployeeList() {
        this.vApi.onGetEmployeesClick((paramEmployee) => {
            this._createEmployeeTable(paramEmployee)
        })
    }
    //Hiển thị tên sau khi login
    _showAdminName() {
        const name = JSON.parse(localStorage.getItem('login'));
        if (name.accessToken) {
            $(".profile-name").text(name?.username)
            if (!name.roles.includes("ROLE_ADMIN")) {
                $('.user-item').hide();
            }
        } else {
            this.vApi.redirectToLogin()
        }

    }


    //Hàm tạo các thành phần của bảng
    _createEmployeeTable(paramEmployee) {
        console.log(paramEmployee)
        let stt = 1
        if ($.fn.DataTable.isDataTable('#table-employee')) {
            $('#table-employee').DataTable().destroy();
        }
        const vOrderTable = $("#table-employee").DataTable({
            // Khai báo các cột của datatable
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false,
            "processing": true,
            "pageLength": 5,
            "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],
            "columns": [
                { "data": null },
                { "data": gCOL_NAME[gCOLUMN_ID.action] },
                { "data": gCOL_NAME[gCOLUMN_ID.lastName] },
                { "data": gCOL_NAME[gCOLUMN_ID.firstName] },
                { "data": gCOL_NAME[gCOLUMN_ID.title] },
                { "data": gCOL_NAME[gCOLUMN_ID.birthDate] },
                { "data": gCOL_NAME[gCOLUMN_ID.hireDate] },
                { "data": gCOL_NAME[gCOLUMN_ID.address] },
                { "data": gCOL_NAME[gCOLUMN_ID.city] },
                { "data": gCOL_NAME[gCOLUMN_ID.region] },
                { "data": gCOL_NAME[gCOLUMN_ID.postalCode] },
                { "data": gCOL_NAME[gCOLUMN_ID.country] },
                { "data": gCOL_NAME[gCOLUMN_ID.homePhone] },
                { "data": gCOL_NAME[gCOLUMN_ID.extension] },
                { "data": gCOL_NAME[gCOLUMN_ID.photo] },
                { "data": gCOL_NAME[gCOLUMN_ID.notes] },
                { "data": gCOL_NAME[gCOLUMN_ID.reportsTo] },
                { "data": gCOL_NAME[gCOLUMN_ID.username] },
                { "data": gCOL_NAME[gCOLUMN_ID.email] },
                { "data": gCOL_NAME[gCOLUMN_ID.activated] },
                { "data": gCOL_NAME[gCOLUMN_ID.userLevel] },

            ],
            // Ghi đè nội dung của cột action
            "columnDefs": [

                {
                    targets: gCOLUMN_ID.stt,
                    render: function () {
                        return stt++
                    }
                }, {

                    targets: gCOLUMN_ID.action,
                    defaultContent: `
                              <img class="edit-employee" src="https://cdn0.iconfinder.com/data/icons/glyphpack/45/edit-alt-512.png" style="width: 20px;cursor:pointer;">
                              <img class="delete-employee" src="https://cdn4.iconfinder.com/data/icons/complete-common-version-6-4/1024/trash-512.png" style="width: 20px;cursor:pointer;">
                            `,
                    createdCell: (cell, cellData, rowData, rowIndex, colIndex) => {
                        if (colIndex === gCOLUMN_ID.action) {
                            $(cell).find('.edit-employee').on('click', () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                this.vModal.openModal('edit', vData);
                            });

                            $(cell).find('.delete-employee').on('click', () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                console.log(vData)
                                this.vModal.openModal('delete', vData);
                            });
                        }
                    }
                }],

        });
        vOrderTable.clear() // xóa toàn bộ dữ liệu trong bảng
        vOrderTable.rows.add(paramEmployee) // cập nhật dữ liệu cho bảng
        vOrderTable.draw()// hàm vẻ lại bảng
        vOrderTable.buttons().container().appendTo('.example1_wrapper .col-md-6:eq(0)')
        $('.table-container').css('overflow-x', 'auto');
    }
    _saveExcelFile(data, filename) {
        const blob = new Blob([data], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
        const url = window.URL.createObjectURL(blob);
        const link = document.createElement("a");
        link.href = url;
        link.download = filename;
        link.click();
    }

    _exportToExcel() {
        $("#btn-export-excel").on("click", () => {
            this.vApi.onExportExcelClick((data) => {
                console.log(data)
                this._saveExcelFile(data, "employee.xlsx");
            });
        });
    }


    // Hàm sẽ được gọi ở class Main 
    renderPage() {
        this._showAdminName();
        this._exportToExcel();
        this._getEmployeeList();
    }
}

/*** REGION 3 - vùng hiện modal*/

class Modal {
    constructor() {
        this.vApi = new CallApi()
    }

    _onEventListner() {
        $("#btn-add-employee").on("click", () => {
            $("#create-employee-modal").modal("show")

        })
        $(".logout").on("click", () => {
            $("#logout-confirm-modal").modal("show")
        })

    }

    _logout() {
        $("#btn-confirm-logout").on("click", () => {
            this.vApi.redirectToLogin()
        })

    }
    _createEmployee() {
        // Clear any previous data and validation messages
        this._clearInput();
        let imageURL;
        $("#input-create-photo").on("change", (event) => {
            const fileInput = event.target;
            const file = fileInput.files[0];
            this.vApi.onUploadFile(file, (response) => {
                imageURL = response
                $(".imagePreview2 ").css("background-image", "url(" + response + ")");
            });
        });

        $("#btn-create-employee").on("click", () => {
            this._clearInValid()

            let isValid = true;
            const vFields = [
                "input-create-lastName",
                "input-create-firstName",
                "input-create-password",
            ];

            vFields.forEach((field) => {
                const value = $(`#${field}`).val().trim();
                if (!value) {
                    isValid = false;
                    $(`#${field}`).addClass("is-invalid");
                    $(`#${field}`).after(`<div class="invalid-feedback error-message">Vui lòng nhập và giá trị hợp lệ!</div>`);
                }
            });

            if (isValid) {
                const activatedValue = $('input[name="activated"]:checked').val();
                console.log("Activated Value:", activatedValue);
                const employeeData = {

                    lastName: $("#input-create-lastName").val().trim(),
                    firstName: $("#input-create-firstName").val().trim(),
                    title: $("#input-create-title").val().trim(),
                    titleOfCourtesy: $("#input-create-titleOfCourtesy").val(),
                    birthDate: $("#input-create-birthDate").val().trim(),
                    hireDate: $("#input-create-hireDate").val().trim(),
                    address: $("#input-create-address").val().trim(),
                    region: $("#input-create-region").val().trim(),
                    postalCode: $("#input-create-code").val().trim(),
                    country: $("#input-create-country").val().trim(),
                    homePhone: $("#input-create-phone").val().trim(),
                    extension: $("#input-create-ext").val().trim(),
                    photo: imageURL,
                    reportsTo: parseInt($("#input-create-reportTo").val().trim()),
                    username: $("#input-create-username").val().trim(),
                    password: $("#input-create-password").val().trim(),
                    email: $("#input-create-email").val().trim(),
                    notes: $("#input-create-note").val().trim(),
                    userLevel: $("#input-update-userLevel").val(),
                    profile: $("#input-update-profile").val(),
                    activated: $('input[name="activated"]:checked').val() || "Y",
                };



                // Send the employeeData to the server to create a new employee
                this.vApi.onCreateEmployeeClick(employeeData, (data) => {
                    console.log(data);
                });
            }
        });
    }


    _updateEmployee(employee) {
        // Clear any previous data and validation messages
        console.log(employee.photo)
        this._clearInput();
        let imageURL;
        $("#input-update-photo").on("change", (event) => {
            const fileInput = event.target;
            const file = fileInput.files[0];
            this.vApi.onUploadFile(file, (response) => {
                imageURL = response
            });
        });
        // Show values in the input fields
        $('#input-update-lastName').val(employee.lastName);
        $('#input-update-firstName').val(employee.firstName);
        $('#input-update-title').val(employee.title);
        $('#input-update-birthDate').val(employee.birthDate);
        $('#input-update-hireDate').val(employee.hireDate);
        $('#input-update-address').val(employee.address);
        $('#input-update-region').val(employee.region);
        $('#input-update-code').val(employee.postalCode);
        $('#input-update-country').val(employee.country);
        $('#input-update-phone').val(employee.homePhone);
        $('#input-update-ext').val(employee.extension);
        $('#input-update-reportTo').val(employee.reportTo);
        $('#input-update-username').val(employee.username);
        $('#input-update-password').val(employee.password);
        $('#input-update-email').val(employee.email);
        $('#input-update-note').val(employee.notes);
        $('#input-update-userLevel').val(employee.userLevel);
        $('input[name="activated2"][value="' + employee.activated + '"]').prop('checked', true);
        if (employee.photo) {
            $(`.imagePreview`).css("background-image", "url(" + employee.photo + ")")
        } else {
            $(`.imagePreview`).css("background-image", `url(${""})`);
        }

        $("#btn-update-employee").on("click", () => {
            this._clearInValid();

            let isValid = true;
            const vFields = [
                "input-create-lastName",
                "input-create-firstName",
                "input-create-password",
            ];

            vFields.forEach((field) => {
                const value = $(`#${field}`).val().trim();
                if (!value) {
                    isValid = false;
                    $(`#${field}`).addClass("is-invalid");
                    $(`#${field}`).after(`<div class="invalid-feedback error-message">Vui lòng nhập và giá trị hợp lệ!</div>`);
                }
            });

            if (isValid) {
                const activatedValue = $('input[name="activated2"]:checked').val();
                console.log("Activated Value:", activatedValue);

                const employeeData = {
                    // Update the fields you want to change
                    lastName: $("#input-update-lastName").val().trim(),
                    firstName: $("#input-update-firstName").val().trim(),
                    title: $("#input-update-title").val().trim(),
                    titleOfCourtesy: $("#input-update-titleOfCourtesy").val() || "",
                    birthDate: $("#input-update-birthDate").val().trim(),
                    hireDate: $("#input-update-hireDate").val().trim(),
                    address: $("#input-update-address").val().trim(),
                    region: $("#input-update-region").val().trim(),
                    postalCode: $("#input-update-code").val().trim(),
                    country: $("#input-update-country").val().trim(),
                    homePhone: $("#input-update-phone").val().trim(),
                    extension: $("#input-update-ext").val().trim(),
                    photo: imageURL,
                    reportsTo: $("#input-update-reportTo").val().trim(),
                    username: $("#input-update-username").val().trim(),
                    password: $("#input-update-password").val().trim(),
                    email: $("#input-update-email").val().trim(),
                    profile: $("#input-update-profile").val() || "",
                    notes: $("#input-update-note").val().trim(),
                    userLevel: $("#input-update-userLevel").val(),
                    activated: $('input[name="activated2"]:checked').val(),
                };

                // Send the updated employeeData to the server to update the employee
                this.vApi.onUpdateEmployeeClick(employee.employeeId, employeeData, (data) => {
                    console.log(data);
                });
            }
        });
    }



    _deleteEmployee(data) {
        $("#btn-confirm-delete-employee").on("click", () => {
            this.vApi.onDeleteEmployeeClick(data.id)
        })

    }
    _clearInValid(input) {
        if (input) {
            input.removeClass("is-invalid");
            $(".invalid-feedback").remove();
        }
        $(".form-control").removeClass("is-invalid");
        $(".invalid-feedback").remove();
    }

    _clearInput() {
        $("#create-employee-form").find("input").val("");
        $("#create-employee-form").find("select").val("")
    }
    openModal(type, data) {
        this._logout();
        if (type === "create") {
            this._clearInput()
            this._onEventListner()
            this._createEmployee()
        } else {
            if (type === "edit") {
                this._updateEmployee(data)
                $("#update-employee-modal").modal("show")
            } else {
                this._deleteEmployee(data)
                $("#delete-confirm-modal").modal("show")
            }
        }

    }
}

/*** REGION 4 - vùng để gọi lên cơ sở dữ liệu lấy đa ta về*/
class CallApi {
    constructor() {
        this.token = this.getCookie("token");
    }

    setToken(token) {
        this.token = token;
    }

    getHeaders() {
        return {
            Authorization: "Bearer " + this.token
        };
    }
    setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    redirectToLogin() {
        // Trước khi logout cần xóa token đã lưu trong cookie
        this.setCookie("token", "", 1);
        window.location.href = "http://127.0.0.1:5500/real-estates-frontend/html/login.html";
    }

    onShowToast(paramTitle, paramMessage) {
        $('#myToast .mr-auto').text(paramTitle)
        $('#myToast .toast-body').text(paramMessage);
        $('#myToast').toast('show');
    }

    onGetPhotosClick(paramCallbackFn) {
        $.ajax({
            url: gPHOTO_URL,
            method: 'GET',
            headers: this.getHeaders(),
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onGetEmployeesClick(paramCallbackFn) {
        $.ajax({
            url: gEMPLOYEE_URL,
            method: 'GET',
            headers: this.getHeaders(),
            success: function (data) {
                paramCallbackFn(data);
            },
            error: (jqXHR, textStatus, errorThrown) => {
                console.log('Error:', errorThrown);
                this.redirectToLogin();
            }
        });
    }

    onGetEmployeeByIdClick(employeeId, paramCallbackFn) {
        $.ajax({
            url: gEMPLOYEE_URL + "/" + employeeId,
            method: 'GET',
            headers: this.getHeaders(),
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onCreateEmployeeClick(employeeData, paramCallbackFn) {
        $.ajax({
            url: gEMPLOYEE_URL,
            method: 'POST',
            data: JSON.stringify(employeeData),
            headers: this.getHeaders(),
            contentType: 'application/json',
            success: (data) => {
                paramCallbackFn(data);
                const render = new RenderPage()
                render.renderPage()
                this.onShowToast("Tạo thành công", "Tạo employee thành công!!")
                $("#create-employee-modal").modal("hide");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown, textStatus);
            }
        });
    }

    onUpdateEmployeeClick(employeeId, employeeData, paramCallbackFn) {
        $.ajax({
            url: gEMPLOYEE_URL + "/" + employeeId,
            method: 'PUT',
            data: JSON.stringify(employeeData),
            headers: this.getHeaders(),
            contentType: 'application/json',
            success: (data) => {
                const render = new RenderPage()
                render.renderPage()
                paramCallbackFn(data);
                $('#update-employee-modal').modal('hide');
                this.onShowToast("Sửa thành công", "Sửa employee thành công!!")
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onDeleteEmployeeClick(employeeId) {
        $.ajax({
            url: gEMPLOYEE_URL + "/" + employeeId,
            method: 'DELETE',
            headers: this.getHeaders(),
            success: () => {
                this.onShowToast("Xóa thành công", "bạn đã xóa employee thành công!!")
                const render = new RenderPage()
                render.renderPage()
                $("#delete-confirm-modal").modal("hide");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onUploadFile(file, paramCallbackFn) {
        const formData = new FormData();
        formData.append('image', file);

        $.ajax({
            url: gUPLOAD_FILE_URL,
            method: 'POST',
            data: formData,
            processData: false,
            contentType: false,
            headers: this.getHeaders(),
            success: (response) => {
                paramCallbackFn(response);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onExportExcelClick(paramCallbackFn) {
        $.ajax({
            url: `http://localhost:8080/api/export/employees/excel`,
            method: 'GET',
            headers: this.getHeaders(),
            xhrFields: {
                responseType: "blob"
            },
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
}
