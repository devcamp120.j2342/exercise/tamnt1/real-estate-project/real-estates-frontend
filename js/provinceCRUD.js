const gPROVINCE_URL = "http://localhost:8080/api/provinces";

const gCOLUMN_ID = {
    stt: 0,
    action: 1,
    name: 2,
    code: 3,

}
const gCOL_NAME = [
    "stt",
    "action",
    "name",
    "code",


]
//Hàm chính để load html hiển thị ra bảng
class Main {
    constructor() {
        $(document).ready(() => {
            this.vOrderList = new RenderPage()
            this.vOrderList.renderPage()
            this.vModal = new Modal()
            this.vModal.openModal("create")
            $('.select2').select2()
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })
        })
    }

}
new Main()
/*** REGION 2 - vùng để render ra bảng*/
class RenderPage {
    constructor() {
        this.vApi = new CallApi()
        this.vModal = new Modal()

    }

    //Hàm gọi api lấy danh sách đơn hàng
    _getProvinceList() {
        this.vApi.onGetProvincesClick((paramprovince) => {
            this._createProvinceTable(paramprovince)
        })
    }
    //Hiển thị tên sau khi login
    _showAdminName() {
        const name = JSON.parse(localStorage.getItem('login'));
        if (name.accessToken) {
            $(".profile-name").text(name?.username)
            if (!name.roles.includes("ROLE_ADMIN")) {
                $('.user-item').hide();
            }
        } else {
            this.vApi.redirectToLogin()
        }

    }


    //Hàm tạo các thành phần của bảng
    _createProvinceTable(paramprovince) {
        let stt = 1
        if ($.fn.DataTable.isDataTable('#table-province')) {
            $('#table-province').DataTable().destroy();
        }
        const vOrderTable = $("#table-province").DataTable({
            // Khai báo các cột của datatable
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false,
            "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],
            "columns": [
                { "data": null },
                { "data": gCOL_NAME[gCOLUMN_ID.action] },
                { "data": gCOL_NAME[gCOLUMN_ID.name] },
                { "data": gCOL_NAME[gCOLUMN_ID.code] },


            ],
            // Ghi đè nội dung của cột action
            "columnDefs": [
                {
                    targets: gCOLUMN_ID.stt,
                    render: function () {
                        return stt++
                    }
                }, {

                    targets: gCOLUMN_ID.action,
                    defaultContent: `
                              <img class="edit-province" src="https://cdn0.iconfinder.com/data/icons/glyphpack/45/edit-alt-512.png" style="width: 20px;cursor:pointer;">
                              <img class="delete-province" src="https://cdn4.iconfinder.com/data/icons/complete-common-version-6-4/1024/trash-512.png" style="width: 20px;cursor:pointer;">
                            `,
                    createdCell: (cell, cellData, rowData, rowIndex, colIndex) => {
                        if (colIndex === gCOLUMN_ID.action) {
                            $(cell).find('.edit-province').on('click', () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                this.vModal.openModal('edit', vData);
                            });

                            $(cell).find('.delete-province').on('click', () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                console.log(vData)
                                this.vModal.openModal('delete', vData);
                            });
                        }
                    }
                }],

        });
        vOrderTable.clear() // xóa toàn bộ dữ liệu trong bảng
        vOrderTable.rows.add(paramprovince) // cập nhật dữ liệu cho bảng
        vOrderTable.draw()// hàm vẻ lại bảng
        vOrderTable.buttons().container().appendTo('.example1_wrapper .col-md-6:eq(0)')
    }

    _saveExcelFile(data, filename) {
        const blob = new Blob([data], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
        const url = window.URL.createObjectURL(blob);
        const link = document.createElement("a");
        link.href = url;
        link.download = filename;
        link.click();
    }

    _exportToExcel() {
        $("#btn-export-excel").on("click", () => {
            this.vApi.onExportExcelClick((data) => {
                console.log(data)
                this._saveExcelFile(data, "province.xlsx");
            });
        });
    }


    // Hàm sẽ được gọi ở class Main 
    renderPage() {
        this._showAdminName()
        this._exportToExcel()
        this._getProvinceList()
    }
}

/*** REGION 3 - vùng hiện modal*/

class Modal {
    constructor() {
        this.vApi = new CallApi()
    }

    _onEventListner() {
        $("#btn-add-province").on("click", () => {
            $("#create-province-modal").modal("show")
        })
        $(".logout").on("click", () => {
            $("#logout-confirm-modal").modal("show")
        })

    }

    _logout() {
        $("#btn-confirm-logout").on("click", () => {
            this.vApi.redirectToLogin()
        })

    }

    _createProvince() {
        this._clearInput();
        $("#btn-create-province").on("click", () => {
            this._clearInValid();

            let isValid = true;
            const vFields = [
                "input-create-name",

            ];

            vFields.forEach((field) => {
                const value = $(`#${field}`).val().trim();
                if (!value) {
                    isValid = false;
                    $(`#${field}`).addClass("is-invalid");
                    $(`#${field}`).after(`<div class="invalid-feedback error-message">Vui lòng nhập và giá trị hợp lệ!.</div>`);
                }
            });

            if (isValid) {
                const provinceData = {
                    name: $("#input-create-name").val().trim(),
                    code: $("#input-create-code").val().trim(),

                };
                this.vApi.onCreateProvinceClick(provinceData, (data) => {
                    console.log(data);
                });
            }
        }).bind(this);
    }

    _updateProvince(province) {
        $("#input-update-name").val(province.name);
        $("#input-update-code").val(province.code);


        $("#btn-update-province").off("click").on("click", () => {
            this._clearInValid();
            let isValid = true;
            const requiredFields = ["input-update-name",];

            requiredFields.forEach((field) => {
                if (!$(`#${field}`).val().trim()) {
                    isValid = false;
                    $(`#${field}`).addClass("is-invalid");
                    $(`#${field}`).after('<div class="invalid-feedback">Vui lòng nhập giá trị hợp lệ!</div>');
                }
            });

            if (isValid) {
                const updatedprovince = {
                    name: $("#input-update-name").val().trim(),
                    code: $("#input-update-code").val().trim(),

                };

                this.vApi.onUpdateProvinceClick(province.id, updatedprovince, (data) => {
                    console.log(data);
                });
            }
        }).bind(this);
    }


    _deleteProvince(data) {
        $("#btn-confirm-delete-province").on("click", () => {
            this.vApi.onDeleteProvinceClick(data.id)
        })

    }
    _clearInValid(input) {
        if (input) {
            input.removeClass("is-invalid");
            $(".invalid-feedback").remove();
        }
        $(".form-control").removeClass("is-invalid");
        $(".invalid-feedback").remove();
    }

    _clearInput() {
        $("#create-province-form").find("input").val("");
        $("#create-province-form").find("select").val("")
    }
    openModal(type, data) {
        this._logout();
        if (type === "create") {
            this._clearInput();
            this._onEventListner();
            this._createProvince();
        } else {
            if (type === "edit") {
                this._updateProvince(data)
                $("#update-province-modal").modal("show")
            } else {
                this._deleteProvince(data)
                $("#delete-confirm-modal").modal("show")
            }
        }

    }
}

/*** REGION 4 - vùng để gọi lên cơ sở dữ liệu lấy đa ta về*/
class CallApi {
    constructor() {
        this.token = this.getCookie("token");
    }
    setToken(token) {
        this.token = token;
    }

    getHeaders() {
        return {
            Authorization: "Bearer " + this.token
        };
    }
    setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    redirectToLogin() {
        // Trước khi logout cần xóa token đã lưu trong cookie
        this.setCookie("token", "", 1);
        window.location.href = "http://127.0.0.1:5500/real-estates-frontend/html/login.html";
    }

    onShowToast(paramTitle, paramMessage) {
        $('#myToast .mr-auto').text(paramTitle)
        $('#myToast .toast-body').text(paramMessage);
        $('#myToast').toast('show');
    }

    onGetProvincesClick(paramCallbackFn) {
        $.ajax({
            url: gPROVINCE_URL,
            method: 'GET',
            headers: this.getHeaders(),
            success: function (data) {
                paramCallbackFn(data);
            },
            error: (jqXHR, textStatus, errorThrown) => {
                console.log('Error:', errorThrown);
                this.redirectToLogin()
            }
        });
    }

    onGetProvinceByIdClick(provinceId, paramCallbackFn) {
        $.ajax({
            url: gPROVINCE_URL + "/" + provinceId,
            headers: this.getHeaders(),
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onCreateProvinceClick(provinceData, paramCallbackFn) {
        $.ajax({
            url: gPROVINCE_URL,
            method: 'POST',
            headers: this.getHeaders(),
            data: JSON.stringify(provinceData),
            headers: this.getHeaders(),
            contentType: 'application/json',
            success: (data) => {
                paramCallbackFn(data);
                const render = new RenderPage();
                render.renderPage();
                this.onShowToast("Tạo thành công", "Tạo province thành công!!");
                $("#create-province-modal").modal("hide");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown, textStatus);
            }
        });
    }

    onUpdateProvinceClick(provinceId, provinceData, paramCallbackFn) {
        $.ajax({
            url: gPROVINCE_URL + "/" + provinceId,
            method: 'PUT',
            data: JSON.stringify(provinceData),
            headers: this.getHeaders(),
            contentType: 'application/json',
            success: (data) => {
                const render = new RenderPage();
                render.renderPage();
                paramCallbackFn(data);
                $('#update-province-modal').modal('hide');
                this.onShowToast("Sửa thành công", "Sửa province thành công!!");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onDeleteProvinceClick(provinceId) {
        $.ajax({
            url: gPROVINCE_URL + "/" + provinceId,
            method: 'DELETE',
            headers: this.getHeaders(),
            success: () => {
                this.onShowToast("Xóa thành công", "Bạn đã xóa province thành công!!");
                const render = new RenderPage();
                render.renderPage();
                $("#delete-confirm-modal").modal("hide");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onExportExcelClick(paramCallbackFn) {
        $.ajax({
            url: `http://localhost:8080/api/export/provinces/excel`,
            method: 'GET',
            headers: this.getHeaders(),
            xhrFields: {
                responseType: "blob"
            },
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }


    getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
}
