/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gREGION_LINK_URL = "http://localhost:8080/api/region-links";
const gUPLOAD_FILE_URL = "http://localhost:8080/api/upload";
const gCOLUMN_ID = {
    stt: 0,
    action: 1,
    name: 2,
    latitude: 3,
    longitude: 4,

}
const gCOL_NAME = [
    "stt",
    "action",
    "name",
    "latitude",
    "longitude"


]
//Hàm chính để load html hiển thị ra bảng
class Main {
    constructor() {
        $(document).ready(() => {
            this.vOrderList = new RenderPage()
            this.vOrderList.renderPage()
            this.vModal = new Modal()
            this.vModal.openModal("create")
            $('.select2').select2()
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })
        })
    }

}
new Main()
/*** REGION 2 - vùng để render ra bảng*/
class RenderPage {
    constructor() {
        this.vApi = new CallApi()
        this.vModal = new Modal()

    }

    //Hàm gọi api lấy danh sách đơn hàng
    _getRegionLinkList() {
        this.vApi.onGetRegionLinksClick((paramRegionLink) => {
            this._createRegionLinkTable(paramRegionLink)
        })
    }
    //Hiển thị tên sau khi login
    _showAdminName() {
        const name = JSON.parse(localStorage.getItem('login'));
        if(name.accessToken) {
          $(".profile-name").text(name?.username)
          if(!name.roles.includes("ROLE_ADMIN")) {
            $('.user-item').hide();
        }
        }else {
          this.vApi.redirectToLogin()
        }
    
      }

    //Hàm tạo các thành phần của bảng
    _createRegionLinkTable(paramRegionLink) {
        let stt = 1
        if ($.fn.DataTable.isDataTable('#table-regionLink')) {
            $('#table-regionLink').DataTable().destroy();
        }
        const vOrderTable = $("#table-regionLink").DataTable({
            // Khai báo các cột của datatable
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false,
            "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],
            "columns": [
                { "data": null },
                { "data": gCOL_NAME[gCOLUMN_ID.action] },
                { "data": gCOL_NAME[gCOLUMN_ID.name] },
                { "data": gCOL_NAME[gCOLUMN_ID.latitude] },
                { "data": gCOL_NAME[gCOLUMN_ID.longitude] },


            ],
            // Ghi đè nội dung của cột action
            "columnDefs": [
                {
                    targets: gCOLUMN_ID.stt,
                    render: function () {
                        return stt++
                    }
                }, {

                    targets: gCOLUMN_ID.action,
                    defaultContent: `
                              <img class="edit-regionLink" src="https://cdn0.iconfinder.com/data/icons/glyphpack/45/edit-alt-512.png" style="width: 20px;cursor:pointer;">
                              <img class="delete-regionLink" src="https://cdn4.iconfinder.com/data/icons/complete-common-version-6-4/1024/trash-512.png" style="width: 20px;cursor:pointer;">
                            `,
                    createdCell: (cell, cellData, rowData, rowIndex, colIndex) => {
                        if (colIndex === gCOLUMN_ID.action) {
                            $(cell).find('.edit-regionLink').on('click', () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                this.vModal.openModal('edit', vData);
                            });

                            $(cell).find('.delete-regionLink').on('click', () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                console.log(vData)
                                this.vModal.openModal('delete', vData);
                            });
                        }
                    }
                }],

        });
        vOrderTable.clear() // xóa toàn bộ dữ liệu trong bảng
        vOrderTable.rows.add(paramRegionLink) // cập nhật dữ liệu cho bảng
        vOrderTable.draw()// hàm vẻ lại bảng
        vOrderTable.buttons().container().appendTo('.example1_wrapper .col-md-6:eq(0)')
    }

    _saveExcelFile(data, filename) {
        const blob = new Blob([data], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
        const url = window.URL.createObjectURL(blob);
        const link = document.createElement("a");
        link.href = url;
        link.download = filename;
        link.click();
    }

    _exportToExcel() {
        $("#btn-export-excel").on("click", () => {
            this.vApi.onExportExcelClick((data) => {
                console.log(data)
                this._saveExcelFile(data, "regionLink.xlsx");
            });
        });
    }


    // Hàm sẽ được gọi ở class Main 
    renderPage() {
        this._showAdminName()
        this._exportToExcel()
        this._getRegionLinkList()
    }
}

/*** REGION 3 - vùng hiện modal*/

class Modal {
    constructor() {
        this.vApi = new CallApi()
    }

    _onEventListner() {
        $("#btn-add-regionLink").on("click", () => {
            $("#create-regionLink-modal").modal("show")

        })
        $(".logout").on("click", () => {
            $("#logout-confirm-modal").modal("show")
        })

    }

    _logout() {
        $("#btn-confirm-logout").on("click", () => {
            this.vApi.redirectToLogin()
        })

    }
    _createRegionLink() {
        this._clearInValid();
        let imageURL;
        $("#input-create-photo").on("change", (event) => {
            const fileInput = event.target;
            const file = fileInput.files[0];
            this.vApi.onUploadFile(file, (response) => {
                imageURL = response
                $(".imagePreview2 ").css("background-image", "url(" + response + ")");
            });
        });

        $("#btn-create-regionLink").on("click", () => {

            let isValid = true;
            const vFields = [
                "input-create-name",
            ];


            vFields.forEach((field) => {
                const value = $(`#${field}`).val().trim();
                if (!value) {
                    isValid = false;
                    $(`#${field}`).addClass("is-invalid");
                    $(`#${field}`).after(`<div class="invalid-feedback error-message">Vui lòng nhập và giá trị hợp lệ!</div>`);
                }
            });

            if (isValid) {
                const regionLinkData = {
                    name: $("#input-create-name").val().trim(),
                    description: $("#input-create-desc").val().trim(),
                    photo: imageURL,
                    address: $("#input-create-address").val().trim(),
                    latitude: $("#input-create-latedute").val().trim(),
                    longitude: $("#input-create-longtadue").val().trim(),
                };
                this.vApi.onCreateRegionLinkClick(regionLinkData, (data) => {
                    console.log(data)
                })
            }
        }).bind(this);


    }

    _updateRegionLink(regionLink) {
        let imageURL;
        $("#input-update-photo").on("change", (event) => {
            const fileInput = event.target;
            const file = fileInput.files[0];
            this.vApi.onUploadFile(file, (response) => {
                imageURL = response
            });
        });
        $("#input-update-name").val(regionLink.name);
        $("#input-update-desc").val(regionLink.requiredDate);
        $("#input-update-address").val(regionLink.address);
        $("#input-update-latedute").val(regionLink.latitude);
        $("#input-update-longtadue").val(regionLink.longitude);
        if (regionLink.photo) {
            $(`.imagePreview`).css("background-image", `url(${regionLink.photo})`);
        } else {
            $(`.imagePreview`).css("background-image", `url(${""})`);
        }

        $("#btn-update-regionLink").on("click", () => {
            this._clearInValid();
            let isValid = true;
            const vFields = [
                "input-update-name",
            ];

            vFields.forEach((field) => {
                const value = $(`#${field}`).val().trim();
                if (!value) {
                    isValid = false;
                    $(`#${field}`).addClass("is-invalid");
                    $(`#${field}`).after(`<div class="invalid-feedback error-message">Vui lòng nhập và giá trị hợp lệ!</div>`);
                }
            });

            if (isValid) {
                const regionLinkData = {
                    name: $("#input-update-name").val().trim(),
                    requiredDate: $("#input-update-desc").val().trim(),
                    photo: imageURL,
                    address: $("#input-update-address").val().trim(),
                    latedute: $("#input-update-latedute").val().trim(),
                    longtadue: $("#input-update-longtadue").val().trim(),
                };

                this.vApi.onUpdateRegionLinkClick(regionLink.id, regionLinkData, (data) => {
                    console.log(data);
                });
            }
        }).bind(this);
    }


    _deleteRegionLink(data) {
        $("#btn-confirm-delete-regionLink").on("click", () => {
            this.vApi.onDeleteRegionLinkClick(data.id)
        })

    }
    _clearInValid(input) {
        if (input) {
            input.removeClass("is-invalid");
            $(".invalid-feedback").remove();
        }
        $(".form-control").removeClass("is-invalid");
        $(".invalid-feedback").remove();
    }

    _clearInput() {
        $("#create-regionLink-form").find("input").val("");
        $("#create-regionLink-form").find("select").val("")
    }
    openModal(type, data) {
        this._logout();
        if (type === "create") {
            this._clearInput();
            this._onEventListner();
            this._createRegionLink();
        } else {
            if (type === "edit") {
                this._updateRegionLink(data)
                $("#update-regionLink-modal").modal("show")
            } else {
                this._deleteRegionLink(data)
                $("#delete-confirm-modal").modal("show")
            }
        }

    }
}

/*** REGION 4 - vùng để gọi lên cơ sở dữ liệu lấy đa ta về*/
class CallApi {
    constructor() {
        this.token = this.getCookie("token");
    }

    setToken(token) {
        this.token = token;
    }

    getHeaders() {
        return {
            Authorization: "Bearer " + this.token
        };
    }

    setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    redirectToLogin() {
        // Trước khi logout cần xóa token đã lưu trong cookie
        this.setCookie("token", "", 1);
        window.location.href = "http://127.0.0.1:5500/real-estates-frontend/html/login.html";
    }
    onShowToast(paramTitle, paramMessage) {
        $('#myToast .mr-auto').text(paramTitle)
        $('#myToast .toast-body').text(paramMessage);
        $('#myToast').toast('show');
    }
    onUploadFile(file, paramCallbackFn) {
        const formData = new FormData();
        formData.append('image', file);

        $.ajax({
            url: gUPLOAD_FILE_URL,
            method: 'POST',
            data: formData,
            headers: this.getHeaders(),
            processData: false,
            contentType: false,
            success: (response) => {
                paramCallbackFn(response);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onGetRegionLinksClick(paramCallbackFn) {
        $.ajax({
            url: gREGION_LINK_URL,
            method: 'GET',
            headers: this.getHeaders(),
            success: function (data) {
                paramCallbackFn(data);
            },
            error: (jqXHR, textStatus, errorThrown) => {
                console.log('Error:', errorThrown);
                this.redirectToLogin();
            }
        });
    }

    onGetRegionLinkByIdClick(regionLinkId, paramCallbackFn) {
        $.ajax({
            url: gREGION_LINK_URL + "/" + regionLinkId,
            method: 'GET',
            headers: this.getHeaders(),
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onCreateRegionLinkClick(regionLinkData, paramCallbackFn) {
        $.ajax({
            url: gREGION_LINK_URL,
            method: 'POST',
            data: JSON.stringify(regionLinkData),
            headers: this.getHeaders(),
            contentType: 'application/json',
            success: (data) => {
                paramCallbackFn(data);
                const render = new RenderPage()
                render.renderPage()
                this.onShowToast("Tạo thành công", "Tạo regionLink thành công!!")
                $("#create-regionLink-modal").modal("hide");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown, textStatus);
            }
        });
    }

    onUpdateRegionLinkClick(regionLinkId, regionLinkData, paramCallbackFn) {
        $.ajax({
            url: gREGION_LINK_URL + "/" + regionLinkId,
            method: 'PUT',
            data: JSON.stringify(regionLinkData),
            headers: this.getHeaders(),
            contentType: 'application/json',
            success: (data) => {
                const render = new RenderPage()
                render.renderPage()
                paramCallbackFn(data);
                $('#update-regionLink-modal').modal('hide');
                this.onShowToast("Sửa thành công", "Sửa regionLink thành công!!")
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onDeleteRegionLinkClick(regionLinkId) {
        $.ajax({
            url: gREGION_LINK_URL + "/" + regionLinkId,
            method: 'DELETE',
            headers: this.getHeaders(),
            success: () => {
                this.onShowToast("Xóa thành công", "bạn đã xóa regionLink thành công!!")
                const render = new RenderPage()
                render.renderPage()
                $("#delete-confirm-modal").modal("hide");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onExportExcelClick(paramCallbackFn) {
        $.ajax({
            url: `http://localhost:8080/api/export/regionLinks/excel`,
            method: 'GET',
            xhrFields: {
                responseType: "blob"
            },
            headers: this.getHeaders(),
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
}
