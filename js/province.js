/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gREAL_ESTATE_URL = "http://localhost:8080/api/real-estates";
const gPROVINCE_URL = "http://localhost:8080/api/provinces";
const gDISTRICT_URL = "http://localhost:8080/api/districts";
const gSTREET_URL = "http://localhost:8080/api/streets";
const gWARD_URL = "http://localhost:8080/api/wards";
const gPROJECT_URL = "http://localhost:8080/api/projects";
const gCUSTOMER_URL = "http://localhost:8080/api/customers";
const gUPLOAD_FILE_URL = "http://localhost:8080/api/upload";
const gCUSTOMER_INFO_URL = "http://localhost:8080/users/me"
const gCOLUMN_ID = {
    stt: 0,
    action: 1,
    district: 2,
    name: 3,
    code: 4,
    districtId: 5,
    provinceId: 6,
    totalRealEstate: 7,

}
const gCOL_NAME = [
    "stt",
    "action",
    "district",
    "_name",
    "_code",
    "districtId",
    "provinceId",
    "totalRealEstate"
]
//Hàm chính để load html hiển thị ra bảng
class Main {
    constructor() {
        $(document).ready(() => {
            this.vOrderList = new RenderPage()
            this.vOrderList.renderPage()
            this.vModal = new Modal()
            this.vModal.openModal("create")
            $('.select2').select2()
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })
        })
    }

}
new Main()
/*** REGION 2 - vùng để render ra bảng*/
class RenderPage {
    constructor() {
        this.vApi = new CallApi()
        this.vModal = new Modal()

    }

    //Hàm gọi api lấy danh sách đơn hàng
    _getProvinceList() {
        this.vApi.onGetCountRealEstate((paramResponse) => {
            console.log(paramResponse)
            this._createProvinceTable(paramResponse)
        })
    }
    //Hiển thị tên sau khi login
    _showAdminName() {
        const name = JSON.parse(localStorage.getItem('login'));
        if (name.accessToken) {
            $(".profile-name").text(name?.username)
            if(!name.roles.includes("ROLE_ADMIN")) {
                $('.user-item').hide();
            }
        } else {
            this.vApi.redirectToLogin()
        }

    }


    //Hàm tạo các thành phần của bảng
    _createProvinceTable(paramResponse) {
        let stt = 1
        if ($.fn.DataTable.isDataTable('#table-province')) {
            $('#table-province').DataTable().destroy();
        }
        const vOrderTable = $("#table-province").DataTable({
            // Khai báo các cột của datatable
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false,
            "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],
            "columns": [
                { "data": null },
                { "data": gCOL_NAME[gCOLUMN_ID.action] },
                { "data": gCOL_NAME[gCOLUMN_ID.code] },
                { "data": gCOL_NAME[gCOLUMN_ID.name] },
                { "data": gCOL_NAME[gCOLUMN_ID.district] },
                { "data": gCOL_NAME[gCOLUMN_ID.districtId] },
                { "data": gCOL_NAME[gCOLUMN_ID.provinceId] },
                { "data": gCOL_NAME[gCOLUMN_ID.totalRealEstate] },


            ],
            // Ghi đè nội dung của cột action
            "columnDefs": [
                {
                    targets: gCOLUMN_ID.stt,
                    render: function () {
                        return stt++
                    }
                }, {

                    targets: gCOLUMN_ID.action,
                    defaultContent: `
                              <img class="edit-province" src="https://cdn0.iconfinder.com/data/icons/font-awesome-solid-vol-2/576/eye-512.png" style="width: 20px;cursor:pointer;">
                              <img class="delete-province" src="https://cdn4.iconfinder.com/data/icons/complete-common-version-6-4/1024/trash-512.png" style="width: 20px;cursor:pointer;">
                            `,
                    createdCell: (cell, cellData, rowData, rowIndex, colIndex) => {
                        if (colIndex === gCOLUMN_ID.action) {
                            $(cell).find('.edit-province').on('click', () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                this.vModal.openModal('edit', vData);
                            });

                            $(cell).find('.delete-province').on('click', () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                console.log(vData)
                                this.vModal.openModal('delete', vData);
                            });
                        }
                    }
                }],

        });
        vOrderTable.clear() // xóa toàn bộ dữ liệu trong bảng
        vOrderTable.rows.add(paramResponse) // cập nhật dữ liệu cho bảng
        vOrderTable.draw()// hàm vẻ lại bảng
        vOrderTable.buttons().container().appendTo('.example1_wrapper .col-md-6:eq(0)')
    }

    _saveExcelFile(data, filename) {
        const blob = new Blob([data], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
        const url = window.URL.createObjectURL(blob);
        const link = document.createElement("a");
        link.href = url;
        link.download = filename;
        link.click();
    }

    _exportToExcel() {
        $("#btn-export-excel").on("click", () => {
            this.vApi.onExportExcelClick((data) => {
                console.log(data)
                this._saveExcelFile(data, "province.xlsx");
            });
        });
    }


    // Hàm sẽ được gọi ở class Main 
    renderPage() {
        this._showAdminName()
        this._exportToExcel()
        this._getProvinceList()
    }
}

/*** REGION 3 - vùng hiện modal*/

class Modal {
    constructor() {
        this.vApi = new CallApi()
    }

    _onEventListner() {
        $("#btn-add-province").on("click", () => {
            $("#create-province-modal").modal("show")
        })
        $(".logout").on("click", () => {
            $("#logout-confirm-modal").modal("show")
        })

    }

    _logout() {
        $("#btn-confirm-logout").on("click", () => {
            this.vApi.redirectToLogin()
        })

    }
    _getProvinceList(type) {
        let provinceDropdown
        if (type === "update") {
          provinceDropdown = $("#input-update-provinceId");
        } else {
          provinceDropdown = $("#input-create-provinceId");
        }
        this.vApi.onGetProvinceList((provinces) => {
          provinceDropdown.empty();
          provinceDropdown.append('<option value="" selected>Tỉnh</option>');
    
          provinces.forEach((province) => {
            const option = `<option value="${province.id}">${province.name}</option>`;
            provinceDropdown.append(option);
          });
        });
      }
      _getDistrictList(type, provinceId) {
        let districtDropdown
        if (type === "update") {
          districtDropdown = $("#input-update-districtId");
        } else {
          districtDropdown = $("#input-create-districtId");
        }
    
        this.vApi.onGetDistrictListByProvinceId(provinceId, (districts) => {
          districtDropdown.empty();
          districtDropdown.append('<option value="" selected>Huyện</option>');
    
          districts.forEach((district) => {
            const option = `<option value="${district.id}">${district.name}</option>`;
            districtDropdown.append(option);
          });
        })
    
      }
   
      _getWardList(type, districtId) {
        let wardDropdown
        if (type === "update") {
          wardDropdown = $("#input-update-wardId");
        } else {
          wardDropdown = $("#input-create-wardId");
        }
    
        this.vApi.onGetWardByDistrictId(districtId, (wards) => {
          wardDropdown.empty();
          wardDropdown.append('<option value="" selected>Xã</option>');
    
          wards.forEach((ward) => {
            const option = `<option value="${ward.id}">${ward.name}</option>`;
            wardDropdown.append(option);
          });
        })
    
      }
      _getStreetList(type, districtId) {
        let streetDropdown
        if (type === "update") {
          streetDropdown = $("#input-update-streetId");
        } else {
          streetDropdown = $("#input-create-streetId");
        }
    
        this.vApi.onGetStreetByDistrictId(districtId, (streets) => {
          streetDropdown.empty();
          streetDropdown.append('<option value="" selected>Đường</option>');
    
          streets.forEach((street) => {
            const option = `<option value="${street.id}">${street.name}</option>`;
            streetDropdown.append(option);
          });
        })
    
      }
      _getProjectList(type) {
        let projectDropdown
        if (type === "update") {
          projectDropdown = $("#input-update-project");
        } else {
          projectDropdown = $("#input-create-project");
        }
    
        this.vApi.onGetProjectList((projects) => {
          projectDropdown.empty();
          projectDropdown.append('<option value="" selected>Dự án</option>');
    
          projects.forEach((project) => {
            const option = `<option value="${project.id}">${project.name}</option>`;
            projectDropdown.append(option);
          });
        })
      }
    
      _getCustomerList(type) {
        let customerDropdown
        if (type === "update") {
          customerDropdown = $("#input-update-customer");
        } else {
          customerDropdown = $("#input-create-customer");
        }
        this.vApi.onGetCustomerList((customers) => {
          customerDropdown.empty();
          customerDropdown.append('<option value="" selected>Khách hàng</option>');
    
          customers.forEach((customer) => {
            const option = `<option value="${customer.id}">${customer.contactName}</option>`;
            customerDropdown.append(option);
          });
        })
      }

    _createProvince() {
        this._clearInput();
        $("#btn-create-province").on("click", () => {
            this._clearInValid();

            let isValid = true;
            const vFields = [
            ];

            vFields.forEach((field) => {
                const value = $(`#${field}`).val().trim();
                if (!value) {
                    isValid = false;
                    $(`#${field}`).addClass("is-invalid");
                    $(`#${field}`).after(`<div class="invalid-feedback error-message">Vui lòng nhập và giá trị hợp lệ!.</div>`);
                }
            });

            if (isValid) {
                const provinceData = {
                    name: $("#input-create-checkNumber").val().trim(),
                    code: $("#input-create-provinceDate").val().trim(),
                };
                this.vApi.onCreateProvinceClick(provinceData, (data) => {
                    console.log(data);
                });
            }
        }).bind(this);
    }

    _updateProvince(data) {
        this.vApi.onGetCustomsRealEstate(data.districtId, data.provinceId, (response) => {
            const realEstateList = response.data;
    
            const $modalBody = $('#update-province-modal .modal-body');
            $modalBody.empty();
    
            realEstateList.forEach((realEstate) => {
                const card = `
                    <div class="card card-item1" style="cursor: pointer;" data-real-estate-id="${realEstate.id}">
                        <div class="card-body">
                            <h5 class="card-title">${realEstate.title}</h5>
                            <p class="card-text">${realEstate.address}</p>
                        </div>
                    </div>
                `;
                $modalBody.append(card);
            });
    
            // Add click event to each card
            $('.card-item1').click((event) => {
                const realEstateId = $(event.currentTarget).data('real-estate-id');
                this.vApi.onGetRealEstateByIdClick(realEstateId,(data)=> {
                    $("#update-realEstate-modal").modal("show")
                    this._updateRealEstate(data)
                })
            });
        });
    }
    

    async _updateRealEstate(data) {
        this._getProvinceList("update")
        this._getCustomerList("update")
        this._getProjectList("update")
        let imageURL;
        $("#input-update-provinceId").on("change", () => {
            const provinceId = $("#input-update-provinceId").val();
            this._getDistrictList("update", provinceId);
        });

        $("#input-update-districtId").on("change", () => {
            const districtId = $("#input-update-districtId").val();
            this._getWardList("update", districtId);
            this._getStreetList("update", districtId)
        });
        try {

            this._getDistrictList("update", data.provinceId);
            this._getWardList("update", data.districtId);
            this._getStreetList("update", data.districtId);
            if (data.provinceId) {
                const provinceData = await this.vApi.onGetProvinceById(data.provinceId);
                $('#input-update-provinceId').val(provinceData.id || '');
            }

            // Get the district name
            if (data.districtId) {
                const districtData = await this.vApi.onGetDistrictListById(data.districtId);
                $('#input-update-districtId').val(districtData.id || '')

            }

            // Get the ward name
            if (data.wardsId) {
                const wardData = await this.vApi.onGetWardById(data?.wardsId);
                $('#input-update-wardId').val(wardData.id || '')
            }

            if (data.streetId) {
                const streetData = await this.vApi.onGetStreetById(data.streetId);
                $('#input-update-streetId').val(streetData.id || '')
            }


            if (data.customerId) {
                const customerData = await this.vApi.onGetCustomerById(data.customerId);
                $('#input-update-customer').val(customerData.id || '');
            }

            if (data.projectId) {
                const projectData = await this.vApi.onGetProjectById(data.projectId);
                $('#input-update-project').val(projectData.id || '');
            }

            $("#input-update-photo").on("change", (event) => {
                const fileInput = event.target;
                const file = fileInput.files[0];
                this.vApi.onUploadFile(file, (response) => {
                    imageURL = response
                    $(".imagePreview ").css("background-image", "url(" + response + ")");
                });
            });

        } catch (error) {
            console.log(error)
        }
        if (data.photo) {

            $(`.imagePreview`).css("background-image", `url(${data.photo})`);
        } else {
            $(`.imagePreview`).css("background-image", `url(${""})`);
        }

        $('#input-update-address').val(data.address);
        $('#input-update-title').val(data.title);
        $('#input-update-type').val(data.type);
        $('#input-update-request2').val(data.request);
        $('#input-update-price').val(data.price);
        $('#input-update-priceMin').val(data.priceMin);
        $('#input-update-priceTime').val(data.priceTime);
        $('#input-update-apartCode').val(data.apartCode);
        $('#input-update-acreage').val(data.acreage);
        $('#input-update-bedroom').val(data.bedroom);
        $('#input-update-balcony').val(data.balcony);
        $('#input-update-landscapeView').val(data.landscapeView);
        $('#input-update-apartLoca').val(data.apartLoca);
        $('#input-update-apartType').val(data.apartType);
        $('#input-update-rent').val(data.priceRent);
        $('#input-update-direction').val(data.direction);
        $('#input-update-totalFloors').val(data.totalFloors);
        $('#input-update-floorNum').val(data.numberFloors);
        $('#input-update-balcony').val(data.balcony);
        $('#input-update-desc').val(data.description);
        $('#input-update-furniture').val(data.furniture);
        $('#input-update-width').val(data.widthY);
        $('#input-update-long').val(data.longX);
        $('#input-update-shape').val(data.shape);
        $('#input-update-longi').val(data.long);
        $('#input-update-lat').val(data.lat);
        $('#input-update-alleyMinWidth').val(data.alleyMinWidth);
        $('#input-update-clcl').val(data.clcl);

        $("#btn-update-realestate").off("click").on("click", (event) => {
            event.preventDefault();
            const address = $("#input-update-address").val();
            const photo = imageURL;
            const provinceId = parseInt($("#input-update-provinceId").val());
            const districtId = parseInt($("#input-update-districtId").val());
            const wardsId = parseInt($("#input-update-wardId").val());
            const streetId = parseInt($("#input-update-streetId").val());
            const projectId = parseInt($("#input-update-project").val());
            const title = $("#input-update-title").val();
            const type = parseInt($("#input-update-type").val());
            const request = parseInt($("#input-update-request").val());
            const customerId = parseInt($("#input-update-customer").val());
            const price = $("#input-update-price").val();
            const priceMin = $("#input-update-priceMin").val();
            const priceTime = parseInt($("#input-update-priceTime").val());
            const apartCode = $("#input-update-apartCode").val();
            const acreage = $("#input-update-acreage").val();
            const bedroom = $("#input-update-bedroom").val();
            const balcony = $("#input-update-balcony").val();
            const landscapeView = $("#input-update-landscapeView").val();
            const apartLoca = $("#input-update-apartLoca").val();
            const apartType = $("#input-update-apartType").val();
            const rentPrice = $("#input-update-rent").val();
            const bdsArea = $("#input-update-acreage").val();
            const houseDirection = $("#input-update-acreage").val();
            const totalFloors = parseInt($("#input-update-totalFloors").val());
            const numberFloors = parseInt($("#input-update-floorNum").val());
            const description = $("#input-update-desc").val();
            const width = $("#input-update-width").val();
            const length = $("#input-update-long").val();
            const createdBy = $("#input-update-createdBy").prop('checked');
            const shape = $("#input-update-shape").val();
            const adjacentRoad = $("#input-update-adjacentRoad").val();
            const alleyMinWidth = $("#input-update-alleyMinWidth").val();
            const ctxdPrice = $("#input-update-ctxdPrice").val();
            const ctxdValue = $("#input-update-ctxdValue").val();
            const alleyWidth = $("#input-update-alleyMinWidth").val();
            const factor = $("#input-update-factor").val();
            const structure = $("#input-update-structure").val();
            const clcl = $("#input-update-clcl").val();
            const lat = $("#input-update-lat").val();
            const long = ($("#input-update-long").val());

            const realEstateData = {
                address,
                photo,
                provinceId,
                districtId,
                wardsId,
                streetId,
                projectId,
                title,
                type,
                request,
                customerId,
                price,
                priceMin,
                priceTime,
                apartCode,
                acreage,
                bedroom,
                balcony,
                landscapeView,
                apartLoca,
                apartType,
                rentPrice,
                bdsArea,
                houseDirection,
                totalFloors,
                numberFloors,
                description,
                width,
                length,
                createdBy,
                shape,
                adjacentRoad,
                alleyMinWidth,
                ctxdPrice,
                ctxdValue,
                alleyWidth,
                factor,
                structure,
                clcl,
                lat,
                long,
            };

            this.vApi.onUpdateRealEstateClick(data?.id, realEstateData, (data) => {
                console.log(data)
            })
        })

    }


    _deleteProvince(data) {
        $("#btn-confirm-delete-province").on("click", () => {
            this.vApi.onDeleteProvinceClick(data.id)
        })

    }
    _clearInValid(input) {
        if (input) {
            input.removeClass("is-invalid");
            $(".invalid-feedback").remove();
        }
        $(".form-control").removeClass("is-invalid");
        $(".invalid-feedback").remove();
    }

    _clearInput() {
        $("#create-province-form").find("input").val("");
        $("#create-province-form").find("select").val("")
    }
    openModal(type, data) {
        this._logout();
        if (type === "create") {
            this._clearInput();
            this._onEventListner();
            this._createProvince();
        } else {
            if (type === "edit") {
                this._updateProvince(data)
                $("#update-province-modal").modal("show")
            } else {
                this._deleteProvince(data)
                $("#delete-confirm-modal").modal("show")
            }
        }

    }
}

/*** REGION 4 - vùng để gọi lên cơ sở dữ liệu lấy đa ta về*/
class CallApi {
    constructor() {
        this.token = this.getCookie("token");
    }
    setToken(token) {
        this.token = token;
    }

    getHeaders() {
        return {
            Authorization: "Bearer " + this.token
        };
    }
    setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    redirectToLogin() {
        // Trước khi logout cần xóa token đã lưu trong cookie
        this.setCookie("token", "", 1);
        window.location.href = "http://127.0.0.1:5500/real-estates-frontend/html/login.html";
    }

    onShowToast(paramTitle, paramMessage) {
        $('#myToast .mr-auto').text(paramTitle)
        $('#myToast .toast-body').text(paramMessage);
        $('#myToast').toast('show');
    }

    onGetProvincesClick(paramCallbackFn) {
        $.ajax({
            url: gPROVINCE_URL,
            method: 'GET',
            headers: this.getHeaders(),
            success: function (data) {
                paramCallbackFn(data);
            },
            error: (jqXHR, textStatus, errorThrown) => {
                console.log('Error:', errorThrown);
                this.redirectToLogin()
            }
        });
    }

    onGetProvinceByIdClick(provinceId, paramCallbackFn) {
        $.ajax({
            url: gPROVINCE_URL + "/" + provinceId,
            headers: this.getHeaders(),
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onGetCountRealEstate(paramCallbackFn) {
        $.ajax({
            url: `${gREAL_ESTATE_URL}/count`,
            headers: this.getHeaders(),
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onGetCustomsRealEstate(districtId, provinceId, paramCallbackFn) {
        $.ajax({
            url: `${gREAL_ESTATE_URL}/customs?districtId=${districtId}&provinceId=${provinceId}`,
            headers: this.getHeaders(),
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }



    onCreateProvinceClick(provinceData, paramCallbackFn) {
        $.ajax({
            url: gPROVINCE_URL,
            method: 'POST',
            headers: this.getHeaders(),
            data: JSON.stringify(provinceData),
            headers: this.getHeaders(),
            contentType: 'application/json',
            success: (data) => {
                paramCallbackFn(data);
                const render = new RenderPage();
                render.renderPage();
                this.onShowToast("Tạo thành công", "Tạo province thành công!!");
                $("#create-province-modal").modal("hide");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown, textStatus);
            }
        });
    }

    onUpdateProvinceClick(provinceId, provinceData, paramCallbackFn) {
        $.ajax({
            url: gPROVINCE_URL + "/" + provinceId,
            method: 'PUT',
            data: JSON.stringify(provinceData),
            headers: this.getHeaders(),
            contentType: 'application/json',
            success: (data) => {
                const render = new RenderPage();
                render.renderPage();
                paramCallbackFn(data);
                $('#update-province-modal').modal('hide');
                this.onShowToast("Sửa thành công", "Sửa province thành công!!");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onDeleteProvinceClick(provinceId) {
        $.ajax({
            url: gPROVINCE_URL + "/" + provinceId,
            method: 'DELETE',
            headers: this.getHeaders(),
            success: () => {
                this.onShowToast("Xóa thành công", "Bạn đã xóa province thành công!!");
                const render = new RenderPage();
                render.renderPage();
                $("#delete-confirm-modal").modal("hide");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    onGetProvinceList(paramCallbackFn) {
        $.ajax({
          url: gPROVINCE_URL,
          method: 'GET',
          headers: this.getHeaders(),
          success: function (data) {
            paramCallbackFn(data);
          },
          error: function (jqXHR, textStatus, errorThrown) {
            console.log('Error:', errorThrown);
          }
        });
      }
      onGetCustomerList(paramCallbackFn) {
        $.ajax({
          url: gCUSTOMER_URL,
          method: 'GET',
          headers: this.getHeaders(),
          success: function (data) {
            paramCallbackFn(data);
          },
          error: function (jqXHR, textStatus, errorThrown) {
            console.log('Error:', errorThrown);
          }
        });
      }
    

    onExportExcelClick(paramCallbackFn) {
        $.ajax({
            url: `http://localhost:8080/api/export/provinces/excel`,
            method: 'GET',
            headers: this.getHeaders(),
            xhrFields: {
                responseType: "blob"
            },
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    onUpdateRealEstateClick(realEstateId, realEstateData, paramCallbackFn) {
        $.ajax({
          url: gREAL_ESTATE_URL + "/" + realEstateId,
          method: 'PUT',
          headers: this.getHeaders(),
          data: JSON.stringify(realEstateData),
          contentType: 'application/json',
          success: (data) => {
            const render = new RenderPage()
            render.renderPage()
            paramCallbackFn(data);
            $('#update-realEstate-modal').modal('hide');
            this.onShowToast("Sửa thành công", "Sửa Bất động sản thành công!!")
          },
          error: function (jqXHR, textStatus, errorThrown) {
            console.log('Error:', errorThrown);
          }
        });
      }
    onGetCustomerById(id) {
        return new Promise((resolve, reject) => {
            $.ajax({
                url: `${gCUSTOMER_URL}/${id}`,
                method: 'GET',
                headers: this.getHeaders(),
                success: function (data) {
                    resolve(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    reject(errorThrown);
                }
            });
        });
    }
    onGetDistrictListById(id) {
        return new Promise((resolve, reject) => {
            $.ajax({
                url: `${gDISTRICT_URL}/${id}`,
                method: 'GET',
                headers: this.getHeaders(),
                success: function (data) {
                    resolve(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    reject(errorThrown);
                }
            });
        });
    }
    onGetStreetById(id) {
        return new Promise((resolve, reject) => {
            $.ajax({
                url: `${gSTREET_URL}/${id}`,
                method: 'GET',
                headers: this.getHeaders(),
                success: function (data) {
                    resolve(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    reject(errorThrown);
                }
            });
        });
    }
    onGetDistrictListById(id) {
        return new Promise((resolve, reject) => {
            $.ajax({
                url: `${gDISTRICT_URL}/${id}`,
                method: 'GET',
                headers: this.getHeaders(),
                success: function (data) {
                    resolve(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    reject(errorThrown);
                }
            });
        });
    }
    onGetWardById(id) {
        return new Promise((resolve, reject) => {
            $.ajax({
                url: `${gWARD_URL}/${id}`,
                method: 'GET',
                headers: this.getHeaders(),
                success: function (data) {
                    resolve(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    reject(errorThrown);
                }
            });
        });
    }
    onGetProvinceById(id) {
        return new Promise((resolve, reject) => {
            $.ajax({
                url: `${gPROVINCE_URL}/${id}`,
                method: 'GET',
                headers: this.getHeaders(),
                success: function (data) {
                    resolve(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    reject(errorThrown);
                }
            });
        });
    }
    onGetProjectById(id) {
        return new Promise((resolve, reject) => {
            $.ajax({
                url: `${gPROJECT_URL}/${id}`,
                method: 'GET',
                headers: this.getHeaders(),
                success: function (data) {
                    resolve(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    reject(errorThrown);
                }
            });
        });
    }
    onGetProjectList(paramCallbackFn) {
        $.ajax({
          url: gPROJECT_URL,
          method: 'GET',
          headers: this.getHeaders(),
          success: function (data) {
            paramCallbackFn(data);
          },
          error: function (jqXHR, textStatus, errorThrown) {
            console.log('Error:', errorThrown);
          }
        });
      }
    
    onGetProjectById(id) {
        return new Promise((resolve, reject) => {
            $.ajax({
                url: `${gPROJECT_URL}/${id}`,
                method: 'GET',
                headers: this.getHeaders(),
                success: function (data) {
                    resolve(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    reject(errorThrown);
                }
            });
        });
    }
    onGetWardByDistrictId(districtId, paramCallbackFn) {
        $.ajax({
          url: `${gWARD_URL}/${districtId}/district`,
          method: 'GET',
          headers: this.getHeaders(),
          success: function (data) {
            paramCallbackFn(data);
          },
          error: function (jqXHR, textStatus, errorThrown) {
            console.log('Error:', errorThrown);
          }
        });
      }
      onGetStreetByDistrictId(districtId, paramCallbackFn) {
        $.ajax({
          url: `${gSTREET_URL}/${districtId}/district`,
          method: 'GET',
          headers: this.getHeaders(),
          success: function (data) {
            paramCallbackFn(data);
          },
          error: function (jqXHR, textStatus, errorThrown) {
            console.log('Error:', errorThrown);
          }
        });
      }    

    onGetRealEstateByIdClick(realEstateId, paramCallbackFn) {
        $.ajax({
          url: gREAL_ESTATE_URL + "/" + realEstateId,
          method: 'GET',
          headers: this.getHeaders(),
          success: function (data) {
            paramCallbackFn(data);
          },
          error: function (jqXHR, textStatus, errorThrown) {
            console.log('Error:', errorThrown);
          }
        });
      }
      onGetDistrictListByProvinceId(provinceId, paramCallbackFn) {
        $.ajax({
          url: `${gDISTRICT_URL}/${provinceId}/province`,
          method: 'GET',
          headers: this.getHeaders(),
          success: function (data) {
            paramCallbackFn(data);
          },
          error: function (jqXHR, textStatus, errorThrown) {
            console.log('Error:', errorThrown);
          }
        });
      }
    getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
}
