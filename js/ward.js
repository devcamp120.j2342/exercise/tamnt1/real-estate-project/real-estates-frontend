/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gWARD_URL = "http://localhost:8080/api/wards";
const gDISTRICT_URL = "http://localhost:8080/api/districts";
const gPROVINCE_URL = "http://localhost:8080/api/provinces";
const gCOLUMN_ID = {
    stt: 0,
    action: 1,
    name: 2,
    prefix: 3,
    district: 4,
    province: 5,

}
const gCOL_NAME = [
    "stt",
    "action",
    "name",
    "prefix",
    "district",
    "province"


]
//Hàm chính để load html hiển thị ra bảng
class Main {
    constructor() {
        $(document).ready(() => {
            this.vOrderList = new RenderPage()
            this.vOrderList.renderPage()
            this.vModal = new Modal()
            this.vModal.openModal("create")
            $('.select2').select2()
            $('.select2bs4').select2({
                theme: 'bootstrap4',
                dropdownParent: $("#create-ward-modal"),
            })
        })
    }

}
new Main()
/*** REGION 2 - vùng để render ra bảng*/
class RenderPage {
    constructor() {
        this.vApi = new CallApi()
        this.vModal = new Modal()

    }

    //Hàm gọi api lấy danh sách đơn hàng
    _getWardList() {
        this.vApi.onGetWardsClick((paramWard) => {
            this._createWardTable(paramWard)
        })
    }
    //Hiển thị tên sau khi login
    _showAdminName() {
        const name = JSON.parse(localStorage.getItem('login'));

        if (name.accessToken) {
            $(".profile-name").text(name?.username)
        } else {
            this.vApi.redirectToLogin()
            if(!name.roles.includes("ROLE_ADMIN")) {
                $('.user-item').hide();
            }
        }

    }

    //Hàm tạo các thành phần của bảng
    _createWardTable(paramWard) {
        let stt = 1
        if ($.fn.DataTable.isDataTable('#table-ward')) {
            $('#table-ward').DataTable().destroy();
        }

        const vOrderTable = $("#table-ward").DataTable({
            // Khai báo các cột của datatable
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false,
            "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],
            "columns": [
                { "data": null },
                { "data": gCOL_NAME[gCOLUMN_ID.action] },
                { "data": gCOL_NAME[gCOLUMN_ID.name] },
                { "data": gCOL_NAME[gCOLUMN_ID.prefix] },
                { "data": gCOL_NAME[gCOLUMN_ID.district] },
                { "data": gCOL_NAME[gCOLUMN_ID.province] },
            ],
            // Ghi đè nội dung của cột action
            "columnDefs": [
                {
                    targets: gCOLUMN_ID.stt,
                    render: function () {
                        return stt++
                    }
                }, {

                    targets: gCOLUMN_ID.action,
                    defaultContent: `
                              <img class="edit-ward" src="https://cdn0.iconfinder.com/data/icons/glyphpack/45/edit-alt-512.png" style="width: 20px;cursor:pointer;">
                              <img class="delete-ward" src="https://cdn4.iconfinder.com/data/icons/complete-common-version-6-4/1024/trash-512.png" style="width: 20px;cursor:pointer;">
                            `,
                    createdCell: (cell, cellData, rowData, rowIndex, colIndex) => {
                        if (colIndex === gCOLUMN_ID.action) {
                            $(cell).find('.edit-ward').on('click', () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                this.vModal.openModal('edit', vData);
                            });

                            $(cell).find('.delete-ward').on('click', () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                console.log(vData)
                                this.vModal.openModal('delete', vData);
                            });
                        }
                    }
                }],

        });
        vOrderTable.clear() // xóa toàn bộ dữ liệu trong bảng
        vOrderTable.rows.add(paramWard) // cập nhật dữ liệu cho bảng
        vOrderTable.draw()// hàm vẻ lại bảng
        vOrderTable.buttons().container().appendTo('.example1_wrapper .col-md-6:eq(0)')
    }

    _saveExcelFile(data, filename) {
        const blob = new Blob([data], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
        const url = window.URL.createObjectURL(blob);
        const link = document.createElement("a");
        link.href = url;
        link.download = filename;
        link.click();
    }

    _exportToExcel() {
        $("#btn-export-excel").on("click", () => {
            this.vApi.onExportExcelClick((data) => {
                console.log(data)
                this._saveExcelFile(data, "ward.xlsx");
            });
        });
    }


    // Hàm sẽ được gọi ở class Main 
    renderPage() {
        this._showAdminName()
        this._exportToExcel()
        this._getWardList()
    }
}

/*** REGION 3 - vùng hiện modal*/

class Modal {
    constructor() {
        this.vApi = new CallApi()
    }

    _onEventListner() {
        $("#btn-add-ward").on("click", () => {
            $("#create-ward-modal").modal("show")
        })
        $(".logout").on("click", () => {
            $("#logout-confirm-modal").modal("show")
        })

    }

    _logout() {
        $("#btn-confirm-logout").on("click", () => {
            this.vApi.redirectToLogin()
        })

    }
    _getDistrictList(provinceId, type) {
        let districtDropdown
        if (type === "update") {
            districtDropdown = $("#input-update-districtId");
        } else {
            districtDropdown = $("#input-create-districtId");
        }
        this.vApi.onGetDistrictListByProvinceId(provinceId, (districts) => {
            districtDropdown.empty();
            districtDropdown.append('<option value="" selected>Huyện</option>');

            districts.forEach((district) => {
                const option = `<option value="${district.id}">${district.name}</option>`;
                districtDropdown.append(option);
            });
        })

    }
    _getProvinceList(type, data) {
        let provinceDropdown
        if (type === "update") {
            provinceDropdown = $("#input-update-provinceId");
        } else {
            provinceDropdown = $("#input-create-provinceId");
        }
        this.vApi.onGetProvinceList((provinces) => {
            provinceDropdown.empty();
            provinceDropdown.append(`<option value="" selected>${data?.province}</option>`);

            provinces.forEach((province) => {
                const option = `<option value="${province.id}">${province.name}</option>`;
                provinceDropdown.append(option);
            });

            // Call the callback function
            if (typeof callback === 'function') {
                callback();
            }
        });
    }

    _createWard() {
        this._getProvinceList("create")
        this._clearInput();
        $("#input-create-provinceId").on("change", () => {
            const provinceId = $("#input-create-provinceId").val();
            console.log(provinceId)
            this._getDistrictList(provinceId);
        });

        $("#btn-create-ward").on("click", () => {
            this._clearInValid();

            let isValid = true;
            const vFields = [
                "input-create-name",

            ];

            vFields.forEach((field) => {
                const value = $(`#${field}`).val().trim();
                if (!value) {
                    isValid = false;
                    $(`#${field}`).addClass("is-invalid");
                    $(`#${field}`).after(`<div class="invalid-feedback error-message">Vui lòng nhập và giá trị hợp lệ!.</div>`);
                }
            });

            if (isValid) {
                const wardData = {
                    name: $("#input-create-name").val().trim(),
                    prefix: $("#input-create-prefix").val().trim(),
                    provinceId: parseInt($("#input-create-provinceId").val().trim()),
                    districtId: parseInt($("#input-create-districtId").val().trim()),
                };
                this.vApi.onCreateWardClick(wardData, (data) => {
                    console.log(data);
                });
            }
        }).bind(this);
    }

    _updateWard(ward) {
        this._getProvinceList("update", ward)
        $("#input-update-provinceId").on("change", () => {
            const provinceId = $("#input-update-provinceId").val();
            this._getDistrictList(provinceId);
        });
        $("#input-update-name").val(ward.name);
        $("#input-update-prefix").val(ward.prefix);
        $('#input-update-provinceId').find('option').text(ward.province || '');
        $('#input-update-districtId').find('option').text(ward.district || '');
        $("#btn-update-ward").off("click").on("click", () => {
            this._clearInValid();
            let isValid = true;
            const requiredFields = ["input-update-name"];

            requiredFields.forEach((field) => {
                if (!$(`#${field}`).val().trim()) {
                    isValid = false;
                    $(`#${field}`).addClass("is-invalid");
                    $(`#${field}`).after('<div class="invalid-feedback">Vui lòng nhập giá trị hợp lệ!</div>');
                }
            });

            if (isValid) {
                const updatedward = {
                    name: $("#input-update-name").val().trim(),
                    prefix: $("#input-update-prefix").val().trim(),
                    provinceId: parseInt($("#input-update-provinceId").val().trim()),
                    districtId: parseInt($("#input-update-districtId").val().trim()),
                };

                this.vApi.onUpdateWardClick(parseInt(ward.id), updatedward, (data) => {
                    console.log(data);
                });
            }
        }).bind(this);
    }


    _deleteWard(data) {
        $("#btn-confirm-delete-ward").on("click", () => {
            this.vApi.onDeleteWardClick(parseInt(data.id))
        })

    }
    _clearInValid(input) {
        if (input) {
            input.removeClass("is-invalid");
            $(".invalid-feedback").remove();
        }
        $(".form-control").removeClass("is-invalid");
        $(".invalid-feedback").remove();
    }

    _clearInput() {
        $("#create-ward-form").find("input").val("");
        $("#create-ward-form").find("select").val("")
    }
    openModal(type, data) {
        this._logout();
        if (type === "create") {
            this._clearInput()
            this._onEventListner()
            this._createWard()
        } else {
            if (type === "edit") {

                $("#update-ward-modal").modal("show")
                this._updateWard(data)
            } else {
                this._deleteWard(data)
                $("#delete-confirm-modal").modal("show")
            }
        }

    }
}

/*** REGION 4 - vùng để gọi lên cơ sở dữ liệu lấy đa ta về*/
class CallApi {
    constructor() {
        this.token = this.getCookie("token");
    }

    setToken(token) {
        this.token = token;
    }

    getHeaders() {
        return {
            Authorization: "Bearer " + this.token
        };
    }
    setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    redirectToLogin() {
        // Trước khi logout cần xóa token đã lưu trong cookie
        this.setCookie("token", "", 1);
        window.location.href = "http://127.0.0.1:5500/real-estates-frontend/html/login.html";
    }

    onShowToast(paramTitle, paramMessage) {
        $('#myToast .mr-auto').text(paramTitle)
        $('#myToast .toast-body').text(paramMessage);
        $('#myToast').toast('show');
    }

    onGetWardsClick(paramCallbackFn) {
        $.ajax({
            url: gWARD_URL + "/district",
            method: 'GET',
            headers: this.getHeaders(),
            success: function (data) {
                paramCallbackFn(data);
            },
            error: (jqXHR, textStatus, errorThrown) => {
                console.log('Error:', errorThrown);
                this.redirectToLogin();
            }
        });
    }

    onGetWardByIdClick(wardId, paramCallbackFn) {
        $.ajax({
            url: gWARD_URL + "/" + wardId,
            method: 'GET',
            headers: this.getHeaders(),
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onCreateWardClick(wardData, paramCallbackFn) {
        $.ajax({
            url: gWARD_URL,
            method: 'POST',
            data: JSON.stringify(wardData),
            headers: this.getHeaders(),
            contentType: 'application/json',
            success: (data) => {
                paramCallbackFn(data);
                const render = new RenderPage()
                render.renderPage()
                this.onShowToast("Tạo thành công", "Tạo ward thành công!!")
                $("#create-ward-modal").modal("hide");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown, textStatus);
            }
        });
    }

    onUpdateWardClick(wardId, wardData, paramCallbackFn) {
        $.ajax({
            url: gWARD_URL + "/" + wardId,
            method: 'PUT',
            data: JSON.stringify(wardData),
            headers: this.getHeaders(),
            contentType: 'application/json',
            success: (data) => {
                const render = new RenderPage()
                render.renderPage()
                paramCallbackFn(data);
                $('#update-ward-modal').modal('hide');
                this.onShowToast("Sửa thành công", "Sửa ward thành công!!")
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onDeleteWardClick(wardId) {
        $.ajax({
            url: gWARD_URL + "/" + wardId,
            method: 'DELETE',
            headers: this.getHeaders(),
            success: () => {
                this.onShowToast("Xóa thành công", "bạn đã xóa ward thành công!!")
                const render = new RenderPage()
                render.renderPage()
                $("#delete-confirm-modal").modal("hide");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onExportExcelClick(paramCallbackFn) {
        $.ajax({
            url: `http://localhost:8080/api/export/wards/excel`,
            method: 'GET',
            xhrFields: {
                responseType: "blob"
            },
            headers: this.getHeaders(),
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onGetDistrictListByProvinceId(provinceId, paramCallbackFn) {
        $.ajax({
            url: `${gDISTRICT_URL}/${provinceId}/province`,
            method: 'GET',
            headers: this.getHeaders(),
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    onGetProvinceById(id) {
        return new Promise((resolve, reject) => {
            $.ajax({
                url: `${gPROVINCE_URL}/${id}`,
                method: 'GET',
                headers: this.getHeaders(),
                success: function (data) {
                    resolve(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    reject(errorThrown);
                }
            });
        });
    }
    onGetDistrictListById(id) {
        return new Promise((resolve, reject) => {
            $.ajax({
                url: `${gDISTRICT_URL}/${id}`,
                method: 'GET',
                headers: this.getHeaders(),
                success: function (data) {
                    resolve(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    reject(errorThrown);
                }
            });
        });
    }

    onGetProvinceList(paramCallbackFn) {
        $.ajax({
            url: gPROVINCE_URL,
            method: 'GET',
            headers: this.getHeaders(),
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
}
