/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gcontractor_URL = "http://localhost:8080/api/construction-contractors";
const gCOLUMN_ID = {
    stt: 0,
    action: 1,
    name: 2,
    projects: 3,
    address: 4,
    phone: 5,
    fax: 6,
    email: 7,
    website: 8,
    note: 9,
}
const gCOL_NAME = [
    "stt",
    "action",
    "name",
    "projects",
    "address",
    "phone",
    "fax",
    "email",
    "website",
    "note"

]
//Hàm chính để load html hiển thị ra bảng
class Main {
    constructor() {
        $(document).ready(() => {
            this.vOrderList = new RenderPage()
            this.vOrderList.renderPage()
            this.vModal = new Modal()
            this.vModal.openModal("create")
            $('.select2').select2()
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })
        })
    }



}
new Main()
/*** REGION 2 - vùng để render ra bảng*/
class RenderPage {
    constructor() {
        this.vApi = new CallApi()
        this.vModal = new Modal()

    }

    //Hàm gọi api lấy danh sách đơn hàng
    _getContractorList() {
        this.vApi.onGetContractorsClick((paramContractor) => {
            this._createContractorTable(paramContractor)
        })
    }
    _showAdminName() {
        const name = JSON.parse(localStorage.getItem('login'));
        if (name.accessToken) {
            $(".profile-name").text(name?.username)
            if (!name.roles.includes("ROLE_ADMIN")) {
                $('.user-item').hide();
            }
        } else {
            this.vApi.redirectToLogin()
        }

    }

    //Hàm tạo các thành phần của bảng
    _createContractorTable(paramContractor) {
        let stt = 1;
        if ($.fn.DataTable.isDataTable('#table-contractor')) {
            $('#table-contractor').DataTable().destroy();
        }
        const vOrderTable = $("#table-contractor").DataTable({
            "dom": "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false,
            "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],
            // Khai báo các cột của datatable
            "columns": [
                { "data": null },
                { "data": gCOL_NAME[gCOLUMN_ID.action] },
                { "data": gCOL_NAME[gCOLUMN_ID.name] },
                { "data": gCOL_NAME[gCOLUMN_ID.projects] },
                { "data": gCOL_NAME[gCOLUMN_ID.address] },
                { "data": gCOL_NAME[gCOLUMN_ID.phone] },
                { "data": gCOL_NAME[gCOLUMN_ID.fax] },
                { "data": gCOL_NAME[gCOLUMN_ID.email] },
                { "data": gCOL_NAME[gCOLUMN_ID.website] },
                { "data": gCOL_NAME[gCOLUMN_ID.note] },
            ],
            // Ghi đè nội dung của cột action
            "columnDefs": [
                {
                    targets: gCOLUMN_ID.stt,
                    render: function () {
                        return stt++;
                    }
                },
                {
                    targets: gCOLUMN_ID.action,
                    defaultContent: `
                        <img class="edit-contractor" src="https://cdn0.iconfinder.com/data/icons/glyphpack/45/edit-alt-512.png" style="width: 20px;cursor:pointer;">
                        <img class="delete-contractor" src="https://cdn4.iconfinder.com/data/icons/complete-common-version-6-4/1024/trash-512.png" style="width: 20px;cursor:pointer;">
                    `,
                    createdCell: (cell, cellData, rowData, rowIndex, colIndex) => {
                        if (colIndex === gCOLUMN_ID.action) {
                            $(cell).find('.edit-contractor').on('click', () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                this.vModal.openModal('edit', vData);
                            });

                            $(cell).find('.delete-contractor').on('click', () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                console.log(vData);
                                this.vModal.openModal('delete', vData);
                            });
                        }
                    }
                }
            ]
        });
        vOrderTable.clear(); // xóa toàn bộ dữ liệu trong bảng
        vOrderTable.rows.add(paramContractor); // cập nhật dữ liệu cho bảng
        vOrderTable.draw(); // hàm vẽ lại bảng

        vOrderTable.buttons().container().appendTo('.example1_wrapper .col-md-6:eq(0)'); // Add buttons to the desired container
    }

    _saveExcelFile(data, filename) {
        const blob = new Blob([data], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
        const url = window.URL.createObjectURL(blob);
        const link = document.createElement("a");
        link.href = url;
        link.download = filename;
        link.click();
    }

    _exportToExcel() {
        $("#btn-export-excel").on("click", () => {
            this.vApi.onExportExcelClick((data) => {
                console.log(data)
                this._saveExcelFile(data, "users.xlsx");
            });
        });
    }
    _createBarChart() {

    }


    // Hàm sẽ được gọi ở class Main 
    renderPage() {
        this._exportToExcel();
        this._showAdminName();
        this._getContractorList();
        this._createBarChart();
    }
}

/*** REGION 3 - vùng hiện modal*/

class Modal {
    constructor() {
        this.vApi = new CallApi()
    }

    _onEventListner() {
        $("#btn-add-contractor").on("click", () => {
            $("#create-contractor-modal").modal("show")
        })
        $(".logout").on("click", () => {
            console.log('yyess')
            $("#logout-confirm-modal").modal("show")
        })

    }

    _logout() {
        $("#btn-confirm-logout").on("click", () => {
            this.vApi.redirectToLogin()
        })

    }
    _createContractor() {
        $("#btn-create-contractor").on("click", () => {
            this._clearInValid()

            let isValid = true;
            const vFields = [
                "input-create-name",
                "input-create-address",
                "input-create-phone",
            ];

            vFields.forEach((field) => {
                const value = $(`#${field}`).val().trim();
                if (!value) {
                    isValid = false;
                    $(`#${field}`).addClass("is-invalid");
                    $(`#${field}`).after(`<div class="invalid-feedback error-message">Vui lòng nhập và giá trị hợp lệ!.</div>`);
                }
            });

            if (isValid) {
                const contractorData = {
                    name: $("#input-create-name").val().trim(),
                    description: $("#input-create-desc").val().trim(),
                    project: $("#input-create-project").val().trim(),
                    address: $("#input-create-address").val().trim(),
                    phone: $("#input-create-phone").val().trim(),
                    phone2: $("#input-create-phone2").val().trim(),
                    fax: $("#input-create-fax").val().trim(),
                    email: $("#input-create-email").val().trim(),
                    website: $("#input-create-website").val().trim(),
                    note: $("#input-create-note").val().trim(),
                };

                this.vApi.onCreateContractorClick(contractorData, (data) => {
                    console.log(data);
                });
            }
        }).bind(this);
    }


    _updateContractor(customer) {
        // Show values in the input fields
        $('#input-update-name').val(customer.name);
        $('#input-update-desc').val(customer.description);
        $('#input-update-project').val(customer.project);
        $('#input-update-address').val(customer.address);
        $('#input-update-phone').val(customer.phone);
        $('#input-update-phone2').val(customer.phone2);
        $('#input-update-fax').val(customer.fax);
        $('#input-update-email').val(customer.email);
        $('#input-update-website').val(customer.website);
        $('#input-update-note').val(customer.note);

        $("#btn-update-contractor").on("click", () => {
            this._clearInValid();
            let isValid = true;
            const vFields = [
                "input-update-name",
                "input-update-address",
                "input-update-phone"
            ];

            vFields.forEach((field) => {
                const value = $(`#${field}`).val().trim();
                if (!value) {
                    isValid = false;
                    $(`#${field}`).addClass("is-invalid");
                    $(`#${field}`).after(`<div class="invalid-feedback error-message">Vui lòng nhập và giá trị hợp lệ!.</div>`);
                }
            });

            if (isValid) {
                const contractorData = {
                    name: $("#input-update-name").val().trim(),
                    description: $("#input-update-desc").val().trim(),
                    project: $("#input-update-project").val().trim(),
                    address: $("#input-update-address").val().trim(),
                    phone: $("#input-update-phone").val().trim(),
                    phone2: $("#input-update-phone2").val().trim(),
                    fax: $("#input-update-fax").val().trim(),
                    email: $("#input-update-email").val().trim(),
                    website: $("#input-update-website").val().trim(),
                    note: $("#input-update-note").val().trim(),
                };

                this.vApi.onUpdateContractorClick(customer.id, contractorData, (data) => {
                    console.log(data);
                });
            }
        }).bind(this);
    }



    _deleteContractor(data) {
        $("#btn-confirm-delete-contractor").on("click", () => {
            this.vApi.onDeleteContractorClick(data.id)
        })

    }
    _clearInValid(input) {
        if (input) {
            input.removeClass("is-invalid");
            $(".invalid-feedback").remove();
        }
        $(".form-control").removeClass("is-invalid");
        $(".invalid-feedback").remove();
    }


    _clearInput() {
        $("#create-contractor-form").find("input").val("");
        $("#create-contractor-form").find("select").val("")
    }
    openModal(type, data) {
        this._logout()
        if (type === "create") {
            this._clearInput()
            this._onEventListner()
            this._createContractor()
        } else {
            if (type === "edit") {
                this._updateContractor(data)
                $("#update-contractor-modal").modal("show")
            } else {
                this._deleteContractor(data)
                $("#delete-confirm-modal").modal("show")
            }
        }

    }
}

/*** REGION 4 - vùng để gọi lên cơ sở dữ liệu lấy đa ta về*/
class CallApi {
    constructor() {
        this.token = this.getCookie("token");
    }
    setToken(token) {
        this.token = token;
    }

    getHeaders() {
        return {
            Authorization: "Bearer " + this.token
        };
    }
    setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    redirectToLogin() {
        // Trước khi logout cần xóa token đã lưu trong cookie
        this.setCookie("token", "", 1);
        window.location.href = "http://127.0.0.1:5500/real-estates-frontend/html/login.html";
    }
    onShowToast(paramTitle, paramMessage) {
        $('#myToast .mr-auto').text(paramTitle)
        $('#myToast .toast-body').text(paramMessage);
        $('#myToast').toast('show');
    }

    onGetContractorsClick(paramCallbackFn) {
        $.ajax({
            url: gcontractor_URL,
            method: 'GET',
            headers: this.getHeaders(),
            success: function (data) {
                paramCallbackFn(data);
            },
            error: (jqXHR, textStatus, errorThrown) => {
                console.log('Error:', errorThrown);

            }
        });
    }

    onExportExcelClick(paramCallbackFn) {
        $.ajax({
            url: `http://localhost:8080/api/export/contractors/excel`,
            method: 'GET',
            xhrFields: {
                responseType: "blob"
            },
            success: function (data) {
                paramCallbackFn(data);
            },
            headers: this.getHeaders(),
            error: (jqXHR, textStatus, errorThrown) => {
                console.log('Error:', errorThrown);
                this.redirectToLogin();
            }
        });
    }
    onGetcontractorByIdClick(contractorId, paramCallbackFn) {
        $.ajax({
            url: gcontractor_URL + "/" + contractorId,
            headers: this.getHeaders(),
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: (jqXHR, textStatus, errorThrown) => {
                console.log('Error:', errorThrown);
                this.redirectToLogin();
            }
        });
    }

    onCreateContractorClick(contractorData, paramCallbackFn) {
        $.ajax({
            url: gcontractor_URL,
            method: 'POST',
            data: JSON.stringify(contractorData),
            headers: this.getHeaders(),
            contentType: 'application/json',
            success: (data) => {
                paramCallbackFn(data);
                const render = new RenderPage()
                render.renderPage()
                this.onShowToast("Tạo thành công", "Tạo contractor thành công!!")
                $("#create-contractor-modal").modal("hide");
            },
            error: (jqXHR, textStatus, errorThrown) => {
                console.log('Error:', errorThrown, textStatus);
                this.redirectToLogin();
            }
        });
    }
    onUpdateContractorClick(contractorId, contractorData, paramCallbackFn) {
        $.ajax({
            url: gcontractor_URL + "/" + contractorId,
            method: 'PUT',
            headers: this.getHeaders(),
            data: JSON.stringify(contractorData),
            contentType: 'application/json',
            success: (data) => {
                const render = new RenderPage()
                render.renderPage()
                paramCallbackFn(data);
                $('#update-contractor-modal').modal('hide');
                this.onShowToast("Sửa thành công", "Sửa contractor thành công!!")
            },
            error: (jqXHR, textStatus, errorThrown) => {
                console.log('Error:', errorThrown);
                this.redirectToLogin();
            }
        });
    }
    onDeleteContractorClick(contractorId) {
        $.ajax({
            url: gcontractor_URL + "/" + contractorId,
            method: 'DELETE',
            success: () => {
                this.onShowToast("Xóa thành công", "bạn đã xóa contractor thành công!!")
                const render = new RenderPage()
                render.renderPage()
                $("#delete-confirm-modal").modal("hide");
            },
            error: (jqXHR, textStatus, errorThrown) => {
                console.log('Error:', errorThrown);
                this.redirectToLogin();
            }
        });
    }
    getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

}