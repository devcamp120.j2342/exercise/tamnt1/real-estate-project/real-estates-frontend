/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gSUBCRIPTION_URL = "http://localhost:8080/api/subscriptions";
const gEMPLOYEE_URL = "http://localhost:8080/api/employees";
const gCOLUMN_ID = {
    stt: 0,
    action: 1,
    user: 2,
    endpoint: 3,
    publicKey: 4,
    authenticationtoken: 5,
    contentencoding: 6,

}
const gCOL_NAME = [
    "stt",
    "action",
    "user",
    "endpoint",
    "publicKey",
    "authenticationToken",
    "contentEncoding"


]
//Hàm chính để load html hiển thị ra bảng
class Main {
    constructor() {
        $(document).ready(() => {
            this.vOrderList = new RenderPage()
            this.vOrderList.renderPage()
            this.vModal = new Modal()
            this.vModal.openModal("create")
            $('.select2').select2()
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })
        })
    }

}
new Main()
/*** REGION 2 - vùng để render ra bảng*/
class RenderPage {
    constructor() {
        this.vApi = new CallApi()
        this.vModal = new Modal()

    }

    //Hàm gọi api lấy danh sách đơn hàng
    _getSubscriptionList() {
        this.vApi.onGetSubscriptionsClick((paramsubscription) => {
            this._createSubscriptionTable(paramsubscription)
        })
    }
    //Hiển thị tên sau khi login
    _showAdminName() {
        const name = JSON.parse(localStorage.getItem('login'));
        if(name.accessToken) {
          $(".profile-name").text(name?.username)
          if(!name.roles.includes("ROLE_ADMIN")) {
            $('.user-item').hide();
        }
        }else {
          this.vApi.redirectToLogin()
        }
    
      }



    //Hàm tạo các thành phần của bảng
    _createSubscriptionTable(paramsubscription) {
        console.log(paramsubscription)
        let stt = 1
        if ($.fn.DataTable.isDataTable('#table-subscription')) {
            $('#table-subscription').DataTable().destroy();
        }
        const vOrderTable = $("#table-subscription").DataTable({
            // Khai báo các cột của datatable
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false,
            "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],
            "columns": [
                { "data": null },
                { "data": gCOL_NAME[gCOLUMN_ID.action] },
                { "data": gCOL_NAME[gCOLUMN_ID.user] },
                { "data": gCOL_NAME[gCOLUMN_ID.endpoint] },
                { "data": gCOL_NAME[gCOLUMN_ID.publicKey] },
                { "data": gCOL_NAME[gCOLUMN_ID.authenticationtoken] },
                { "data": gCOL_NAME[gCOLUMN_ID.contentencoding] },

            ],
            // Ghi đè nội dung của cột action
            "columnDefs": [
                {
                    targets: gCOLUMN_ID.stt,
                    render: function () {
                        return stt++
                    }
                }, {

                    targets: gCOLUMN_ID.action,
                    defaultContent: `
                              <img class="edit-subscription" src="https://cdn0.iconfinder.com/data/icons/glyphpack/45/edit-alt-512.png" style="width: 20px;cursor:pointer;">
                              <img class="delete-subscription" src="https://cdn4.iconfinder.com/data/icons/complete-common-version-6-4/1024/trash-512.png" style="width: 20px;cursor:pointer;">
                            `,
                    createdCell: (cell, cellData, rowData, rowIndex, colIndex) => {
                        if (colIndex === gCOLUMN_ID.action) {
                            $(cell).find('.edit-subscription').on('click', () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                this.vModal.openModal('edit', vData);
                            });

                            $(cell).find('.delete-subscription').on('click', () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                console.log(vData)
                                this.vModal.openModal('delete', vData);
                            });
                        }
                    }
                }],

        });
        vOrderTable.clear() // xóa toàn bộ dữ liệu trong bảng
        vOrderTable.rows.add(paramsubscription) // cập nhật dữ liệu cho bảng
        vOrderTable.draw()// hàm vẻ lại bảng
        vOrderTable.buttons().container().appendTo('.example1_wrapper .col-md-6:eq(0)')
        $('.table-container').css('overflow-x', 'auto');
    }

    _saveExcelFile(data, filename) {
        const blob = new Blob([data], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
        const url = window.URL.createObjectURL(blob);
        const link = document.createElement("a");
        link.href = url;
        link.download = filename;
        link.click();
    }

    _exportToExcel() {
        $("#btn-export-excel").on("click", () => {
            this.vApi.onExportExcelClick((data) => {
                console.log(data)
                this._saveExcelFile(data, "subscription.xlsx");
            });
        });
    }


    // Hàm sẽ được gọi ở class Main 
    renderPage() {
        this._showAdminName()
        this._exportToExcel()
        this._getSubscriptionList()
    }
}

/*** REGION 3 - vùng hiện modal*/

class Modal {
    constructor() {
        this.vApi = new CallApi()
    }

    _onEventListner() {
        $("#btn-add-subscription").on("click", () => {
            $("#create-subscription-modal").modal("show")
        })
        $(".logout").on("click", () => {
            $("#logout-confirm-modal").modal("show")
        })

    }

    _logout() {
        $("#btn-confirm-logout").on("click", () => {
            this.vApi.redirectToLogin()
        })

    }
    async _getUserList(type) {
        let userDropdown;
        if (type === "update") {
            userDropdown = $("#input-update-user");
        } else {
            userDropdown = $("#input-create-user");
        }

        try {
            const users = await this.vApi.onGetUserList();
            userDropdown.empty();
            userDropdown.append('<option value="" selected>Chọn</option>');

            users.forEach((user) => {
                const option = `<option value="${user.employeeId}">${user.username}</option>`;
                userDropdown.append(option);
            });
        } catch (error) {
            console.log('Error:', error);
        }
    }


    async _createSubscription() {
        await this._getUserList();
        this._clearInput();
        $("#btn-create-subscription").on("click", () => {
            this._clearInValid();

            let isValid = true;
            const vFields = [
                "input-update-user",
                "input-create-endpoint",
                "input-create-key",
            ];

            vFields.forEach((field) => {
                const value = $(`#${field}`).val().trim();
                if (!value) {
                    isValid = false;
                    $(`#${field}`).addClass("is-invalid");
                    $(`#${field}`).after(`<div class="invalid-feedback error-message">Vui lòng nhập và giá trị hợp lệ!.</div>`);
                }
            });

            if (isValid) {
                const subscriptionData = {
                    user: $("#input-create-user").val().trim(),
                    endpoint: $("#input-create-endpoint").val().trim(),
                    publicKey: $("#input-create-key").val().trim(),
                    authenticationToken: $("#input-create-auth").val().trim(),
                    contentEncoding: $("#input-create-endcoding").val().trim()
                };

                // Call your API to create a subscription with the subscriptionData
                this.vApi.onCreateSubscriptionClick(subscriptionData, (data) => {
                    console.log(data);
                });
            }
        });
    }

    async _updateSubscription(data) {
        await this._getUserList("update");
        $("#input-update-user").val(parseInt(data.user));

        $("#input-update-endpoint").val(data.endpoint);
        $("#input-update-key").val(data.publicKey);
        $("#input-update-auth").val(data.authenticationToken);
        $("#input-update-endcoding").val(data.contentEncoding);
        $("#btn-update-subscription").on("click", () => {
            this._clearInValid();

            let isValid = true;
            const vFields = [
                "input-update-user",
                "input-update-endpoint",
                "input-update-key",
            ];

            vFields.forEach((field) => {
                const value = $(`#${field}`).val().trim();
                if (!value) {
                    isValid = false;
                    $(`#${field}`).addClass("is-invalid");
                    $(`#${field}`).after(`<div class="invalid-feedback error-message">Vui lòng nhập và giá trị hợp lệ!.</div>`);
                }
            });

            if (isValid) {
                const subscriptionData = {
                    user: $("#input-update-user").val().trim(),
                    endpoint: $("#input-update-endpoint").val().trim(),
                    publicKey: $("#input-update-key").val().trim(),
                    authenticationToken: $("#input-update-auth").val().trim(),
                    contentEncoding: $("#input-update-endcoding").val().trim()
                };

                this.vApi.onUpdateSubscriptionClick(data.id, subscriptionData, (data) => {
                    console.log("Updated Subscription Data:", data);
                });
            }
        })
    };


    _deleteSubscription(data) {
        $("#btn-confirm-delete-subscription").on("click", () => {
            this.vApi.onDeleteSubscriptionClick(data.id)
        })

    }
    _clearInValid(input) {
        if (input) {
            input.removeClass("is-invalid");
            $(".invalid-feedback").remove();
        }
        $(".form-control").removeClass("is-invalid");
        $(".invalid-feedback").remove();
    }

    _clearInput() {
        $("#create-subscription-form").find("input").val("");
        $("#create-subscription-form").find("select").val("")
    }
    openModal(type, data) {
        this._logout();
        if (type === "create") {
            this._clearInput();
            this._onEventListner();
            this._createSubscription();
        } else {
            if (type === "edit") {
                this._updateSubscription(data)
                $("#update-subscription-modal").modal("show")
            } else {
                this._deleteSubscription(data)
                $("#delete-confirm-modal").modal("show")
            }
        }

    }
}

/*** REGION 4 - vùng để gọi lên cơ sở dữ liệu lấy đa ta về*/
class CallApi {
    constructor() {
        this.token = this.getCookie("token");
    }
    setToken(token) {
        this.token = token;
    }

    getHeaders() {
        return {
            Authorization: "Bearer " + this.token
        };
    }
    setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    redirectToLogin() {
        // Trước khi logout cần xóa token đã lưu trong cookie
        this.setCookie("token", "", 1);
        window.location.href = "http://127.0.0.1:5500/real-estates-frontend/html/login.html";
    }

    onShowToast(paramTitle, paramMessage) {
        $('#myToast .mr-auto').text(paramTitle)
        $('#myToast .toast-body').text(paramMessage);
        $('#myToast').toast('show');
    }

    onGetSubscriptionsClick(paramCallbackFn) {
        $.ajax({
            url: gSUBCRIPTION_URL,
            method: 'GET',
            headers: this.getHeaders(),
            success: function (data) {
                paramCallbackFn(data);
            },
            error: (jqXHR, textStatus, errorThrown) => {
                console.log('Error:', errorThrown);
                this.redirectToLogin()
            }
        });
    }

    onGetSubscriptionByIdClick(subscriptionId, paramCallbackFn) {
        $.ajax({
            url: gSUBCRIPTION_URL + "/" + subscriptionId,
            headers: this.getHeaders(),
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onCreateSubscriptionClick(subscriptionData, paramCallbackFn) {
        $.ajax({
            url: gSUBCRIPTION_URL,
            method: 'POST',
            headers: this.getHeaders(),
            data: JSON.stringify(subscriptionData),
            headers: this.getHeaders(),
            contentType: 'application/json',
            success: (data) => {
                paramCallbackFn(data);
                const render = new RenderPage();
                render.renderPage();
                this.onShowToast("Tạo thành công", "Tạo subscription thành công!!");
                $("#create-subscription-modal").modal("hide");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown, textStatus);
            }
        });
    }

    onUpdateSubscriptionClick(subscriptionId, subscriptionData, paramCallbackFn) {
        $.ajax({
            url: gSUBCRIPTION_URL + "/" + subscriptionId,
            method: 'PUT',
            data: JSON.stringify(subscriptionData),
            headers: this.getHeaders(),
            contentType: 'application/json',
            success: (data) => {
                const render = new RenderPage();
                render.renderPage();
                paramCallbackFn(data);
                $('#update-subscription-modal').modal('hide');
                this.onShowToast("Sửa thành công", "Sửa subscription thành công!!");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onDeleteSubscriptionClick(subscriptionId) {
        $.ajax({
            url: gSUBCRIPTION_URL + "/" + subscriptionId,
            method: 'DELETE',
            headers: this.getHeaders(),
            success: () => {
                this.onShowToast("Xóa thành công", "Bạn đã xóa subscription thành công!!");
                const render = new RenderPage();
                render.renderPage();
                $("#delete-confirm-modal").modal("hide");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    onGetUserList() {
        return new Promise((resolve, reject) => {
            $.ajax({
                url: gEMPLOYEE_URL,
                method: 'GET',
                headers: this.getHeaders(),
                success: function (data) {
                    resolve(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    reject(errorThrown);
                }
            });
        });
    }

    onExportExcelClick(paramCallbackFn) {
        $.ajax({
            url: `http://localhost:8080/api/export/subscriptions/excel`,
            method: 'GET',
            headers: this.getHeaders(),
            xhrFields: {
                responseType: "blob"
            },
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }


    getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
}
