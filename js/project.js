/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gPROJECT_URL = "http://localhost:8080/api/projects";
const gPROVINCE_URL = "http://localhost:8080/api/provinces";
const gDISTRICT_URL = "http://localhost:8080/api/districts";
const gWARD_URL = "http://localhost:8080/api/wards";
const gINVESTOR_URL = "http://localhost:8080/api/investors";
const gCONTRACTOR_URL = "http://localhost:8080/api/construction-contractors";
const gDESIGN_UNIT_URL = "http://localhost:8080/api/design-units";
const gUPLOAD_FILE_URL = "http://localhost:8080/api/upload";
const gCOLUMN_ID = {
  stt: 0,
  action: 1,
  name: 2,
  provinceId: 3,
  districtId: 4,
  wardId: 5,
  address: 6,
  description: 7,
  acreage: 8,
  utilities: 9,
  regionLink: 10,
  latitude: 11,
  longitude: 12,
};
const gCOL_NAME = [
  "stt",
  "action",
  "name",
  "provinceId",
  "districtId",
  "wardsId",
  "address",
  "description",
  "acreage",
  "utilities",
  "regionLink",
  "latitude",
  "longitude"
];
//Hàm chính để load html hiển thị ra bảng
class Main {
  constructor() {
    $(document).ready(() => {
      this.vOrderList = new RenderPage()
      this.vOrderList.renderPage()
      this.vModal = new Modal()
      this.vModal.openModal("create")
      $('.select2').select2()
      $('.select2bs4').select2({
        theme: 'bootstrap4'
      })
    })
  }

}
new Main()
/*** REGION 2 - vùng để render ra bảng*/
class RenderPage {
  constructor() {
    this.vApi = new CallApi()
    this.vModal = new Modal()
    this.token = this.vApi.getCookie("token")

  }

  //Hàm gọi api lấy danh sách
  _getProjectList() {
    this._createProjectTable()
  }
  //Hiển thị tên sau khi login
  _showAdminName() {
    const name = JSON.parse(localStorage.getItem('login'));
    if (name.accessToken) {
      $(".profile-name").text(name?.username)
      if (!name.roles.includes("ROLE_ADMIN")) {
        $('.user-item').hide();
      }
    } else {
      this.vApi.redirectToLogin()
    }

  }


  //Hàm tạo các thành phần của bảng
  async _createProjectTable() {

    $.fn.dataTable.ext.errMode = 'none';
    const gNAME = ["action", "id", "name", "provinceId", "districtId", "latitude", "longtitude", "acreage", "constructArea", "numBlock", "numApartment", "investor", "constructionContractor", "designUnit"];
    var colname;
    var gTable = $("#table-data").DataTable({
      searching: false,
      processing: true, // shows loading image while fetching data
      serverSide: true, // activates server side pagination
      ajax: {
        headers: {
          Authorization: "Bearer " + this.token,
        },
        url: gPROJECT_URL + "/dataTable", // API
      },
      columns: [{ data: gNAME[0] }, { data: gNAME[1] }, { data: gNAME[2] }, { data: gNAME[3] }, { data: gNAME[4] }, { data: gNAME[5] }, { data: gNAME[6] }, { data: gNAME[7] }, { data: gNAME[8] }, { data: gNAME[9] }, { data: gNAME[10] }, { data: gNAME[11] }, { data: gNAME[12] }, { data: gNAME[13] }],
      order: [[1, "asc"]],
      scrollX: true,
      rowCallback: function (row, data) {
        // add provinces
        $.ajax({
          headers: {
            Authorization: "Bearer " + this.token,
          },
          type: "GET",
          url: gPROVINCE_URL + "/" + data.provinceId,
          success: function (response) {
            $("td:eq(3)", row).html(response.name);
          },
          error: function (error) {
            console.log(error);
          },
        });

        // add district
        $.ajax({
          headers: {
            Authorization: "Bearer " + this.token,
          },
          type: "GET",
          url: gDISTRICT_URL + "/" + data.districtId,
          success: function (response) {
            $("td:eq(4)", row).html(response.name);
          },
          error: function (error) {
            console.log(error);
          },
        });

        // add investor
        $.ajax({
          headers: {
            Authorization: "Bearer " + this.token,
          },
          type: "GET",
          url: gINVESTOR_URL + "/" + data.investor,
          success: function (response) {
            $("td:eq(11)", row).html(response.name);
          },
          error: function (error) {
            console.log(error);
          },
        });

        // add construction contractor
        $.ajax({
          headers: {
            Authorization: "Bearer " + this.token,
          },
          type: "GET",
          url: gCONTRACTOR_URL + "/" + data.constructionContractor,
          success: function (response) {
            $("td:eq(12)", row).html(response.name);
          },
          error: function (error) {
            console.log(error);
          },
        });

        // add design unit
        $.ajax({
          headers: {
            Authorization: "Bearer " + this.token,
          },
          type: "GET",
          url: gDESIGN_UNIT_URL + "/" + data.designUnit,
          success: function (response) {
            $("td:eq(13)", row).html(response.name);
          },
          error: function (error) {
            console.log(error);
          },
        });
      },
      columnDefs: [
        {

          targets: 0,
          defaultContent: `
                                 <img class="edit-project" src="https://cdn0.iconfinder.com/data/icons/glyphpack/45/edit-alt-512.png" style="width: 20px;cursor:pointer;">
                                 <img class="delete-project" src="https://cdn4.iconfinder.com/data/icons/complete-common-version-6-4/1024/trash-512.png" style="width: 20px;cursor:pointer;">
                               `,
          createdCell: (cell, cellData, rowData, rowIndex, colIndex) => {
            if (colIndex === 0) {
              $(cell).find('.edit-project').on('click', () => {
                const vData = gTable.row(rowIndex).data();
                this.vModal.openModal('edit', vData);
              });

              $(cell).find('.delete-project').on('click', () => {
                const vData = gTable.row(rowIndex).data();
                this.vModal.openModal('delete', vData);
              });
            }
          }
        }
      ],
    });
    gTable.buttons().container().appendTo('.example1_wrapper .col-md-6:eq(0)')
    $('.table-container').css('overflow-x', 'auto');
  }
  _saveExcelFile(data, filename) {
    const blob = new Blob([data], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
    const url = window.URL.createObjectURL(blob);
    const link = document.createElement("a");
    link.href = url;
    link.download = filename;
    link.click();
  }

  _exportToExcel() {
    $("#btn-export-excel").on("click", () => {
      this.vApi.onExportExcelClick((data) => {
        console.log(data)
        this._saveExcelFile(data, "order-detail.xlsx");
      });
    });
  }


  // Hàm sẽ được gọi ở class Main 
  renderPage() {
    this._showAdminName()
    this._exportToExcel()
    this._getProjectList()
  }
}

/*** REGION 3 - vùng hiện modal*/

class Modal {
  constructor() {
    this.vApi = new CallApi()
  }

  _onEventListner() {
    $("#btn-add-project").on("click", () => {
      $("#create-project-modal").modal("show")
    })
    $(".logout").on("click", () => {
      $("#logout-confirm-modal").modal("show")
    })

  }

  _logout() {
    $("#btn-confirm-logout").on("click", () => {
      this.vApi.redirectToLogin()
    })

  }
  _getProvinceList(type) {
    let provinceDropdown
    if (type === "update") {
      provinceDropdown = $("#input-update-provinceId");
    } else {
      provinceDropdown = $("#input-create-provinceId");
    }


    this.vApi.onGetProvinceList((provinces) => {
      provinceDropdown.empty();
      provinceDropdown.append('<option value="" selected>Tỉnh</option>');

      provinces.forEach((province) => {
        const option = `<option value="${province.id}">${province.name}</option>`;
        provinceDropdown.append(option);
      });
    });
  }
  _getDistrictList(provinceId, type) {
    let districtDropdown
    if (type === "update") {
      districtDropdown = $("#input-update-districtId");
    } else {
      districtDropdown = $("#input-create-districtId");
    }


    this.vApi.onGetDistrictListByProvinceId(provinceId, (districts) => {
      districtDropdown.empty();
      districtDropdown.append('<option value="" selected>Huyện</option>');

      districts.forEach((district) => {
        const option = `<option value="${district.id}">${district.name}</option>`;
        districtDropdown.append(option);
      });
    })

  }
  _getWardList(districtId, type) {
    let wardDropdown
    if (type === "update") {
      wardDropdown = $("#input-update-wardId");
    } else {
      wardDropdown = $("#input-create-wardId");
    }
    this.vApi.onGetWardByDistrictId(districtId, (wards) => {
      wardDropdown.empty();
      wardDropdown.append('<option value="" selected>Xã</option>');

      wards.forEach((ward) => {
        const option = `<option value="${ward.id}">${ward.name}</option>`;
        wardDropdown.append(option);
      });
    })

  }
  _getInvestorList(type) {
    let investorDropdown
    if (type === "update") {
      investorDropdown = $("#input-update-investor");
    } else {
      investorDropdown = $("#input-create-investor");
    }

    this.vApi.onGetInvestorsClick((investors) => {
      investorDropdown.empty();
      investorDropdown.append('<option value="" selected>Choose Investor</option>');

      investors.forEach((investor) => {
        const option = `<option value="${investor.id}">${investor.name}</option>`;
        investorDropdown.append(option);
      });
    });
  }
  _getContractorList(type) {
    let contractorDropdown
    if (type === "update") {
      contractorDropdown = $("#input-update-contractor");
    } else {
      contractorDropdown = $("#input-create-contractor");
    }

    this.vApi.onGetContractorsClick((contractors) => {
      contractorDropdown.empty();
      contractorDropdown.append('<option value="" selected>Choose Contractor</option>');

      contractors.forEach((contractor) => {
        const option = `<option value="${contractor.id}">${contractor.name}</option>`;
        contractorDropdown.append(option);
      });
    });
  }
    _getDesginUnitList(type) {
    let designUnitDropdown
    if (type === "update") {
      designUnitDropdown = $("#input-update-design");
    } else {
      designUnitDropdown = $("#input-create-design");
    }
    this.vApi.onGetDesignUnitClick((designUnits) => {
      designUnitDropdown.empty();
      designUnitDropdown.append('<option value="" selected>Choose Design Unit</option>');

      designUnits.forEach((designUnit) => {
        const option = `<option value="${designUnit.id}">${designUnit.name}</option>`;
        designUnitDropdown.append(option);
      });
    });
  }


  _createProject() {
    this._getProvinceList("create");
    this._getInvestorList("create");
    this._getContractorList("create");
    this._getDesginUnitList("create");
    this._clearInput();
    let imageURL;
    $("#input-create-photo").on("change", (event) => {
      const fileInput = event.target;
      const file = fileInput.files[0];
      this.vApi.onUploadFile(file, (response) => {
        imageURL = response
      });
    });


    $("#input-create-provinceId").on("change", () => {
      const provinceId = $("#input-create-provinceId").val();
      console.log(provinceId)
      this._getDistrictList(provinceId);
    });

    $("#input-create-districtId").on("change", () => {
      const districtId = $("#input-create-districtId").val();
      this._getWardList(districtId);
    });

    $("#btn-create-project").on("click", () => {
      this._clearInValid();

      let isValid = true;
      const vFields = [
        "input-create-name",
        "input-create-numApartment"
      ];


      vFields.forEach((field) => {
        const value = $(`#${field}`).val().trim();
        if (!value) {
          isValid = false;
          $(`#${field}`).addClass("is-invalid");
          $(`#${field}`).after(`<div class="invalid-feedback error-message">Please enter a valid value!</div>`);
        }
      });

      if (isValid) {

        const utilitiesCheckboxes = [
          ["input-create-utilities1", "1", "schoolsLibraries"],
          ["input-create-utilities2", "2", "gymShoppingCentres"],
          ["input-create-utilities3", "3", "publicTransports"],
          ["input-create-utilities5", "5", "parkConcert"],

        ];

        const regionLinkCheckboxes = [
          ["input-create-regionLink1", "1", "newYorkCenter"],
          ["input-create-regionLink2", "2", "centralPark"],
          ["input-create-regionLink3", "3", "supermarkets2Miles"],
          ["input-create-regionLink4", "5", "district1"],

        ];

        const utilitiesValue = this._getCheckedValues(utilitiesCheckboxes).join(",");
        const regionLinkValue = this._getCheckedValues(regionLinkCheckboxes).join(",");
        const projectData = {
          name: $("#input-create-name").val().trim(),
          provinceId: parseInt($("#input-create-provinceId").val()),
          districtId: parseInt($("#input-create-districtId").val()),
          wardId: parseInt($("#input-create-wardId").val()),
          latitude: parseFloat($("#input-create-latitude").val()),
          longitude: parseFloat($("#input-create-longitude").val()),
          address: $("#input-create-address").val().trim(),
          slogan: $("#input-create-slogan").val().trim(),
          description: $("#input-create-desc").val().trim(),
          acreage: $("#input-create-acreage").val().trim(),
          numBlock: parseInt($("#input-create-numBlock").val()),
          numFloors: parseInt($("#input-create-numFloors").val()),
          numApartment: parseInt($("#input-create-numApartment").val()),
          apartmentArea: $("#input-create-apartmentArea").val().trim(),
          investor: parseInt($("#input-create-investor").val()),
          constructionContractor: $("#input-create-contractor").val(),
          designUnit: $("#input-create-design").val(),
          utilities: utilitiesValue,
          regionLink: regionLinkValue,
          photo: imageURL
        };
        this.vApi.onCreateProjectClick(projectData, (data) => {
          console.log(data);
        });
      }
    });
  }

  _getCheckedValues(checkboxes) {
    return checkboxes
      .filter(([checkboxId, checkboxValue]) => $(`#${checkboxId}`).prop("checked"))
      .map(([, checkboxValue, checkboxName]) => checkboxValue);
  }

  async _updateProject(data) {
    this._clearInput();
    this._getProvinceList("update");
    this._getInvestorList("update");
    this._getContractorList("update");
    this._getDesginUnitList("update");
    this._clearInValid();
    console.log(data)
    let imageURL;
    $("#input-update-photo").on("change", (event) => {
      const fileInput = event.target;
      const file = fileInput.files[0];
      this.vApi.onUploadFile(file, (response) => {
        imageURL = response
      });
    });


    $("#input-update-provinceId").on("change", () => {
      const provinceId = $("#input-update-provinceId").val();
      this._getDistrictList(provinceId, "update");
    });

    $("#input-update-districtId").on("change", () => {
      const districtId = $("#input-update-districtId").val();
      this._getWardList(districtId, "update");
    });
    try {
      if(data.provinceId) {
        const provinceData = await this.vApi.onGetProvinceById(data.provinceId);
        $('#input-update-provinceId').val(provinceData.id || '');
      }


      if(data.investor) {
        const investorData = await this.vApi.onGetInvestorById(data.investor);
        $('#input-update-investor').val(investorData.id || '');
      }
  
 
    if(data.constructionContractor) {
      const contractorData = await this.vApi.onGetContractorById(data.constructionContractor);
      $('#input-update-contractor').val(contractorData.id || '');
    }

    if(data.districtId) {
      // Get the district name
      const districtData = await this.vApi.onGetDistrictListById(data.districtId);
      $('#input-update-districtId').find('option').text(districtData.name || '');
    }


      
    } catch (error) {
      console.log(error)
    }

   
    $('#input-update-wardId').find('option').text(data.wardId || '');
    $('#input-update-name').val(data.name);
    $('#input-update-latitude').val(data.latitude);
    $('#input-update-longitude').val(data.longitude);
    $('#input-update-address').val(data.address);
    $('#input-update-slogan').val(data.slogan);
    $('#input-update-desc').val(data.description);
    $('#input-update-acreage').val(data.acreage);
    $('#input-update-numBlock').val(data.numBlock);
    $('#input-update-numFloors').val(data.numFloors);
    $('#input-update-numApartment').val(data.numApartment);
    $('#input-update-apartmentArea').val(data.apartmentArea);
    $('#input-update-investor').val(data.investor);
    $('#input-update-contractor').val(data.constructionContractor);
    $('#input-update-design').val(data.designUnit);


    $("#btn-update-project").on("click", () => {
      let isValid = true;
      const requiredFields = [
        "input-update-investor",
        "input-update-numApartment"

      ];

      requiredFields.forEach((field) => {
        if (!$(`#${field}`).val()) {
          isValid = false;
          $(`#${field}`).addClass("is-invalid");
          $(`#${field}`).after('<div class="invalid-feedback">Vui lòng nhập giá trị hợp lệ!</div>');
        }
      });

      const utilitiesCheckboxes = [
        ["input-create-utilities1", "1", "schoolsLibraries"],
        ["input-create-utilities2", "2", "gymShoppingCentres"],
        ["input-create-utilities3", "3", "publicTransports"],
        ["input-create-utilities5", "5", "parkConcert"],

      ];

      const regionLinkCheckboxes = [
        ["input-create-regionLink1", "1", "newYorkCenter"],
        ["input-create-regionLink2", "2", "centralPark"],
        ["input-create-regionLink3", "3", "supermarkets2Miles"],
        ["input-create-regionLink4", "5", "district1"],

      ];

      const utilitiesValue = this._getCheckedValues(utilitiesCheckboxes).join(",");
      const regionLinkValue = this._getCheckedValues(regionLinkCheckboxes).join(",");
      if (isValid) {
        const projectData = {
          name: $("#input-update-name").val().trim(),
          provinceId: parseInt($("#input-update-provinceId").val()),
          districtId: parseInt($("#input-update-districtId").val()),
          wardId: parseInt($("#input-update-wardId").val()),
          latitude: parseFloat($("#input-update-latitude").val()),
          longitude: parseFloat($("#input-update-longitude").val()),
          address: $("#input-update-address").val().trim(),
          slogan: $("#input-update-slogan").val().trim(),
          description: $("#input-update-desc").val().trim(),
          acreage: $("#input-update-acreage").val().trim(),
          numBlock: parseInt($("#input-update-numBlock").val()),
          numFloors: parseInt($("#input-update-numFloors").val()),
          numApartment: parseInt($("#input-update-numApartment").val()),
          apartmentArea: $("#input-update-apartmentArea").val().trim(),
          investor: parseInt($("#input-update-investor").val()),
          constructionContractor: $("#input-update-contractor").val(),
          designUnit: $("#input-update-design").val(),
          utilities: utilitiesValue,
          regionLink: regionLinkValue,
          photo: imageURL
        };

        console.log(projectData)

        this.vApi.onUpdateProjectClick(data.id, projectData, (data) => {
          console.log(data);
        });
      }

    })

  }
  _deleteProject(data) {
    $("#btn-confirm-delete-project").on("click", () => {
      this.vApi.onDeleteProjectClick(data.id)
    })

  }
  _clearInValid(input) {
    if (input) {
      input.removeClass("is-invalid");
      $(".invalid-feedback").remove();
    }
    $(".form-control").removeClass("is-invalid");
    $(".invalid-feedback").remove();
  }

  _clearInput() {
    $("#create-project-form").find("input").val("");
    $("#create-project-form").find("select").val("")
  }
  openModal(type, data) {
    this._logout();
    if (type === "create") {
      this._clearInput();
      this._onEventListner();
      this._createProject();
    } else {
      if (type === "edit") {
        this._updateProject(data)
        $("#update-project-modal").modal("show")
      } else {
        this._deleteProject(data)
        $("#delete-confirm-modal").modal("show")
      }
    }

  }
}

/*** REGION 4 - vùng để gọi lên cơ sở dữ liệu lấy đa ta về*/
class CallApi {
  constructor() {
    this.token = this.getCookie("token");
  }

  setToken(token) {
    this.token = token;
  }

  getHeaders() {
    return {
      Authorization: "Bearer " + this.token
    };
  }
  setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  }

  redirectToLogin() {
    // Trước khi logout cần xóa token đã lưu trong cookie
    this.setCookie("token", "", 1);
    window.location.href = "http://127.0.0.1:5500/real-estates-frontend/html/login.html";
  }

  onShowToast(paramTitle, paramMessage) {
    $('#myToast .mr-auto').text(paramTitle)
    $('#myToast .toast-body').text(paramMessage);
    $('#myToast').toast('show');
  }

  onGetProjectsClick(paramCallbackFn) {
    $.ajax({
      url: gPROJECT_URL,
      method: 'GET',
      headers: this.getHeaders(),
      success: function (data) {
        paramCallbackFn(data);
      },
      error: (jqXHR, textStatus, errorThrown) => {
        console.log('Error:', errorThrown);
        this.redirectToLogin();
      }
    });
  }

  onGetProvinceList(paramCallbackFn) {
    $.ajax({
      url: gPROVINCE_URL,
      method: 'GET',
      headers: this.getHeaders(),
      success: function (data) {
        paramCallbackFn(data);
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log('Error:', errorThrown);
      }
    });
  }

  onGetDistrictListByProvinceId(provinceId, paramCallbackFn) {
    $.ajax({
      url: `${gDISTRICT_URL}/${provinceId}/province`,
      method: 'GET',
      headers: this.getHeaders(),
      success: function (data) {
        paramCallbackFn(data);
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log('Error:', errorThrown);
      }
    });
  }

  onGetWardByDistrictId(districtId, paramCallbackFn) {
    $.ajax({
      url: `${gWARD_URL}/${districtId}/district`,
      method: 'GET',
      headers: this.getHeaders(),
      success: function (data) {
        paramCallbackFn(data);
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log('Error:', errorThrown);
      }
    });
  }

  onGetInvestorsClick(paramCallbackFn) {
    $.ajax({
      url: gINVESTOR_URL,
      method: 'GET',
      headers: this.getHeaders(),
      success: function (data) {
        paramCallbackFn(data);
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log('Error:', errorThrown);
      }
    });
  }

  onGetDistrictListById(id) {
    return new Promise((resolve, reject) => {
      $.ajax({
        url: `${gDISTRICT_URL}/${id}`,
        method: 'GET',
        headers: this.getHeaders(),
        success: function (data) {
          resolve(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
          reject(errorThrown);
        }
      });
    });
  }

  onGetInvestorById(id) {
    return new Promise((resolve, reject) => {
      $.ajax({
        url: `${gINVESTOR_URL}/${id}`,
        method: 'GET',
        headers: this.getHeaders(),
        success: function (data) {
          resolve(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
          reject(errorThrown);
        }
      });
    });
  }

  onGetDesignUnitById(id) {
    return new Promise((resolve, reject) => {
      $.ajax({
        url: `${gDESIGN_UNIT_URL}/${id}`,
        method: 'GET',
        headers: this.getHeaders(),
        success: function (data) {
          resolve(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
          reject(errorThrown);
        }
      });
    });
  }

  onGetContractorById(id) {
    return new Promise((resolve, reject) => {
      $.ajax({
        url: `${gCONTRACTOR_URL}/${id}`,
        method: 'GET',
        headers: this.getHeaders(),
        success: function (data) {
          resolve(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
          reject(errorThrown);
        }
      });
    });
  }

  onGetWardById(id) {
    return new Promise((resolve, reject) => {
      $.ajax({
        url: `${gWARD_URL}/${id}`,
        method: 'GET',
        headers: this.getHeaders(),
        success: function (data) {
          resolve(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
          reject(errorThrown);
        }
      });
    });
  }

  onGetProvinceById(id) {
    return new Promise((resolve, reject) => {
      $.ajax({
        url: `${gPROVINCE_URL}/${id}`,
        method: 'GET',
        headers: this.getHeaders(),
        success: function (data) {
          resolve(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
          reject(errorThrown);
        }
      });
    });
  }

  onGetStreetById(id) {
    return new Promise((resolve, reject) => {
      $.ajax({
        url: `${gSTREET_URL}/${id}`,
        method: 'GET',
        headers: this.getHeaders(),
        success: function (data) {
          resolve(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
          reject(errorThrown);
        }
      });
    });
  }

  onGetContractorsClick(paramCallbackFn) {
    $.ajax({
      url: gCONTRACTOR_URL,
      method: 'GET',
      headers: this.getHeaders(),
      success: function (data) {
        paramCallbackFn(data);
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log('Error:', errorThrown);
      }
    });
  }

  onGetDesignUnitClick(paramCallbackFn) {
    $.ajax({
      url: gDESIGN_UNIT_URL,
      method: 'GET',
      headers: this.getHeaders(),
      success: function (data) {
        paramCallbackFn(data);
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log('Error:', errorThrown);
      }
    });
  }

  onGetProjectByIdClick(projectId, paramCallbackFn) {
    $.ajax({
      url: gPROJECT_URL + "/" + projectId,
      method: 'GET',
      headers: this.getHeaders(),
      success: function (data) {
        paramCallbackFn(data);
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log('Error:', errorThrown);
      }
    });
  }

  onCreateProjectClick(projectData, paramCallbackFn) {
    $.ajax({
      url: gPROJECT_URL,
      method: 'POST',
      data: JSON.stringify(projectData),
      headers: this.getHeaders(),
      contentType: 'application/json',
      success: (data) => {
        paramCallbackFn(data);
        const render = new RenderPage()
        render.renderPage()
        this.onShowToast("Tạo thành công", "Tạo project thành công!!")
        window.location.href = "http://127.0.0.1:5500/real-estates-frontend/html/project.html";
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log('Error:', errorThrown);
      }
    });
  }
  onUpdateProjectClick(projectId, projectData, paramCallbackFn) {
    $.ajax({
      url: gPROJECT_URL + "/" + projectId,
      method: 'PUT',
      data: JSON.stringify(projectData),
      headers: this.getHeaders(),
      contentType: 'application/json',
      success: (data) => {
        const render = new RenderPage()
        render.renderPage()
        location.reload()
        paramCallbackFn(data);
        $('#update-project-modal').modal('hide');
        this.onShowToast("Sửa thành công", "Sửa project thành công!!")
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log('Error:', errorThrown);
      }
    });
  }

  onDeleteProjectClick(projectId) {
    $.ajax({
      url: gPROJECT_URL + "/" + projectId,
      method: 'DELETE',
      headers: this.getHeaders(),
      success: () => {
        this.onShowToast("Xóa thành công", "Bạn đã xóa project thành công!!");
        const render = new RenderPage();
        render.renderPage();
        $("#delete-confirm-modal").modal("hide");
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log('Error:', errorThrown);
      }
    });
  }

  onUploadFile(file, paramCallbackFn) {
    const formData = new FormData();
    formData.append('image', file);

    $.ajax({
      url: gUPLOAD_FILE_URL,
      method: 'POST',
      data: formData,
      headers: this.getHeaders(),
      processData: false,
      contentType: false,
      success: (response) => {
        paramCallbackFn(response);
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log('Error:', errorThrown);
      }
    });
  }
  onExportExcelClick(paramCallbackFn) {
    $.ajax({
      url: `http://localhost:8080/api/export/order-details/excel`,
      method: 'GET',
      headers: this.getHeaders(),
      xhrFields: {
        responseType: "blob"
      },
      success: function (data) {
        paramCallbackFn(data);
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log('Error:', errorThrown);
      }
    });
  }

  getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }
  onGetStreetById(id) {
    return new Promise((resolve, reject) => {
      $.ajax({
        url: `${gSTREET_URL}/${id}`,
        method: 'GET',
        headers: this.getHeaders(),
        success: function (data) {
          resolve(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
          reject(errorThrown);
        }
      });
    });
  }

    onGetCustomerById(id) {
    return new Promise((resolve, reject) => {
      $.ajax({
        url: `${gCUSTOMER_URL}/${id}`,
        method: 'GET',
        headers: this.getHeaders(),
        success: function (data) {
          resolve(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
          reject(errorThrown);
        }
      });
    });
  }
  onGetDistrictListById(id) {
    return new Promise((resolve, reject) => {
      $.ajax({
        url: `${gDISTRICT_URL}/${id}`,
        method: 'GET',
        headers: this.getHeaders(),
        success: function (data) {
          resolve(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
          reject(errorThrown);
        }
      });
    });
  }
  onGetWardById(id) {
    return new Promise((resolve, reject) => {
      $.ajax({
        url: `${gWARD_URL}/${id}`,
        method: 'GET',
        headers: this.getHeaders(),
        success: function (data) {
          resolve(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
          reject(errorThrown);
        }
      });
    });
  }
  onGetProvinceById(id) {
    return new Promise((resolve, reject) => {
      $.ajax({
        url: `${gPROVINCE_URL}/${id}`,
        method: 'GET',
        headers: this.getHeaders(),
        success: function (data) {
          resolve(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
          reject(errorThrown);
        }
      });
    });
  }
}