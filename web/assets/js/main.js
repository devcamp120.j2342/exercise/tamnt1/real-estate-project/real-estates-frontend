/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBASE_URL = "http://localhost:8080/users/me"
const gREAL_ESTATE_URL = "http://localhost:8080/api/real-estates";
const gPROVINCE_URL = "http://localhost:8080/api/provinces";
const gDISTRICT_URL = "http://localhost:8080/api/districts";
const gSTREET_URL = "http://localhost:8080/api/streets";
const gWARD_URL = "http://localhost:8080/api/wards";
const gPROJECT_URL = "http://localhost:8080/api/projects";
const gCUSTOMER_URL = "http://localhost:8080/api/customers";
const gUPLOAD_FILE_URL = "http://localhost:8080/api/upload";
const gCUSTOMER_INFO_URL = "http://localhost:8080/users/me"
/*** REGION 2 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
$(document).ready(function () {
  "use strict";

  showProfileAfterLogin()
  const select = (el, all = false) => {
    el = el.trim();
    if (all) {
      return $(el);
    } else {
      return $(el).first();
    }
  };

  $('#input-city').select2({
    theme: 'bootstrap'
  });

  const on = (type, el, listener, all = false) => {
    const selectEl = select(el, all);
    if (selectEl.length) {
      selectEl.on(type, listener);
    }
  };

  // Vùng scroll 
  const onscroll = (el, listener) => {
    el.addEventListener('scroll', listener);
  };

  // vùng toogle
  const selectHNavbar = select('.navbar-default');
  if (selectHNavbar.length) {
    $(window).scroll(function () {
      if ($(this).scrollTop() > 100) {
        selectHNavbar.addClass('navbar-reduce').removeClass('navbar-trans');
      } else {
        selectHNavbar.removeClass('navbar-reduce').addClass('navbar-trans');
      }
    });
  }

  // nút về top 
  const backtotop = select('.back-to-top');
  if (backtotop.length) {
    const toggleBacktotop = () => {
      if ($(window).scrollTop() > 100) {
        backtotop.addClass('active');
      } else {
        backtotop.removeClass('active');
      }
    };
    $(window).on('load', toggleBacktotop);
    onscroll(document, toggleBacktotop);
  }

  // loading page khi mà vừa vào trang
  const preloader = select('#preloader');
  if (preloader.length) {
    $(window).on('load', function () {
      preloader.remove();
    });
  }
  // cửa sổ tìm kiếm bật tắt
  const body = select('body');
  on('click', '.navbar-toggle-box', function (e) {
    e.preventDefault();
    body.addClass('box-collapse-open').removeClass('box-collapse-closed');
  });

  on('click', '.close-box-collapse', function (e) {
    e.preventDefault();
    body.removeClass('box-collapse-open').addClass('box-collapse-closed');
  });

  // slider tự chạy
  new Swiper('.intro-carousel', {
    speed: 600,
    loop: true,
    autoplay: {
      delay: 2000,
      disableOnInteraction: false
    },
    slidesPerView: 'auto',
    pagination: {
      el: '.swiper-pagination',
      type: 'bullets',
      clickable: true
    }
  });


  new Swiper('#property-carousel', {
    speed: 600,
    loop: true,
    autoplay: {
      delay: 5000,
      disableOnInteraction: false
    },
    slidesPerView: 'auto',
    pagination: {
      el: '.propery-carousel-pagination',
      type: 'bullets',
      clickable: true
    },
    breakpoints: {
      320: {
        slidesPerView: 1,
        spaceBetween: 20
      },
      1200: {
        slidesPerView: 3,
        spaceBetween: 20
      }
    }
  });


  new Swiper('#news-carousel', {
    speed: 600,
    loop: true,
    autoplay: {
      delay: 5000,
      disableOnInteraction: false
    },
    slidesPerView: 'auto',
    pagination: {
      el: '.news-carousel-pagination',
      type: 'bullets',
      clickable: true
    },
    breakpoints: {
      320: {
        slidesPerView: 1,
        spaceBetween: 20
      },
      1200: {
        slidesPerView: 3,
        spaceBetween: 20
      }
    }
  });


  new Swiper('#testimonial-carousel', {
    speed: 600,
    loop: true,
    autoplay: {
      delay: 5000,
      disableOnInteraction: false
    },
    slidesPerView: 'auto',
    pagination: {
      el: '.testimonial-carousel-pagination',
      type: 'bullets',
      clickable: true
    }
  });


  new Swiper('#property-single-carousel', {
    speed: 600,
    loop: true,
    autoplay: {
      delay: 5000,
      disableOnInteraction: false
    },
    pagination: {
      el: '.property-single-carousel-pagination',
      type: 'bullets',
      clickable: true
    }
  });



  const PAGE = 0
  const SIZE = 6
  const customerId = JSON.parse(localStorage.getItem('customer'))?.id;
  if (customerId) {
    onGetRealEstatesByCustomerIdClick(customerId, (data) => {
      renderPropertiesByCustomer(PAGE + 1, data)
      renderPropertiesByCustomerPending(PAGE + 1, data)
    })
  }

  onGetRealEstateClick(PAGE, SIZE, (data) => {
    renderProperties(PAGE + 1, SIZE, data)

  })

  $(".pagination-container").on("click", ".page-link", function (event) {
    event.preventDefault();
    const nextPage = parseInt($(this).attr("data-page"));
    const searchKeyword = $('#search-keyword').val();
    const size = 6;

    searchProperty(searchKeyword, nextPage, size, (data) => {
      renderProperties(nextPage, size, data);
    });
  });
  $("#btn-delete-cancel").on("click", function () {
    $("#delete-confirm-modal").modal("hide")
  })
  $(".close-update").on("click", function () {
    $("#update-realEstate-modal").modal("hide")
  })

  $('.search-property').on('click', function (event) {
    event.preventDefault();
    const searchKeyword = $('#search-keyword').val();
    const page = 0;
    const size = 6;
    searchProperty(searchKeyword, page, size, (data) => {
      renderProperties(page + 1, size, data);
    });
  });

  $('.filter-property').on('click', function (event) {
    event.preventDefault();

    // Get the filter values from the form
    const bedroom = $('#input-bedroom').val();
    const bath = $('#input-bath').val();
    const price = $('#input-price').val();
    const provinceId = $('#input-city').val();

    const filterData = {
      bedroom: bedroom === 'Any' ? null : parseInt(bedroom),
      bath: bath === 'Any' ? null : parseInt(bath),
      price: price === 'Unlimited' ? null : parseInt(price.replace(/[^0-9]/g, '')),
      provinceId: provinceId === 'All City' ? null : parseInt(provinceId),
    };


    const page = 0;
    const size = 6;
    filterProperty(page, size, filterData, (data) => {
      renderProperties(page + 1, size, data);
    });

  });
  $(".logout-item").on("click", () => {
    logout();
    showProfileAfterLogin()
  })

  $(".property-wrapper").on("click", ".property-link", function (event) {
    event.preventDefault();
    const propertyId = $(this).data("property-id");

    const propertyDetailUrl = `http://127.0.0.1:5500/real-estates-frontend/web/property-single.html?id=${propertyId}`;
    window.location.href = propertyDetailUrl;

  });

  getDetailPage();
  getProvinceFilterList()

});
/*** REGION 3 -   RENDER API - Vùng để render ra sản phẩm*/
function renderProperties(currentPage, size, paramHomes) {

  const totalPages = Math.floor(paramHomes.totalCount / size);
  const propertyGrid = $(".property-wrapper");
  propertyGrid.empty();

  paramHomes.data.forEach((property) => {
    let propertyCard
    if (property.proccess === "active") {
      propertyCard = `
      <div class="col-md-4">
        <div class="card-box-a card-shadow">  
          <div class="img-box-a">
            <img src="${property.photo || getRandomHomePhoto()}" alt="" class="img-a img-fluid">
          </div>
          <div class="card-overlay">
            <div class="card-overlay-a-content">
              <div class="card-header-a">
                <h5 class="card-title-a">
                 ${property.title || ""}
                </h5>
              </div>
              <div class="card-body-a">
                <div class="price-box d-flex">
                  <span class="price-a">${property.status || "Seller"} | $ ${property.price}</span>
                </div>  
                <a href="http://127.0.0.1:5500/real-estates-frontend/web/property-single.html?" class="link-a property-link" data-property-id=${property.id}>Click here to view
                  <span class="bi bi-chevron-right"></span>
                </a>
              </div>
              <div class="card-footer-a container">
                <ul class="card-info d-flex justify-content-around">
                <li>
                    <h4 class="card-info-title">Area</h4>
                    <span>${truncateAddress(property.address)}</span>
               </li>
                  <li>
                    <h4 class="card-info-title">Beds</h4>
                    <span>${property.bedroom || ""}</span>
                  </li>
                  <li>
                    <h4 class="card-info-title">Baths</h4>
                    <span>${property.bath || ""}</span>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    `;
      propertyGrid.append(propertyCard);
    }


  });
  const paginationHtml = createPagination(totalPages, currentPage);
  $(".pagination-container").html(paginationHtml);
}

// cho khách hàng xem bài đăng của họ

function renderPropertiesByCustomer(currentPage, paramHomes) {

  const totalPages = 1
  const propertyGrid = $(".property-container");
  propertyGrid.empty();
  const activeProperties = paramHomes.data.filter(property => property.proccess === "active");

  paramHomes.data.forEach((property) => {
    let propertyCard
    if (property.proccess === "active") {
      propertyCard = `
      <div class="col-md-4">
        <div class="card-box-a card-shadow">  
          <div class="img-box-a">
            <img src="${property.photo || getRandomHomePhoto()}" alt="" class="img-a img-fluid">
          </div>
          <div class="card-overlay">
            <div class="card-overlay-a-content">
              <div class="card-header-a">
                <h5 class="card-title-a">
                 ${property.title || ""}
                </h5>
              </div>
              <div class="card-body-a">
                <div class="price-box d-flex">
                  <span class="price-a">${property.status || "Seller"} | $ ${property.price}</span>
                </div>  
                <button class="btn btn-c" id="btn-modal-update"  data-property-id=${property.id}>Update
                </button>
                <button class="btn btn-d" id="btn-modal-delete"  data-property-id=${property.id}>Delete
                </button>
              </div>
              <div class="card-footer-a container">
                <ul class="card-info d-flex justify-content-around">
                  <li>
                      <h4 class="card-info-title">Area</h4>
                      <span>${truncateAddress(property.address)}</span>
                  </li>
                  <li>
                    <h4 class="card-info-title">Beds</h4>
                    <span>${property.bedroom||""}</span>
                  </li>
                  <li>
                    <h4 class="card-info-title">Baths</h4>
                    <span>${property.bath || 2}</span>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    `;
      propertyGrid.append(propertyCard);
      propertyGrid.on("click", "#btn-modal-update", function () {
        const propertyId = $(this).data("property-id");

        updateRealEstate(propertyId)
      })
      propertyGrid.on("click", "#btn-modal-delete", function () {
        const propertyId = $(this).data("property-id");

        deleteRealEstate(propertyId)
      })

    }


  });
  if (activeProperties.length) {
    const paginationHtml = createPagination(totalPages, currentPage);
    $(".pagination-wrapper").html(paginationHtml);
  }

}

function renderPropertiesByCustomerPending(currentPage, paramHomes) {
  const totalPages = 1
  const propertyGrid = $(".property-container-pending");
  propertyGrid.empty(); // Clear the existing content if any
  const pendingProperties = paramHomes.data.filter(property => property.process !== "active");
  paramHomes.data.forEach((property) => {
    let propertyCard
    if (property.proccess === "pending") {
      propertyCard = `
      <div class="col-md-4">
        <div class="card-box-a card-shadow">  
          <div class="img-box-a">
            <img src="${property.photo || getRandomHomePhoto()}" alt="" class="img-a img-fluid">
          </div>
          <div class="card-overlay">
            <div class="card-overlay-a-content">
              <div class="card-header-a">
                <h5 class="card-title-a">
                 ${property.title || ""}
                </h5>
              </div>
              <div class="card-body-a">
                <div class="price-box d-flex">
                  <span class="price-a">${property.status || "Seller"} | $ ${property.price}</span>
                </div>  
                <button class="btn btn-c" id="btn-modal-update-pending"  data-property-id=${property.id}>Update
                </button>
                <button class="btn btn-d" id="btn-modal-delete-pending"  data-property-id=${property.id}>Delete
                </button>
              </div>
              <div class="card-footer-a container">
                <ul class="card-info d-flex justify-content-around">
                  <li>
                  <h4 class="card-info-title">Area</h4>
                  <span>${truncateAddress(property.address)}</span>
                  </li>
                  <li>
                    <h4 class="card-info-title">Beds</h4>
                    <span>${property.acreage}</span>
                  </li>
                  <li>
                    <h4 class="card-info-title">Baths</h4>
                    <span>${property.bath || 2}</span>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    `;

      propertyGrid.append(propertyCard);
      propertyGrid.on("click", "#btn-modal-update-pending", function () {
        const propertyId = $(this).data("property-id");
        updateRealEstate(propertyId)
      })
      propertyGrid.on("click", "#btn-modal-delete-pending", function () {
        const propertyId = $(this).data("property-id");
        console.log(propertyId)
        deleteRealEstate(propertyId)
      })
    }
  });
  if (pendingProperties.length) {
    const paginationHtml = createPagination(totalPages, currentPage);
    $(".pagination-wrapper-pending").html(paginationHtml);
  }

}

function getDetailPage() {
  const propertyId = getQueryParam('id');
  if (propertyId) {
    onGetRealEstateByIdClick(propertyId, (data) => {
      renderDetail(data)
    });
  }
}

async function renderDetail(property) {
  $(".detail-price").text(property.price);
  let province
  if (property.provinceId) {
    province = await onGetProvinceById(property.provinceId)
  }
  const request = property.request;
  let requestText = "";
  switch (request) {
    case 1:
      requestText = "Cần bán";
      break;
    case 2:
      requestText = "Cần mua";
      break;
    case 3:
      requestText = "Cần thuê";
      break;
    case 4:
      requestText = "Cho thuê";
      break;
    default:
      requestText = "Không để";
      break;
  }
  const type = property.type;
  let typeText = "";
  switch (type) {
    case 0:
      typeText = "Phòng trọ";
      break;
    case 1:
      typeText = "Đất";
      break;
    case 2:
      typeText = "Nhà ở";
      break;
    case 3:
      typeText = "Chung cư";
      break;
    case 4:
      typeText = "Văn phòng";
      break;
    case 5:
      typeText = "Kinh doanh";
      break;
    default:
      typeText = "Phòng trọ";
      break;
  }

  // Update property summary
  $(".detail-property-title").text(property.title);
  $(".detail-property-address").text(property.address);
  $(".detail-property-id").text(property.id);
  $(".detail-location").text(province.name);
  $(".detail-request").text(requestText);
  $(".detail-type").text(typeText);
  $(".detail-acreage").text(property.acreage);
  $(".detail-bedroom").text(property.bedroom || "Không để");
  $(".detail-bath").text(property.bath || "Không để");
  $(".detail-image").attr("src", property.photo || getRandomHomePhoto());
  // Update property description
  $(".property-description .description").text(property.description);
}

function createPagination(totalPages, currentPage) {
  const maxVisiblePages = 5; // Maximum number of visible pages in the pagination

  let paginationHtml = `
    <div class="col-sm-12">
      <nav class="pagination-a">
        <ul class="pagination justify-content-end">
          <li class="page-item ${currentPage === 1 ? 'disabled' : ''}">
            <a class="page-link" href="#" tabindex="-1" data-page="${currentPage - 1}">
              <span class="bi bi-chevron-left"></span>
            </a>
          </li>
  `;

  // Show ellipsis and pages before current page
  for (let page = 1; page < currentPage; page++) {
    if (currentPage - page < maxVisiblePages) {
      paginationHtml += `
        <li class="page-item">
          <a class="page-link" href="#" data-page="${page}">${page}</a>
        </li>
      `;
    }
  }

  // Show current page
  paginationHtml += `
    <li class="page-item active">
      <a class="page-link" href="#" data-page="${currentPage}">${currentPage}</a>
    </li>
  `;

  // Show ellipsis and pages after current page
  for (let page = currentPage + 1; page <= totalPages; page++) {
    if (page - currentPage < maxVisiblePages) {
      paginationHtml += `
        <li class="page-item">
          <a class="page-link" href="#" data-page="${page}">${page}</a>
        </li>
      `;
    }
  }

  paginationHtml += `
          <li class="page-item next ${currentPage === totalPages ? 'disabled' : ''}">
            <a class="page-link" href="#" data-page="${currentPage + 1}">
              <span class="bi bi-chevron-right"></span>
            </a>
          </li>
        </ul>
      </nav>
    </div>
  `;

  return paginationHtml;
}

function searchProperty(searchKeyword, page, size, callbackFn) {
  if (searchKeyword) {
    onGetRealEstateBySearch(page, size, searchKeyword, (result) => {
      callbackFn(result);
    });
  } else {
    onGetRealEstateClick(page, size, (data) => {
      callbackFn(data);
    });
  }
}
function filterProperty(page, size, data, callbackFn) {
  const { bedroom, provinceId, price, bath } = data;

  if (bedroom || provinceId || price || bath) {
    const filterData = {
      bath: bath || null,
      bedroom: bedroom || null,
      provinceId: provinceId || null,
      price: price || null
    };

    onGetRealEstateByFilter(page, size, filterData, (result) => {
      callbackFn(result);
    });
  } else {
    onGetRealEstateClick(page, size, (data) => {
      callbackFn(data);
    });
  }
}


function getProvinceFilterList() {
  let provinceDropdown = $("#input-city")
  onGetProvinceList((provinces) => {
    provinceDropdown.empty();
    provinceDropdown.append('<option value="" selected>City</option>');

    provinces.forEach((province) => {
      const option = `<option value="${province.id}">${province.name}</option>`;
      provinceDropdown.append(option);
    });
  });

}
function showProfileAfterLogin() {
  const customer = JSON.parse(localStorage.getItem('customer'));
  if (!customer || !customer.accessToken) {
    $('.login').show();
    $('.profile-item').hide();
    $('.create-realEstate').hide();
  } else {
    $('.profile-item').show();
    $('.login').hide();
    $('.create-realEstate').show();
  }
}

function getProvinceList(type) {
  let provinceDropdown
  if (type === "update") {
    provinceDropdown = $("#input-update-provinceId");
  } else {
    provinceDropdown = $("#input-create-provinceId");
  }
  onGetProvinceList((provinces) => {
    provinceDropdown.empty();
    provinceDropdown.append('<option value="" selected>Province</option>');

    provinces.forEach((province) => {
      const option = `<option value="${province.id}">${province.name}</option>`;
      provinceDropdown.append(option);
    });
  });
}
function getDistrictList(type, provinceId) {
  let districtDropdown
  if (type === "update") {
    districtDropdown = $("#input-update-districtId");
  } else {
    districtDropdown = $("#input-create-districtId");
  }

  onGetDistrictListByProvinceId(provinceId, (districts) => {
    districtDropdown.empty();
    districtDropdown.append('<option value="" selected>District</option>');

    districts.forEach((district) => {
      const option = `<option value="${district.id}">${district.name}</option>`;
      districtDropdown.append(option);
    });
  })

}
function getWardList(type, districtId) {
  let wardDropdown
  if (type === "update") {
    wardDropdown = $("#input-update-wardId");
  } else {
    wardDropdown = $("#input-create-wardId");
  }

  onGetWardByDistrictId(districtId, (wards) => {
    wardDropdown.empty();
    wardDropdown.append('<option value="" selected>Ward</option>');

    wards.forEach((ward) => {
      const option = `<option value="${ward.id}">${ward.name}</option>`;
      wardDropdown.append(option);
    });
  })

}
function getStreetList(type, districtId) {
  let streetDropdown
  if (type === "update") {
    streetDropdown = $("#input-update-streetId");
  } else {
    streetDropdown = $("#input-create-streetId");
  }

  onGetStreetByDistrictId(districtId, (streets) => {
    streetDropdown.empty();
    streetDropdown.append('<option value="" selected>Đường</option>');

    streets.forEach((street) => {
      const option = `<option value="${street.id}">${street.name}</option>`;
      streetDropdown.append(option);
    });
  })

}
function getProjectList(type) {
  let projectDropdown
  if (type === "update") {
    projectDropdown = $("#input-update-project");
  } else {
    projectDropdown = $("#input-create-project");
  }

  onGetProjectList((projects) => {
    projectDropdown.empty();
    projectDropdown.append('<option value="" selected>Project</option>');

    projects.forEach((project) => {
      const option = `<option value="${project.id}">${project.name}</option>`;
      projectDropdown.append(option);
    });
  })
}
async function updateRealEstate(id) {
  $("#update-realEstate-modal").modal("show")
  getProjectList("update")
  getProvinceList("update")
  const userId = JSON.parse(localStorage.getItem('customer')).id;
  let imageURL;
  $("#input-update-photo").on("change", (event) => {
    const fileInput = event.target;
    const file = fileInput.files[0];
    onUploadFile(file, (response) => {
      imageURL = response
      $(".imagePreview ").css("background-image", "url(" + response + ")");
    });
  });
  $("#input-update-provinceId").on("change", () => {
    const provinceId = $("#input-update-provinceId").val();
    getDistrictList("update", provinceId);
  });

  $("#input-update-districtId").on("change", () => {
    const districtId = $("#input-update-districtId").val();
    getWardList("update", districtId);
    getStreetList("update", districtId)
  });
  try {
    onGetRealEstateByIdClick(id, async (data) => {
      getDistrictList("update", data.provinceId);
      getWardList("update", data.districtId);
      getStreetList("update", data.districtId);
      console.log(data)
      if (data.provinceId) {
        const provinceData = await onGetProvinceById(data.provinceId);
        $('#input-update-provinceId').val(provinceData.id || '');
      }

      // Get the district name
      if (data.districtId) {
        const districtData = await onGetDistrictListById(data.districtId);
        $('#input-update-districtId').val(districtData.id || '')

      }

      // Get the ward name
      if (data.wardsId) {
        const wardData = await onGetWardById(data?.wardsId);
        $('#input-update-wardId').val(wardData.id || '')
      }

      if (data.streetId) {
        const streetData = await onGetStreetById(data.streetId);
        $('#input-update-streetId').val(streetData.id || '')
      }
      if (data.projectId) {
        const projectData = await onGetProjectById(data.projectId);
        $('#input-update-project').val(projectData.id || '');
      }
      $('#input-update-address').val(data.address);
      $('#input-update-title').val(data.title);
      $('#input-update-type').val(data.type);
      $('#input-update-request2').val(data.request);
      $('#input-update-price').val(data.price);
      $('#input-update-priceMin').val(data.priceMin);
      $('#input-update-priceTime').val(data.priceTime);
      $('#input-update-apartCode').val(data.apartCode);
      $('#input-update-acreage').val(data.acreage);
      $('#input-update-bedroom').val(data.bedroom);
      $('#input-update-balcony').val(data.balcony);
      $('#input-update-landscapeView').val(data.landscapeView);
      $('#input-update-apartLoca').val(data.apartLoca);
      $('#input-update-apartType').val(data.apartType);
      $('#input-update-rent').val(data.priceRent);
      $('#input-update-direction').val(data.direction);
      $('#input-update-totalFloors').val(data.totalFloors);
      $('#input-update-floorNum').val(data.numberFloors);
      $('#input-update-balcony').val(data.balcony);
      $('#input-update-desc').val(data.description);
      $('#input-update-furniture').val(data.furniture);
      $('#input-update-width').val(data.widthY);
      $('#input-update-long').val(data.longX);
      $('#input-update-shape').val(data.shape);
      $('#input-update-longi').val(data.long);
      $('#input-update-lat').val(data.lat);
      $('#input-update-alleyMinWidth').val(data.alleyMinWidth);
      $('#input-update-clcl').val(data.clcl);
      if (data.photo) {
        $(`.imagePreview`).css("background-image", `url(${data.photo})`);
      } else {
        $(`.imagePreview`).css("background-image", `url(${""})`);
      }

    })

  } catch (error) {
    console.log(error)
  }

  $("#btn-update-realestate").off("click").on("click", (event) => {
    event.preventDefault();
    const address = $("#input-update-address").val();
    const photo = imageURL;
    const provinceId = parseInt($("#input-update-provinceId").val());
    const districtId = parseInt($("#input-update-districtId").val());
    const wardsId = parseInt($("#input-update-wardId").val());
    const streetId = parseInt($("#input-update-streetId").val());
    const projectId = parseInt($("#input-update-project").val());
    const title = $("#input-update-title").val();
    const type = parseInt($("#input-update-type").val());
    const request = parseInt($("#input-update-request").val());
    const customerId = userId;
    const price = $("#input-update-price").val();
    const priceMin = $("#input-update-priceMin").val();
    const priceTime = parseInt($("#input-update-priceTime").val());
    const apartCode = $("#input-update-apartCode").val();
    const acreage = $("#input-update-acreage").val();
    const bedroom = $("#input-update-bedroom").val();
    const balcony = $("#input-update-balcony").val();
    const landscapeView = $("#input-update-landscapeView").val();
    const apartLoca = $("#input-update-apartLoca").val();
    const apartType = $("#input-update-apartType").val();
    const rentPrice = $("#input-update-rent").val();
    const bdsArea = $("#input-update-acreage").val();
    const houseDirection = $("#input-update-acreage").val();
    const totalFloors = parseInt($("#input-update-totalFloors").val());
    const numberFloors = parseInt($("#input-update-floorNum").val());
    const description = $("#input-update-desc").val();
    const width = $("#input-update-width").val();
    const length = $("#input-update-long").val();
    const createdBy = $("#input-update-createdBy").prop('checked');
    const shape = $("#input-update-shape").val();
    const adjacentRoad = $("#input-update-adjacentRoad").val();
    const alleyMinWidth = $("#input-update-alleyMinWidth").val();
    const ctxdPrice = $("#input-update-ctxdPrice").val();
    const ctxdValue = $("#input-update-ctxdValue").val();
    const alleyWidth = $("#input-update-alleyMinWidth").val();
    const factor = $("#input-update-factor").val();
    const structure = $("#input-update-structure").val();
    const clcl = $("#input-update-clcl").val();
    const lat = $("#input-update-lat").val();
    const long = ($("#input-update-long").val());
    const proccess = "pending";
    const realEstateData = {
      address,
      photo,
      provinceId,
      districtId,
      wardsId,
      streetId,
      projectId,
      title,
      type,
      request,
      customerId,
      price,
      priceMin,
      priceTime,
      apartCode,
      acreage,
      bedroom,
      balcony,
      landscapeView,
      apartLoca,
      apartType,
      rentPrice,
      bdsArea,
      houseDirection,
      totalFloors,
      numberFloors,
      description,
      width,
      length,
      createdBy,
      shape,
      adjacentRoad,
      alleyMinWidth,
      ctxdPrice,
      ctxdValue,
      alleyWidth,
      factor,
      structure,
      clcl,
      lat,
      long,
      proccess
    };

    onUpdateRealEstateClick(id, realEstateData, (data) => {
      console.log(data)
    })
  })

}

function deleteRealEstate(id) {
  $("#delete-confirm-modal").modal("show")

  $("#btn-confirm-delete-realEstate").on("click", function () {
    onDeleteRealEstateClick(id)
  })

}

function logout() {
  setCookie("token", "", 1);
  localStorage.removeItem('customer');

}

/*** REGION 4 -   API CALL - Vùng gọi api trả về data */

function onGetRealEstateClick(page, size, paramCallbackFn) {
  $.ajax({
    url: `${gREAL_ESTATE_URL}/active?page=${page}&size=${size}`,
    method: 'GET',
    success: function (data) {
      paramCallbackFn(data);
    },
    error: function (jqXHR, textStatus, errorThrown) {
      console.log('Error:', errorThrown);
    }
  });
}
function onGetRealEstatesByCustomerIdClick(customerId, paramCallbackFn) {
  $.ajax({
    url: `${gREAL_ESTATE_URL}/${customerId}/customers`,
    method: 'GET',
    headers: getHeaders(),
    success: function (data) {
      paramCallbackFn(data);
    },
    error: (jqXHR, textStatus, errorThrown) => {
      console.log('Error:', errorThrown);
      redirectToLogin();
    }
  });
}
function onGetRealEstateByIdClick(realEstateId, paramCallbackFn) {
  $.ajax({
    url: gREAL_ESTATE_URL + "/" + realEstateId,
    method: 'GET',
    success: function (data) {
      paramCallbackFn(data);
    },
    error: function (jqXHR, textStatus, errorThrown) {
      console.log('Error:', errorThrown);
    }
  });
}
function onGetRealEstateBySearch(page, size, title, paramCallbackFn) {
  $.ajax({
    url: `${gREAL_ESTATE_URL}/search?title=${title}&page=${page}&size=${size}`,
    method: 'GET',
    success: function (data) {
      paramCallbackFn(data);
      $("body").addClass("box-collapse-closed")
      $("body").removeClass("box-collapse-open")
    },
    error: function (jqXHR, textStatus, errorThrown) {
      console.log('Error:', errorThrown);
    }
  });
}

function onGetRealEstateByFilter(page, size, data, paramCallbackFn) {
  const { bedroom, bath, provinceId, price } = data;

  // Construct the URL with the provided filter data
  const url = `${gREAL_ESTATE_URL}/filter?bedroom=${bedroom || ''}&bath=${bath || ''}&provinceId=${provinceId || ''}&price=${price || ''}&page=${page}&size=${size}`;
  $.ajax({
    url: url,
    method: 'GET',
    success: function (data) {
      paramCallbackFn(data);
      $("body").addClass("box-collapse-closed")
      $("body").removeClass("box-collapse-open")
    },
    error: function (jqXHR, textStatus, errorThrown) {
      console.log('Error:', errorThrown);
    }
  });
}

function onGetProvinceList(paramCallbackFn) {
  $.ajax({
    url: gPROVINCE_URL,
    method: 'GET',
    success: function (data) {
      paramCallbackFn(data);
    },
    error: function (jqXHR, textStatus, errorThrown) {
      console.log('Error:', errorThrown);
    }
  });
}

function onGetProvinceById(id) {
  return new Promise((resolve, reject) => {
    $.ajax({
      url: `${gPROVINCE_URL}/${id}`,
      method: 'GET',
      success: function (data) {
        resolve(data);
      },
      error: function (jqXHR, textStatus, errorThrown) {
        reject(errorThrown);
      }
    });
  });
}

function onShowToast(paramTitle, paramMessage) {
  $('#myToast .mr-auto').text(paramTitle)
  $('#myToast .toast-body').text(paramMessage);
  $('#myToast').toast('show');
}

function onGetRealEstatesByCustomerIdClick(customerId, paramCallbackFn) {
  $.ajax({
    url: `${gREAL_ESTATE_URL}/${customerId}/customers`,
    method: 'GET',
    headers: getHeaders(),
    success: function (data) {
      paramCallbackFn(data);
    },
    error: (jqXHR, textStatus, errorThrown) => {
      console.log('Error:', errorThrown);
    }
  });
}

function onGetProjectList(paramCallbackFn) {
  $.ajax({
    url: gPROJECT_URL,
    method: 'GET',
    headers: getHeaders(),
    success: function (data) {
      paramCallbackFn(data);
    },
    error: function (jqXHR, textStatus, errorThrown) {
      console.log('Error:', errorThrown);
    }
  });
}
function onGetCustomerInfo(paramCallbackFn) {
  $.ajax({
    url: gCUSTOMER_INFO_URL,
    method: 'GET',
    headers: getHeaders(),
    success: function (data) {
      paramCallbackFn(data);
    },
    error: function (jqXHR, textStatus, errorThrown) {
      console.log('Error:', errorThrown);
    }
  });
}


function onGetDistrictListByProvinceId(provinceId, paramCallbackFn) {
  $.ajax({
    url: `${gDISTRICT_URL}/${provinceId}/province`,
    method: 'GET',
    headers: getHeaders(),
    success: function (data) {
      paramCallbackFn(data);
    },
    error: function (jqXHR, textStatus, errorThrown) {
      console.log('Error:', errorThrown);
    }
  });
}

function onGetWardByDistrictId(districtId, paramCallbackFn) {
  $.ajax({
    url: `${gWARD_URL}/${districtId}/district`,
    method: 'GET',
    headers: getHeaders(),
    success: function (data) {
      paramCallbackFn(data);
    },
    error: function (jqXHR, textStatus, errorThrown) {
      console.log('Error:', errorThrown);
    }
  });
}

function onGetStreetByDistrictId(districtId, paramCallbackFn) {
  $.ajax({
    url: `${gSTREET_URL}/${districtId}/district`,
    method: 'GET',
    headers: getHeaders(),
    success: function (data) {
      paramCallbackFn(data);
    },
    error: function (jqXHR, textStatus, errorThrown) {
      console.log('Error:', errorThrown);
    }
  });
}
function onGetCustomerById(id) {
  return new Promise((resolve, reject) => {
    $.ajax({
      url: `${gCUSTOMER_URL}/${id}`,
      method: 'GET',
      headers: getHeaders(),
      success: function (data) {
        resolve(data);
      },
      error: function (jqXHR, textStatus, errorThrown) {
        reject(errorThrown);
      }
    });
  });
}
function onGetStreetById(id) {
  return new Promise((resolve, reject) => {
    $.ajax({
      url: `${gSTREET_URL}/${id}`,
      method: 'GET',
      headers: getHeaders(),
      success: function (data) {
        resolve(data);
      },
      error: function (jqXHR, textStatus, errorThrown) {
        reject(errorThrown);
      }
    });
  });
}
function onGetDistrictListById(id) {
  return new Promise((resolve, reject) => {
    $.ajax({
      url: `${gDISTRICT_URL}/${id}`,
      method: 'GET',
      headers: getHeaders(),
      success: function (data) {
        resolve(data);
      },
      error: function (jqXHR, textStatus, errorThrown) {
        reject(errorThrown);
      }
    });
  });
}
function onGetWardById(id) {
  return new Promise((resolve, reject) => {
    $.ajax({
      url: `${gWARD_URL}/${id}`,
      method: 'GET',
      headers: getHeaders(),
      success: function (data) {
        resolve(data);
      },
      error: function (jqXHR, textStatus, errorThrown) {
        reject(errorThrown);
      }
    });
  });
}
function onGetProvinceById(id) {
  return new Promise((resolve, reject) => {
    $.ajax({
      url: `${gPROVINCE_URL}/${id}`,
      method: 'GET',
      headers: getHeaders(),
      success: function (data) {
        resolve(data);
      },
      error: function (jqXHR, textStatus, errorThrown) {
        reject(errorThrown);
      }
    });
  });
}
function onGetProjectById(id) {
  return new Promise((resolve, reject) => {
    $.ajax({
      url: `${gPROJECT_URL}/${id}`,
      method: 'GET',
      headers: getHeaders(),
      success: function (data) {
        resolve(data);
      },
      error: function (jqXHR, textStatus, errorThrown) {
        reject(errorThrown);
      }
    });
  });
}


function onGetRealEstateByIdClick(realEstateId, paramCallbackFn) {
  $.ajax({
    url: gREAL_ESTATE_URL + "/" + realEstateId,
    method: 'GET',
    headers: getHeaders(),
    success: function (data) {
      paramCallbackFn(data);
    },
    error: function (jqXHR, textStatus, errorThrown) {
      console.log('Error:', errorThrown);
    }
  });
}


function onUpdateRealEstateClick(realEstateId, realEstateData, paramCallbackFn) {
  $.ajax({
    url: gREAL_ESTATE_URL + "/" + realEstateId,
    method: 'PUT',
    headers: getHeaders(),
    data: JSON.stringify(realEstateData),
    contentType: 'application/json',
    success: (data) => {
      paramCallbackFn(data);
      location.reload()
      $('#update-realEstate-modal').modal('hide');
      onShowToast("Sửa thành công", "Sửa Bất động sản thành công!!")
    },
    error: function (jqXHR, textStatus, errorThrown) {
      console.log('Error:', errorThrown);
    }
  });
}   
function onDeleteRealEstateClick(realEstateId) {
  $.ajax({
    url: gREAL_ESTATE_URL + "/" + realEstateId,
    method: 'DELETE',
    headers: getHeaders(),
    success: () => {
      onShowToast("Xóa thành công", "bạn đã xóa Bất động sản thành công!!")
      $("#delete-confirm-modal").modal("hide");
      location.reload()
    },
    error: function (jqXHR, textStatus, errorThrown) {
      console.log('Error:', errorThrown);
    }
  });
}
function onUploadFile(file, paramCallbackFn) {
  const formData = new FormData();
  formData.append('image', file);

  $.ajax({
    url: gUPLOAD_FILE_URL,
    method: 'POST',
    headers: getHeaders(),
    data: formData,
    processData: false,
    contentType: false,
    success: (response) => {
      paramCallbackFn(response);
    },
    error: function (jqXHR, textStatus, errorThrown) {
      console.log('Error:', errorThrown);
    }
  });
}


function getHeaders() {
  return {
    Authorization: "Bearer " + getCookie("token")
  };
}
function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}


function setCookie(name, value, days) {
  const expires = new Date();
  expires.setTime(expires.getTime() + days * 24 * 60 * 60 * 1000);
  document.cookie = name + "=" + encodeURIComponent(value) + ";expires=" + expires.toUTCString() + ";path=/";
}


/*** REGION 5 - UTILITY - Vùng  UTILITY */
function getQueryParam(name) {
  const queryString = window.location.search;
  const urlParams = new URLSearchParams(queryString);
  return urlParams.get(name);
}

function getRandomHomePhoto() {
  const randomIndex = Math.floor(Math.random() * homePhoto.length);
  return homePhoto[randomIndex];
}
function truncateAddress(address) {
  const maxWords = 5;
  const words = address.split(' ');
  if (words.length <= maxWords) {
    return address;
  } else {
    const truncatedWords = words.slice(0, maxWords);
    return `${truncatedWords.join(' ')} ...`;
  }
}
const homePhoto = [
  "https://images.unsplash.com/photo-1588012886079-baef0ac45fbd?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=867&q=80",
  "https://images.unsplash.com/photo-1604014238170-4def1e4e6fcf?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=870&q=80",
  "https://images.unsplash.com/photo-1600585154363-67eb9e2e2099?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=870&q=80",
  "https://images.unsplash.com/photo-1600047509782-20d39509f26d?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=701&q=80",
  "https://images.unsplash.com/photo-1600563438938-a9a27216b4f5?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=870&q=80",
  "https://images.unsplash.com/photo-1547638599-d4bf222cf5d1?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=870&q=80",
  "https://images.unsplash.com/photo-1426122402199-be02db90eb90?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=870&q=80",
  "https://images.unsplash.com/photo-1489370321024-e0410ad08da4?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=870&q=80",
  "https://plus.unsplash.com/premium_photo-1682377521715-95d16dc51943?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=871&q=80",
  "https://images.unsplash.com/photo-1512915922686-57c11dde9b6b?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=873&q=80",
  "https://images.unsplash.com/photo-1628744448840-55bdb2497bd4?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=870&q=80",
  "https://images.unsplash.com/photo-1594540992254-0e2239661647?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=869&q=80",
  "https://plus.unsplash.com/premium_photo-1661883982941-50af7720a6ff?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=774&q=80"

]


