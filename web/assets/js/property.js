/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gREAL_ESTATE_URL = "http://localhost:8080/api/real-estates";
const gPROVINCE_URL = "http://localhost:8080/api/provinces";
const gDISTRICT_URL = "http://localhost:8080/api/districts";
const gSTREET_URL = "http://localhost:8080/api/streets";
const gWARD_URL = "http://localhost:8080/api/wards";
const gPROJECT_URL = "http://localhost:8080/api/projects";
const gCUSTOMER_URL = "http://localhost:8080/api/customers";
const gUPLOAD_FILE_URL = "http://localhost:8080/api/upload";
const gCUSTOMER_INFO_URL = "http://localhost:8080/users/me"
const gBASE_URL = "http://127.0.0.1:5500/real-estates-frontend/web"
//Hàm chính để load html hiển thị ra bảng
class Main {
  constructor() {
    $(document).ready(() => {
      const property = new AddProperty()
      property.createProperty()

      

    })
  }

}
new Main()
/*** REGION 3 - vùng hiện modal*/

class AddProperty {
  constructor() {
    this.vApi = new CallApi()
  }
  _getProvinceList() {

    const provinceDropdown = $("#input-create-provinceId");

    this.vApi.onGetProvinceList((provinces) => {
      provinceDropdown.empty();
      provinceDropdown.append('<option value="" selected>Province</option>');

      provinces.forEach((province) => {
        const option = `<option value="${province.id}">${province.name}</option>`;
        provinceDropdown.append(option);
      });
    });
  }
  _getDistrictList(provinceId) {

    const districtDropdown = $("#input-create-districtId");


    this.vApi.onGetDistrictListByProvinceId(provinceId, (districts) => {
      districtDropdown.empty();
      districtDropdown.append('<option value="" selected>District</option>');

      districts.forEach((district) => {
        const option = `<option value="${district.id}">${district.name}</option>`;
        districtDropdown.append(option);
      });
    })

  }
  _getWardList(districtId) {
    const wardDropdown = $("#input-create-wardId");
    this.vApi.onGetWardByDistrictId(districtId, (wards) => {
      wardDropdown.empty();
      wardDropdown.append('<option value="" selected>Ward</option>');

      wards.forEach((ward) => {
        const option = `<option value="${ward.id}">${ward.name}</option>`;
        wardDropdown.append(option);
      });
    })
    
  }
  _getStreetList(districtId) {
    const streetDropdown = $("#input-create-streetId");

    this.vApi.onGetStreetByDistrictId(districtId, (streets) => {
      streetDropdown.empty();
      streetDropdown.append('<option value="" selected>Street</option>');

      streets.forEach((street) => {
        const option = `<option value="${street.id}">${street.name}</option>`;
        streetDropdown.append(option);
      });
    })

  }
  _getProjectList() {
    const projectDropdown = $("#input-create-projectId");
    this.vApi.onGetProjectList((projects) => {
      projectDropdown.empty();
      projectDropdown.append('<option value="" selected>Project</option>');
      projects.forEach((project) => {
        const option = `<option value="${project.id}">${project.name}</option>`;
        projectDropdown.append(option);
      });
    })
  }



  _createRealEstate() {
    this._getProvinceList();
    this._clearInput();
    this._getProjectList();
    let imageURL;
    $("#input-create-provinceId").on("change", () => {
      const provinceId = $("#input-create-provinceId").val();
      this._getDistrictList(provinceId);
    });

    $("#input-create-districtId").on("change", () => {
      const districtId = $("#input-create-districtId").val();
      this._getWardList(districtId);
      this._getStreetList(districtId);
    });
    $("#input-create-photo").on("change", (event) => {
      const fileInput = event.target;
      const file = fileInput.files[0];
      this.vApi.onUploadFile(file, (response) => {
        imageURL = response
        console.log(response)
        $(".imagePreview ").css("background-image", "url(" + response + ")");
      });
    });


    $("#btn-add-realEstate").on("click", (event) => {
      this._clearInValid();
      event.preventDefault();
      let isValid = true;
      const vFields = [
        "input-create-title",

      ];

      vFields.forEach((field) => {
        const value = $(`#${field}`).val();
        if (!value) {
          isValid = false;
          $(`#${field}`).addClass("is-invalid");
          $(`#${field}`).after(`<div class="invalid-feedback error-message">Please enter a valid value!</div>`);
        }
      });

      if (isValid) {
        // Get values from the form
        const address = $("#input-create-address").val();
        const photo = imageURL;
        const provinceId = parseInt($("#input-create-provinceId").val());
        const districtId = parseInt($("#input-create-districtId").val());
        const wardId = parseInt($("#input-create-wardId").val());
        const streetId = parseInt($("#input-create-streetId").val());
        const projectId = parseInt($("#input-create-projectId").val());
        const title = $("#input-create-title").val();
        const type = parseInt($("#input-create-type").val());
        const request = parseInt($("#input-create-request").val());
        const customerId = JSON.parse(localStorage.getItem('customer'))?.id;;
        const price = $("#input-create-price").val();
        const minPrice = $("#input-create-minPrice").val();
        const priceTime = parseInt($("#input-create-priceTime").val());
        const apartCode = $("#input-create-apartCode").val();
        const acreage = $("#input-create-acreage").val();
        const bedroom = $("#input-create-bedroom").val();
        const balcony = parseInt($("#input-create-balcony").val());
        const landscapeView = $("#input-create-landscapeView").val();
        const apartLoca = $("#input-create-apartLoca").val();
        const apartType = $("#input-create-apartType").val();
        const rentPrice = parseFloat($("#input-create-rent").val());
        const bdsArea = $("#input-create-acreage").val();
        const houseDirection = $("#input-create-acreage").val();
        const totalFloors = parseInt($("#input-create-totalFloors").val());
        const numberFloors = parseInt($("#input-create-floorNum").val());;
        const description = $("#input-create-desc").val();
        const width = parseFloat($("#input-create-width").val());
        const length = parseFloat($("#input-create-long").val());
        const shape = $("#input-create-shape").val();
        const adjacentRoad = $("#input-create-adjacentRoad").val();
        const alleyMinWidth = $("#input-create-alleyMinWidth").val();
        const ctxdPrice = $("#input-create-ctxdPrice").val();
        const ctxdValue = $("#input-create-ctxdValue").val();
        const alleyWidth = $("#input-create-alleyMinWidth").val();
        const factor = parseInt($("#input-create-factor").val());
        const structure = $("#input-create-structure").val();
        const clcl = parseInt($("#input-create-clcl").val());
        const lat = parseInt($("#input-create-lat").val());
        const long = parseInt(($("#input-create-long").val()));

        const realEstateData = {
          address,
          photo,
          bedroom,
          provinceId,
          districtId,
          wardId,
          streetId,
          projectId,
          title,
          type,
          request,
          customerId,
          price,
          minPrice,
          priceTime,
          apartCode,
          acreage,
          balcony,
          landscapeView,
          apartLoca,
          apartType,
          rentPrice,
          bdsArea,
          houseDirection,
          totalFloors,
          numberFloors,
          description,
          width,
          length,
          shape,
          adjacentRoad,
          alleyMinWidth,
          ctxdPrice,
          ctxdValue,
          alleyWidth,
          factor,
          structure,
          clcl,
          lat,
          long,
        };

        this.vApi.onCreateRealEstateClick(realEstateData, (data) => {
          console.log(data)
        })

      }
    })
  }

 _showProfileAdterLogin(){
    const customer = JSON.parse(localStorage.getItem('customer'));
    console.log("called", customer)
    if (customer?.accessToken) {
      $('.profile-item').show();
      $('.login').hide();
      $('.create-realEstate').show();
    } else {
      window.location.href = `${gBASE_URL}/login.html`;
      $('.login').show();
      $('.profile-item').hide();
      $('.create-realEstate').hide();
    }
  
  }

  _getCheckedValues(checkboxes) {
    return checkboxes
      .filter(([checkboxId, checkboxValue]) => $(`#${checkboxId}`).prop("checked"))
      .map(([, checkboxValue, checkboxName]) => checkboxValue);
  }
  _clearInValid(input) {
    if (input) {
      input.removeClass("is-invalid");
      $(".invalid-feedback").remove();
    }
    $(".form-control").removeClass("is-invalid");
    $(".invalid-feedback").remove();
  }

  _clearInput() {
    $("#create-realEstate-form").find("input").val("");
    $("#create-realEstate-form").find("select").val("")
  }
  _onEventListener() {
    $(".logout-item").on("click", () => {
      this._logout();
      this._showProfileAdterLogin();
    })
  }

  _logout() {
    this._setCookie("token", "", 1);
    localStorage.removeItem('customer');
  
  }
  _setCookie(name, value, days) {
    const expires = new Date();
    expires.setTime(expires.getTime() + days * 24 * 60 * 60 * 1000);
    document.cookie = name + "=" + encodeURIComponent(value) + ";expires=" + expires.toUTCString() + ";path=/";
  }
  createProperty() {
    this._onEventListener()
    this._createRealEstate()
    this._showProfileAdterLogin()
  }
}

/*** REGION 4 - vùng để gọi lên cơ sở dữ liệu lấy đa ta về*/
class CallApi {
  constructor() {
    this.token = this.getCookie("token");
  }

  setToken(token) {
    this.token = token;
  }

  getHeaders() {
    return {
      Authorization: "Bearer " + this.token
    };
  }
  setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  }

  redirectToLogin() {
    // Trước khi logout cần xóa token đã lưu trong cookie
    this.setCookie("token", "", 1);
    window.location.href = "http://127.0.0.1:5500/real-estates-frontend/html/login.html";
  }

  onShowToast(paramTitle, paramMessage) {
    $('#myToast .mr-auto').text(paramTitle)
    $('#myToast .toast-body').text(paramMessage);
    $('#myToast').toast('show');
  }
  onGetCustomerInfo(paramCallbackFn) {
    $.ajax({
      url: gCUSTOMER_INFO_URL,
      method: 'GET',
      headers: this.getHeaders(),
      success: function (data) {
        paramCallbackFn(data);
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log('Error:', errorThrown);
      }
    });
  }

  onCheckProcessRealEstateClick(processState, id, paramCallbackFn) {
    $.ajax({
      url: `${gREAL_ESTATE_URL}/${id}/process`,
      method: 'PUT',
      headers: this.getHeaders(),
      data: processState,
      contentType: 'application/json',
      success: (data) => {
        paramCallbackFn(data);
        this.onShowToast("Duyệt thành công", "Duyệt bất động sản thành công!!")

      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log('Error:', errorThrown);
      }
    });
  }

  onGetRealEstatesClick(paramCallbackFn) {
    $.ajax({
      url: gREAL_ESTATE_URL,
      method: 'GET',
      headers: this.getHeaders(),
      success: function (data) {
        paramCallbackFn(data);
      },
      error: (jqXHR, textStatus, errorThrown) => {
        console.log('Error:', errorThrown);
        this.redirectToLogin();
      }
    });
  }

  onGetProjectList(paramCallbackFn) {
    $.ajax({
      url: gPROJECT_URL,
      method: 'GET',
      headers: this.getHeaders(),
      success: function (data) {
        paramCallbackFn(data);
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log('Error:', errorThrown);
      }
    });
  }

  onGetCustomerList(paramCallbackFn) {
    $.ajax({
      url: gCUSTOMER_URL,
      method: 'GET',
      headers: this.getHeaders(),
      success: function (data) {
        paramCallbackFn(data);
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log('Error:', errorThrown);
      }
    });
  }

  onGetProvinceList(paramCallbackFn) {
    $.ajax({
      url: gPROVINCE_URL,
      method: 'GET',
      headers: this.getHeaders(),
      success: function (data) {
        paramCallbackFn(data);
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log('Error:', errorThrown);
      }
    });
  }

  onGetDistrictListByProvinceId(provinceId, paramCallbackFn) {
    $.ajax({
      url: `${gDISTRICT_URL}/${provinceId}/province`,
      method: 'GET',
      headers: this.getHeaders(),
      success: function (data) {
        paramCallbackFn(data);
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log('Error:', errorThrown);
      }
    });
  }

  onGetWardByDistrictId(districtId, paramCallbackFn) {
    $.ajax({
      url: `${gWARD_URL}/${districtId}/district`,
      method: 'GET',
      headers: this.getHeaders(),
      success: function (data) {
        paramCallbackFn(data);
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log('Error:', errorThrown);
      }
    });
  }

  onGetStreetByDistrictId(districtId, paramCallbackFn) {
    $.ajax({
      url: `${gSTREET_URL}/${districtId}/district`,
      method: 'GET',
      headers: this.getHeaders(),
      success: function (data) {
        paramCallbackFn(data);
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log('Error:', errorThrown);
      }
    });
  }
  onGetCustomerById(id) {
    return new Promise((resolve, reject) => {
      $.ajax({
        url: `${gCUSTOMER_URL}/${id}`,
        method: 'GET',
        headers: this.getHeaders(),
        success: function (data) {
          resolve(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
          reject(errorThrown);
        }
      });
    });
  }
  onGetDistrictListById(id) {
    return new Promise((resolve, reject) => {
      $.ajax({
        url: `${gDISTRICT_URL}/${id}`,
        method: 'GET',
        headers: this.getHeaders(),
        success: function (data) {
          resolve(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
          reject(errorThrown);
        }
      });
    });
  }
  onGetStreetById(id) {
    return new Promise((resolve, reject) => {
      $.ajax({
        url: `${gSTREET_URL}/${id}`,
        method: 'GET',
        headers: this.getHeaders(),
        success: function (data) {
          resolve(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
          reject(errorThrown);
        }
      });
    });
  }
  onGetDistrictListById(id) {
    return new Promise((resolve, reject) => {
      $.ajax({
        url: `${gDISTRICT_URL}/${id}`,
        method: 'GET',
        headers: this.getHeaders(),
        success: function (data) {
          resolve(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
          reject(errorThrown);
        }
      });
    });
  }
  onGetWardById(id) {
    return new Promise((resolve, reject) => {
      $.ajax({
        url: `${gWARD_URL}/${id}`,
        method: 'GET',
        headers: this.getHeaders(),
        success: function (data) {
          resolve(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
          reject(errorThrown);
        }
      });
    });
  }
  onGetProvinceById(id) {
    return new Promise((resolve, reject) => {
      $.ajax({
        url: `${gPROVINCE_URL}/${id}`,
        method: 'GET',
        headers: this.getHeaders(),
        success: function (data) {
          resolve(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
          reject(errorThrown);
        }
      });
    });
  }
  onGetProjectById(id) {
    return new Promise((resolve, reject) => {
      $.ajax({
        url: `${gPROJECT_URL}/${id}`,
        method: 'GET',
        headers: this.getHeaders(),
        success: function (data) {
          resolve(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
          reject(errorThrown);
        }
      });
    });
  }


  onGetRealEstateByIdClick(realEstateId, paramCallbackFn) {
    $.ajax({
      url: gREAL_ESTATE_URL + "/" + realEstateId,
      method: 'GET',
      headers: this.getHeaders(),
      success: function (data) {
        paramCallbackFn(data);
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log('Error:', errorThrown);
      }
    });
  }

  onCreateRealEstateClick(realEstateData, paramCallbackFn) {
    $.ajax({
      url: gREAL_ESTATE_URL,
      method: 'POST',
      headers: this.getHeaders(),
      data: JSON.stringify(realEstateData),
      contentType: 'application/json',
      success: (data) => {
        paramCallbackFn(data);
        this.onShowToast("Tạo thành công", "Tạo Bất động sản thành công!!")
         window.location.href = "property-manage.html";

      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log('Error:', errorThrown, textStatus);
      }
    });
  }


  onUploadFile(file, paramCallbackFn) {
    const formData = new FormData();
    formData.append('image', file);

    $.ajax({
      url: gUPLOAD_FILE_URL,
      method: 'POST',
      headers: this.getHeaders(),
      data: formData,
      processData: false,
      contentType: false,
      success: (response) => {
        paramCallbackFn(response);
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log('Error:', errorThrown);
      }
    });
  }


  getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }
}
