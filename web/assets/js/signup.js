
const gBASE_URL = "http://127.0.0.1:5500/real-estates-frontend/web"
const gSIGNUP_URL = "http://localhost:8080/api/auth/signup"
const gCUSTOMER_URL = "http://localhost:8080/api/customers"
$(document).ready(function() {
    //Sự kiện bấm nút sign up
    $("#submit").on("click", async function(event) {
        event.preventDefault()
        var username = $("#inputUsername").val().trim();
        var email = $("#inputEmail").val().trim();
        var phone =$("#inputPhone").val().trim();
        var password = $("#inputPassword").val().trim();

        if(phone) {
           const customer = await onGetCustomerByMobile(phone)
           var confirmPassword = $("#inputConfirmPassword").val().trim();
           if (validateForm(username, email, password, confirmPassword, phone)) {
               signUpForm(username, email, password,customer.id);
           }
        }
  
    });

    function signUpForm(username, email, password, customerId) {
        var vSignUpData = {
            username: username,
            email,
            password,
            customerId:customerId ||null
        };

        console.log(vSignUpData)

        $.ajax({
            url: gSIGNUP_URL,
            type: "POST",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(vSignUpData),
            success: function(responseObject) {
                responseHandler(responseObject);
               
            },
            error: function(xhr) {
                // Lấy error message
                var errorMessage = xhr.responseJSON.message;
                showError(errorMessage);
                console.log(errorMessage)
                onShowToast("Error", errorMessage)
            }
        });
    }

    //Xử lý object trả về khi sign up thành công
    function responseHandler(data) {
        console.log(data)
        onShowToast("Sign-up successful!","Sign-up successful!");
        window.location.href=`${gBASE_URL}/login.html`
    }

    //Hiển thị lỗi lên form
    function showError(message) {
        var errorElement = $("#error");

        errorElement.html(message);
        errorElement.removeClass("d-none");
        errorElement.addClass("d-block");
    }

    //Validate dữ liệu từ form
    function validateForm(username, email, password, confirmPassword, phone) {
        if (username === "") {
            onShowToast("Username Error", "Username is invalid");
            return false;
        }

        if (email === "") {
            onShowToast("Email Error", "Email is invalid");
            return false;
        }

        if (password === "") {
            onShowToast("Password Error", "Password is invalid");
            return false;
        }

        if (confirmPassword === "") {
            onShowToast("Password Error", "Password is invalid");
            return false;
        }

        if (password !== confirmPassword) {
            onShowToast("Password does not match", "Password does not match");
            return false;
        }

         if (!phone) {
            onShowToast("Phone error", "Phone is not valid");
            return false;
        }

        return true;
    }
});

function onGetCustomerByMobile(mobile) {
    return new Promise((resolve, reject) => {
        $.ajax({
          url: `${gCUSTOMER_URL}/${mobile}/mobile`,
          method: 'GET',
          success: function (data) {
            resolve(data);
          },
          error: function (jqXHR, textStatus, errorThrown) {
            reject(errorThrown);
          }
        });
      });
  }

function  onShowToast(paramTitle, paramMessage) {
    $('#myToast .mr-auto').text(paramTitle)
    $('#myToast .toast-body').text(paramMessage);
    $('#myToast').toast('show');
  }
